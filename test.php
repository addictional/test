<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Интернет-магазин \"Одежда\"");
?>
<style type="text/css">
	.loader
	{
		border :5px solid grey;
		border-top: 5px solid #f7bb03;
		border-bottom: 5px solid #f7bb03;
		width: 20px;
		height: 20px;
		border-radius: 50%;
		animation: spin 2s linear infinite;
	}
	@keyframes spin {
		from
		{
			transform: rotate(0deg);
		}
		to
		{
			transform: rotate(360deg);
		}
	}
</style>
<div class="loader"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>