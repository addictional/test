<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Sender\Subscription;
Loader::includeModule('sender');

class subscribeCustom extends CBitrixComponent
{
	protected $request;
	protected $user;
	public function __construct($component)
	{
		parent::__construct($component);
		$this->request = $_REQUEST;
		$this->user = $this->initUser();
	}
	public function executeComponent()
	{
		$this->initSubscribe();
	}
	protected function checkRequest()
	{
		return isset($this->request['AJAX'])&&$this->request['AJAX']=='YES'&& isset($this->request['EMAIL_SUB']);
			
	}
	protected function checkEmail()
	{
			return preg_match('/^((\"[^\"\f\n\r\t\v\b]+\")|([\w\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+(\.[\w\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+)*))@((\[(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))\])|(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))|((([A-Za-z0-9\-])+\.)+[A-Za-z\-]+))$/D', $this->request['EMAIL_SUB']);
	}
	protected function addSubscriber()
	{
		if(Subscription::add($this->request['EMAIL_SUB'],array($this->user)))
		{
			echo json_encode(array('STATE' => true));
		}
		else
			echo json_encode(array('error' => 'yes'));
	}
	protected function initSubscribe()
	{
		if(isset($this->request['AJAX'])&&$this->request['AJAX']="YES")
		{
			global $APPLICATION;
			if($this->checkRequest()&&$this->checkEmail())
			{
				$APPLICATION->restartBuffer();
				$this->addSubscriber();
				die();
			}
			else
			{
				$APPLICATION->restartBuffer();
				echo json_encode(array('STATE' => false));
				die();
			}
		}
		else
		$this->IncludeComponentTemplate();	
	}
	protected function initUser()
	{
        global $USER;
        if ($USER->getId())
            return $USER->getId();
        else
            return \CSaleUser::GetAnonymousUserID();
    }
}