<div class="helpful">
      <div class="container">
        <div class="helpful__container-flex">
          <div class="helpful__container-imagetext">
            <div class="helpful__container-image"><img class="helpful__image-envelope" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/envelope.svg" alt="envelope"></div>
            <div class="helpful__container-text">
              <p class="helpful__text">Только полезная информация, новинки и скидки для своих!</p>
            </div>
          </div>
          <div class="helpful__container-inputbutton">
            <div class="helpful__container-input">
              <input class="helpful__input-email" type="text" placeholder="Введите Ваш E-mail">
            </div>
            <div class="helpful__container-button">
              <button class="helpful__button-join">Вступить в клуб</button>
            </div>
          </div>
        </div>
      </div>
    </div>