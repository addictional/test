<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */
// echo '<pre>'; print_r($arParams); echo '<pre>';
$arParams['CACHE_TIME'];
use Bitrix\Main\Loader;
use Bitrix\Main\Data\Cache;
if ($arParams["ID"] <= 0)
   $arParams["ID"] = 10;

// $cacheDir = "/".self::MODULE_ID."/".$cacheId;
if($this->StartResultCache(false, $USER->GetGroups()))
{
		if(Loader::includeModule('sale'))
	{
		$arFilter = array(
			'USER_ID' => $USER->GetID(),
			'>=PROPERTY_VAL_BY_CODE_delivery_date' => ConvertDateTime(date("d.m.Y G:i:s"),"YYYY-MM-DD")." 00:00:00"
		);
		$order = array(
			'PROPERTY_VAL_BY_CODE_delivery_date' => "ASC"
		);
		$orders = CSaleOrder::GetList($order,$arFilter);
		if ($result = $orders->fetch()) 
		{
			$arResult['ITEMS'][]=$result['ID'];
		}
		else{
			$arResult=false;
		}
		if($arResult!=false){
		$obProps = Bitrix\Sale\Internals\OrderPropsValueTable::getList(array('filter' => array('ORDER_ID' => $arResult['ITEMS'])));
		while($prop = $obProps->Fetch()){
			if($prop['CODE']=='delivery_date')
   		$arResult = $prop['VALUE'];
		}}

	}		
		$this->IncludeComponentTemplate();
}
else

	
	
	

?>