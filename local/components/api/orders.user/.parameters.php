<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("form")) return;

$arStartPage = array("list" => GetMessage("COMP_FORM_VALUES_LIST"), "new" => GetMessage("COMP_FORM_VALUES_NEW"));

$arrForms = array();
$rsForm = CForm::GetList($by='s_sort', $order='asc', !empty($_REQUEST["site"]) ? array("SITE" => $_REQUEST["site"]) : array(), $v3);
while ($arForm = $rsForm->Fetch())
{
	$arrForms[$arForm["ID"]] = "[".$arForm["ID"]."] ".$arForm["NAME"];
}

if (intval($arCurrentValues["WEB_FORM_ID"]) > 0)
{
	$show_list = true;
	$rsFieldList = CFormField::GetList(intval($arCurrentValues["WEB_FORM_ID"]), "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
	$arFieldList = array();
	while ($arField = $rsFieldList->GetNext())
	{
		$arFieldList[$arField["SID"]] = "[".$arField["SID"]."] ".$arField["TITLE"];
	}
}
else
{
	$show_list = false;
}

$arComponentParameters = array(
	"PARAMETERS" => array(
		"AJAX_MODE" => array(),
		"VARIABLE_ALIASES" => Array(
			"action" => Array("NAME" => GetMessage("COMP_FORM_PARAMS_ACTION_ALIAS")),
		),
		
		"WEB_FORM_ID" => array(
			"NAME" => GetMessage("COMP_FORM_PARAMS_WEB_FORM_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arrForms,
			"ADDITIONAL_VALUES"	=> "Y",
			"REFRESH" => "Y",
			"DEFAULT" => "={\$_REQUEST[WEB_FORM_ID]}",
			"PARENT" => "DATA_SOURCE",
		),

		"RESULT_ID" => array(
			"NAME" => GetMessage("COMP_FORM_PARAMS_RESULT_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => "={\$_REQUEST[RESULT_ID]}",
			"PARENT" => "DATA_SOURCE",
		),

		

		"SHOW_LIST_PAGE" => array(
			"NAME" => GetMessage("COMP_FORM_PARAMS_SHOW_LIST_PAGE"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
			"ADDITIONAL_VALUES" => "N",
			"PARENT" => "BASE",
		),

		"SHOW_EDIT_PAGE" => array(
			"NAME" => GetMessage("COMP_FORM_PARAMS_SHOW_EDIT_PAGE"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
			"ADDITIONAL_VALUES" => "N",
			"PARENT" => "BASE",
		),

		"USE_EXTENDED_ERRORS" => array(
			"NAME" => GetMessage("COMP_FORM_PARAMS_USE_EXTENDED_ERRORS"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
			"PARENT" => "VISUAL",
		),

		"CACHE_TIME" => array("DEFAULT" => "3600"),
	),
);
?>