<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */
Bitrix\Main\Loader::includeModule("sale");
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;
use Bitrix\Sale;

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$arResult['URL']='index.php?count=123&action=submit&bxajaxid=comp_'.$arParams['~AJAX_ID'];
if(isset($_REQUEST['count'])){

		
		$arResult['price']=$basket->getPrice();

		$items=$basket->getBasketItems();
		$arResult['count']=0;
		foreach ($basket as $key => $item) {
			$arResult['count']+=$item->getQuantity();
		}
		$APPLICATION->RestartBuffer();
		echo json_encode($arResult['count']);
		die();
}
 if(isset($_REQUEST['ID'])){
 	$APPLICATION->RestartBuffer();
 var_dump($_REQUEST);
 	
 	die();
 }
/*************************************************************************
	Processing of received parameters
*************************************************************************/
else{	
		$user = CUser::GetByID($USER->GetID())->fetch(); 
		
		$arResult['compare'] = count($user['UF_COMPARES']);
		$arResult['favorite'] = count($user['UF_FAVORITES']);
		$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
		
		$arResult['price']=$basket->getPrice();

		$items=$basket->getBasketItems();
		$arResult['count']=0;
		foreach ($basket as $key => $item) {
			$arResult['count']+=$item->getQuantity();
		}
		
		$this->IncludeComponentTemplate();
	}
	
	

?>