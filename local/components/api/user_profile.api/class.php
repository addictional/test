<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $USER;
use Bitrix\Main\Mail\Event;
use Bitrix\Main\Loader;
use Bitrix\Sender\Subscription;
Loader::includeModule('sender');

class UserProfile extends CBitrixComponent {
	protected $request;
	protected $user;
	protected $change = array( "LID" => SITE_ID,
   "ACTIVE" => "Y",);
	public function __construct($component)
	{
		global $USER;
		parent::__construct($component);
		$this->user = $USER->GetID();
		$this->request = $_REQUEST;
	}
	protected function changeEmail()
	{
		if(isset($this->request['EMAIL']))
		{
			if (isset($_SESSION['CHANGE_EMAIL'])&& isset($_REQUEST['CHANGE_EMAIL']))
			{
				$this->change['EMAIL']=trim($_SESSION['CHANGE_EMAIL']);
				$this->change['LOGIN']=trim($_SESSION['CHANGE_EMAIL']);
			}
			else{
			Event::send(array(
			"EVENT_NAME" => "SENDER_SUBSCRIBE_CONFIRM",
			"LID" => "s1",
			"C_FIELDS" => array(
				"EMAIL" => $this->request['EMAIL'],
			)
			));
			$_SESSION['CHANGE_EMAIL'] = $this->request['EMAIL'];
		}
		}
	}
	protected function changePhone()
	{
		if(isset($this->request['PERSONAL_PHONE']))
		{
			$this->change['PERSONAL_PHONE']=$this->request['PERSONAL_PHONE'];
		}
	}
	protected function changeGen()
	{
		if(isset($this->request['UF_GEN']))
		{
			$this->change['UF_GEN']=$this->request['UF_GEN'];
		}
	}
	protected function changeName()
	{
		if(isset($this->request['NAME']))
		{
			$this->change['NAME']=$this->request['NAME'];
		}
	}
	protected function changeLastName()
	{
		if(isset($this->request['LAST_NAME']))
		{
			$this->change['LAST_NAME']=$this->request['LAST_NAME'];
		}
	}
	protected function changeAddress()
	{
		$this->changeStreet();
		$this->changeBuild();
		$this->changeCorp();
		$this->changeAppart();
	}
	protected function changeStreet()
	{
		if(isset($this->request['UF_STREET']))
		{
			$this->change['UF_STREET']=$this->request['UF_STREET'];
		}
	}
	protected function changeBuild()
	{
		if(isset($this->request['UF_BUILD']))
		{
			$this->change['UF_BUILD']=$this->request['UF_BUILD'];
		}
	}
	protected function changeCorp()
	{
		if(isset($this->request['UF_CORP']))
		{
			$this->change['UF_CORP']=$this->request['UF_CORP'];
		}
	}
	protected function changeAppart()
	{
		if(isset($this->request['UF_APPART']))
		{
			$this->change['UF_APPART']=$this->request['UF_APPART'];
		}
	}
	protected function changePassword()
	{
		if(isset($this->request['PASSWORD']) && isset($this->request['CONFIRM_PASSWORD']))
		{
			$this->change['PASSWORD']=$this->request['PASSWORD'];
			$this->change['CONFIRM']=$this->request['CONFIRM_PASSWORD'];
			echo json_encode(array("STATE" => true));
		}elseif (!isset($this->request['CONFIRM_PASSWORD'])&& isset($this->request['PASSWORD'])) {
			echo json_encode(array("STATE" => false));
		}
	}
	protected function subscribe()
	{
		if($this->request['subscribe']){
		global $USER;
		if(Subscription::add($USER->GetEmail(),array($this->user)))
			{
		}
		else
		{}	
		}
	}
	protected function init(){
		if(isset($this->request['AJAX'])&& $this->request['AJAX'] == "CHANGE")
		{
			global $APPLICATION;
			$APPLICATION->restartBuffer();
			$this->changeEmail();
			$this->changePhone();
			$this->changeAddress();
			$this->subscribe();
			$this->changeGen();
			$this->changeName();
			$this->changeLastName();
			$this->changePassword();
			$user = new CUser;
			$user->Update($this->user, $this->change);
			die();
		}	
	}
	public function executeComponent()
	{
		$this->init();
		$this->IncludeComponentTemplate();
		// echo "<pre>"; print_r($this->change); echo "<pre>";
	}
}