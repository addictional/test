<!-- <?echo "<pre>"; print_r($_SESSION); echo "<pre>";?> -->
  <?
  $user = CUser::GetByID($USER->GetID())->fetch();
  // echo "<pre>";  print_r($user); echo "<pre>";
  ?>
<div class="mydetails__bigtitle-wrapper">
      <div class="container">
        <div class="mydetails__bigtitle-text">  Мои данные</div>
      </div>
    </div>
    <div class="mydetails__big-wrapper">
      <div class="container">
        <div class="mydetails__wrapper-flex">
          <aside class="aside__menu-wrapper">
            <div class="aside__menu-container-type">
              <a class="aside__menu-link" href="/personal/">Главная</a>
              <a class="aside__menu-link" href="/personal/cart/">Моя корзина</a>
              <a class="aside__menu-link" href="/personal/delivery-hystory.php">Мои доставки</a>
              <a class="aside__menu-link" href="javascript: void(0);">Избранное</a>
              <a class="aside__menu-link aside__menu-link-borderbottom" href="javascript: void(0);">Сравнение</a>
              <p class="aside__menu-link-opened">Мои данные</p>
              <a class="aside__menu-link" href="/personal/history-order.php">История заказов</a>
              <a class="aside__menu-link" href="javascript: void(0);">Бонусные баллы</a><a class="aside__menu-link" href="javascript: void(0);">Выход</a>
            </div>
          </aside>
          <main class="mydetails__main-wrapper">
            <div class="mydetails__main-toptext">
              в этом разделе вы можете отредактировать
              свои контактные данные или изменить пароль для входа:
            </div>
            <form class="mydetails__form" action="">
              <div class="mydetails__form-wrapper-flex">
                <div class="mydetails__inputemail-wrapper"><span class="mydetails__inputemail-text">Ваш E-mail</span>
                  <input class="mydetails__inputemail-input" readonly value="<?=$user['EMAIL']?>" type="text">
                </div>
                <div class="mydetails__inputfirstname-wrapper"><span class="mydetails__inputfirstname-text">Имя</span>
                  <input class="mydetails__inputfirstname-input" value="<?=$user['NAME']?>" type="text">
                </div>
                <div class="mydetails__inputlastname-wrapper"><span class="mydetails__inputlastname-text">Фамилия</span>
                  <input class="mydetails__inputlastname-input" value="<?=$user['LAST_NAME']?>" type="text">
                </div>
                <div class="mydetails__inputbirthday-wrapper"><span class="mydetails__inputbirthday-text">Дата рождения</span>
                  <input class="mydetails__inputbirthday-input" value="<?=$user['PERSONAL_BIRTHDAY']?>" type="text">
                </div>
                <div class="mydetails__inputgender-wrapper"><span class="mydetails__inputgender-text">Пол</span>
                  <div class="mydetails__radiogroup-wrapper-flex">
                    <div class="mydetails__radiogroup-wrapper-male">
                      <input type="radio" value="M" class="gen" <?if($user['UF_GEN']=='M')echo "checked";?> name="group1" id="r1">
                      <label for="r1">&nbsp;Мужской&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    </div>
                    <div class="mydetails__radiogroup-wrapper-female">
                      <input type="radio" value="F" class="gen" <?if($user['UF_GEN']=='F')echo "checked";?> name="group2" id="r2">
                      <label for="r2">&nbsp;Женский</label>
                    </div>
                  </div>
                </div>
                <div class="mydetails__inputphone-wrapper"><span class="mydetails__inputphone-text">Телефон</span>
                  <input class="mydetails__inputphone-input" value="<?=$user['PERSONAL_PHONE']?>" type="text">
                </div>
              </div>
              <div class="mydetails__deliveryaddress-text">Адрес доставки:</div>
              <div class="mydetails__deliveryaddress-inputs-wrapper-flex">
                <div class="mydetails__deliveryaddress-inputstreet-wrapper">
                  <input class="mydetails__deliveryaddress-inputstreet" value="<?=$user['UF_STREET']?>" type="text" placeholder="Улица">
                </div>
                <div class="mydetails__deliveryaddress-inputhouse-wrapper">
                  <input class="mydetails__deliveryaddress-inputhouse" value="<?=$user['UF_BUILD']?>" type="text" placeholder="Дом">
                </div>
                <div class="mydetails__deliveryaddress-inputblock-wrapper">
                  <input class="mydetails__deliveryaddress-inputblock" value="<?=$user['UF_CORP']?>" type="text" placeholder="Корпус">
                </div>
                <div class="mydetails__deliveryaddress-inputapartment-wrapper">
                  <input class="mydetails__deliveryaddress-inputapartment" value="<?=$user['UF_APPART']?>" type="text" placeholder="Квартира">
                </div>
                <div class="mydetails__deliveryaddress-buttondelete-wrapper">
                  <input class="mydetails__deliveryaddress-buttondelete" type="button" value="Удалить">
                </div>
              </div>
              <div class="mydetails__deliveryaddress-button-add-wrapper">
                <button class="mydetails__deliveryaddress-button-add">Добавить еще 1 адрес доставки</button>
              </div>
              <div class="mydetails__deliveryaddress-emaildispatch-wrapper">
                <div class="mydetails__deliveryaddress-emaildispatch-title-wrapper">E-mail рассылки:</div>
                <div class="mydetails__deliveryaddress-emaildispatch-checkboxlabel-wrapper">
                  <input type="checkbox" name="checkbox-dispatch" id="checkbox-dispatch" value="true" checked>
                  <label for="checkbox-dispatch">&nbsp;Полезная информация, новинки и скидки для своих!</label>
                </div>
                <div class="mydetails__deliveryaddress-emaildispatch-button-wrapper">
                  <button class="mydetails__deliveryaddress-emaildispatch-button">Сохранить</button>
                </div>
              </div>
              <div class="mydetails__deliveryaddress-emailchange-wrapper">
                <div class="mydetails__deliveryaddress-emailchange-title-wrapper">Изменить E-mail:</div>
                <div class="mydetails__deliveryaddress-emailchange-subtitle-wrapper">Введите новый E-mail:</div>
                <div class="mydetails__deliveryaddress-emailchange-inputtext-wrapper-flex">
                  <div class="mydetails__deliveryaddress-emailchange-input-wrapper">
                    <input id="changemail" type="text" placeholder="">
                  </div>
                  <div class="mydetails__deliveryaddress-emailchange-text-wrapper">
                    На указанный вами электронный адрес будет выслано письмо со ссылкой,
                    перейдите по ней для подтверждения адреса электронной почты
                  </div>
                </div>
                <div class="mydetails__deliveryaddress-emailchange-buttonsend-wrapper">
                  <button class="mydetails__deliveryaddress-emailchange-buttonsend">Выслать подтверждение</button>
                </div>
              </div>
              <div class="mydetails__passwordchange-wrapper">
                <div class="mydetails__passwordchange-title-wrapper">Изменить пароль:</div>
                <div class="mydetails__passwordchange-wrapper-flex">
                  <div class="mydetails__passwordchange-inputtextnewpassword">Новый пароль
                    <input class="mydetails__passwordchange-input" type="password">
                  </div>
                  <div class="mydetails__passwordchange-confirmnewpassword">Подтвердите новый пароль
                    <input class="mydetails__confirmpassword-input" type="password">
                  </div>
                </div>
                <div class="mydetails__passwordchange-button-wrapper">
                  <input class="mydetails__passwordchange-button" type="button" value="Изменить пароль">
                </div>
              </div>
            </form>
          </main>
        </div>
      </div>
    </div>