document.addEventListener('DOMContentLoaded',()=>{
	var data = {"AJAX" : "CHANGE"};
	document.querySelector('.mydetails__inputfirstname-input').addEventListener('change',(e)=>{
		data['NAME'] = e.target.value;
	});
	document.querySelector('.mydetails__inputlastname-input').addEventListener('change',(e)=>{
		data['LAST_NAME'] = e.target.value;
	});
	document.querySelector('.mydetails__inputphone-input').addEventListener('change',(e)=>{
		data['PERSONAL_PHONE'] = e.target.value;
	});
	document.querySelector('.mydetails__inputbirthday-input').addEventListener('change',(e)=>{
		data['DATE'] = e.target.value;
	});
	document.querySelector('.mydetails__deliveryaddress-inputstreet').addEventListener('change',(e)=>{
		data['UF_STREET'] = e.target.value;
	});
	document.querySelector('.mydetails__deliveryaddress-inputhouse').addEventListener('change',(e)=>{
		data['UF_BUILD'] = e.target.value;
	});
	document.querySelector('.mydetails__deliveryaddress-inputapartment').addEventListener('change',(e)=>{
		data['UF_CORP'] = e.target.value;
	});
	document.querySelector('.mydetails__inputbirthday-input').addEventListener('change',(e)=>{
		data['UF_APPART'] = e.target.value;
	});
	document.querySelectorAll('.gen').forEach((element,index)=>{
		element.addEventListener('click',(e)=>{
			document.querySelectorAll('.gen').forEach((elem,inde)=>{
				elem.checked = false;
			});
			e.target.checked=true;
			data['UF_GEN']= e.target.value
		});
	});
	document.querySelector('.mydetails__deliveryaddress-emaildispatch-button').addEventListener('click',(e)=>{
		e.preventDefault();
		data['subscribe'] = document.querySelector('#checkbox-dispatch').checked;
		console.log(data);
		$.ajax({data : data}).then((response)=>{alert('успешно');});
	});
	document.querySelector('.mydetails__deliveryaddress-emailchange-buttonsend').addEventListener('click',(e)=>{
		e.preventDefault();
		data = {"AJAX" : "CHANGE","EMAIL" : document.querySelector('#changemail').value};
		console.log(data);
		$.ajax({data : data}).then((response)=>{alert('Высланно письмо на почту!')});
	});
	document.querySelector('.mydetails__passwordchange-button').addEventListener('click',()=>{
		data = {"AJAX" : "CHANGE","PASSWORD" : document.querySelector('.mydetails__passwordchange-input').value,
		"CONFIRM_PASSWORD" : document.querySelector('.mydetails__confirmpassword-input').value};
		if(data['PASSWORD']==data['CONFIRM_PASSWORD'])
		{
			$.ajax({data : data}).then((response)=>{return response;}).then((data)=>{
				if(data.STATE)
					alert('успешно');
				else
					alert('ошибка');
			});
		}else
		{
			alert('неправильно!');
		}	
	});
});