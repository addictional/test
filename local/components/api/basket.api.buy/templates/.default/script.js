document.addEventListener('DOMContentLoaded', ()=>{
	let deleter = document.querySelectorAll('a.delete');
	let plus = document.querySelectorAll('.plus');
	let minus = document.querySelectorAll('.minus');
	let totalDiscount = document.querySelector('#totalDiscount');
	let totalPrice = document.querySelector('#totalPrice');
	let paymentBlocks = document.querySelectorAll('.payment-target');
	let deliveryBlocks = document.querySelectorAll('.select-delivery');
	let confirmOrder = document.querySelector('#confirm-order');
	document.querySelector('#email').onchange=checkEmailHandler;
	document.querySelector('#discount').onchange=inputDiscountCoupon;
	var street = "";
	var email;
	var phone;
	var user_name;
	var description;
	var paymentService;
	var deliveryService;
	let deleterLength = deleter.length;
	 deleter.forEach((element,index)=>{
		element.addEventListener('click', (event)=>{
			event.preventDefault();
			let deleteThis=event.target.getAttribute('data-product');
			$.ajax({data : {'delete': deleteThis}}).then((response)=>{data=JSON.parse(response);
			if(data.STATE)
				{
					element=document.getElementById(data.id);
					element.parentNode.removeChild(element);
					totalDiscount.innerHTML = data.totalDisc;
					totalPrice.innerHTML = data.totalPrice;
					if((--deleterLength)==0)
						showEmptyBasket();
				}
		})
			.catch((err)=>{console.log("error: "+err);});
		});
	});

	plus.forEach(function(el) {
	  el.addEventListener('click',function(e){
	     e.target.parentElement.children[1].value++;
	     let id = e.target.parentElement.children[1].getAttribute('data-product');
	     $.ajax({data : {'plus': id}}).then((response)=>{data=JSON.parse(response);
			if(data.STATE)
				{
					element=document.querySelector('#price'+data.id);
					element.innerHTML = data.price;
					totalDiscount.innerHTML = data.totalDisc;
					totalPrice.innerHTML = data.totalPrice;
				}
		})
			.catch((err)=>{console.log("error: "+err);});
	  });
	});

	minus.forEach(function(el) {
	  el.addEventListener('click',function(e){
	    let qty = e.target.parentElement.children[1];
	     if(parseInt(qty.value) === 1)
	     {
	       return;
	     } 
	     else 
	     {
	        let count = qty.value--;
	        let id = qty.getAttribute('data-product');
	        $.ajax({data : {'minus': id}}).then((response)=>{data=JSON.parse(response);
			if(data.STATE)
				{
					element=document.querySelector('#price'+data.id);
					element.innerHTML = data.price;
					totalDiscount.innerHTML = data.totalDisc;
					totalPrice.innerHTML = data.totalPrice;

				}
		})
			.catch((err)=>{console.log("error: "+err);});
	     }
	  });
	});
	deliveryBlocks.forEach((element,index)=>{
		element.addEventListener('click',(e)=>{
			deliveryBlocks.forEach((element,index)=>{
				element.checked=false;
			});
			e.target.checked = true;
			deliveryService = e.target.getAttribute('data-id');
			console.log(e.target);
		});
	});
	paymentBlocks.forEach((element,index)=>{
		element.addEventListener('click',(e)=>{
			paymentBlocks.forEach((element,index)=>{
				element.checked=false;
			});
			e.target.checked = true;
			if(e.target.getAttribute('data-id')=='10')
			{
				paymentService = e.target.getAttribute('data-id');
				document.querySelector('.order-pay-details').style.display="block";
			}
			else
			{
				paymentService = e.target.getAttribute('data-id');
				document.querySelector('.order-pay-details').style.display="none";
			}
		});
	});
	confirmOrder.addEventListener('click',(e)=>{
		e.preventDefault();
		let streetCollection = document.querySelectorAll('.user-fields');
		streetCollection.forEach((element,index)=>{
			street +=" "+element.value;
		});
		email = document.querySelector('#email').value;
		user_fio = document.querySelector('#user-fio').value;
		phone = document.querySelector('#user-phone').value;
		description = document.querySelector('textarea').value;
		var city = document.querySelector('#selectCity').options[document.querySelector('#selectCity').selectedIndex].innerText;
		let data = {"AJAX": "Y","CONFIRM_ORDER": "YES","delivery": deliveryService, "payment" : paymentService, "address" : street,
		 "city" : city,"email" : email, "user_fio": user_fio,"phone": phone,"description": description};
		$.ajax({data: data}).then((response)=>{return JSON.parse(response);}).then((data)=>{console.log(data);if(data.redirect)
			{
				window.location.href = "/personal/test.php?ORDER_ID="+data.order_id+"&PAYMENT_ID=3";
			}})
		.catch((err)=>{console.log(err)});
	});
});
function checkEmailHandler(e)
{
	$.ajax({data:{'AJAX':'Y' , 'check': e.target.value}}).then((response)=>{return JSON.parse(response);})
	.then((data)=>{if(data.user_isset)
		{
			document.querySelector('.b-popup-city__02').style.display = "block";
			document.querySelector('.content-city--closed').addEventListener('click',()=>{
				document.querySelector('.b-popup-city__02').style.display = "none";
			});
		}
		else {
			console.log(data.user_isset)
		}}).catch((err)=>{console.log('error: '+err);});
}
function inputDiscountCoupon(e)
{	e.preventDefault();
	$.ajax({data:{'AJAX':'Y', 'discount' : e.target.value}}).then((response)=>{ 
		return JSON.parse(response);
	})
	.then((data)=>{if (data.STATE)
	{
		for(key in data.items)
		{
			if(data.items[key]['BASE_PRICE']!=data.items[key]['PRICE'])
			{
				document.querySelector('#bc3'+key).innerHTML='<p class="ptb">'+data.items[key]['PRICE']+'</p><p class="ptn old-price">'+data.items[key]['BASE_PRICE']+'</p>';
				document.querySelector('#price'+key).innerText=data.items[key]['FINAL_PRICE']+' ₽';
			}
		}
		document.querySelector('#totalDiscount').innerHTML = 'Скидка: '+data.discount+' ₽';
		document.querySelector('#totalPrice').innerHTML = 'Итого: '+data.price+' ₽';
	}else{
		throw new Error(data.STATE);
	}}).catch((err)=>{
		if (err=='Error: false'){
			console.log('неправильный купон');
		}else
		console.log("error: "+err);
	});
}	
function showEmptyBasket()
{
	document.querySelector('#empty-basket-hidden').style.display='block';
	document.querySelector('#show-empty-basket').style.display='none';
}