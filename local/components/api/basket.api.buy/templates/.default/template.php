` <?
 // echo '<pre>'; print_r($arResult); echo '<pre>'; 
 ?> 

<?if($USER->IsAuthorized()){
  $userData = CUser::GetByID(intval($USER->GetID()));
  $user = $userData->Fetch();
  // echo "<pre>";  print_r($user); echo "<pre>";
}?>
<div id="empty-basket-hidden" style="display: none;">
<div class="catalog__bigtitle-wrapper item__head--title">
      <div class="container">
        <div class="catalog__bigtitle-text item__head--title">Корзина</div>
        <div class="empty-bucket"><img src="<?=SITE_TEMPLATE_PATH?>/images/img/empty-b.png">
          <div>
            <p>Ваша корзина пуста.</p><span>Воскользуйтесь поиском или <a href="/catalog/multituly/">каталогом</a>, что бы найти необходимые товары.<br>Если в корзине были товары - <a href="/auth/">войдите в личный кабинет</a>, что бы посмотреть список.</span>
            <a href="/"><button>На главную</button></a>
          </div>
        </div>
      </div>
    </div>
</div>
<div id="show-empty-basket">
<div class="catalog__bigtitle-wrapper item__head--title">
      <div class="container">
        <div class="catalog__bigtitle-text item__head--title">	Корзина</div>
        <div class="in-bucket">
          <div class="in-bucket__head">
            <div class="bh1">
              <p>Товар</p>
            </div>
            <div class="bh2">
              <p>Наименование</p>
            </div>
            <div class="bh3">
              <p>Цена</p>
            </div>
            <div class="bh4">
              <p>Количество</p>
            </div>
            <div class="bh5">
              <p>Общая сумма</p>
            </div>
            <div class="bh6"><a href="" onclick="">Удалить</a></div>
          </div>
          <?foreach($arResult['ITEMS'] as $key => $item):?>
          <div id="<?=$item['ID']?>" class="in-bucket__card">
            <div class="bc1"><img style="width: 150px;" src="<?=$item['IMAGE']?>" alt=""></div>
            <div class="bc2">
              <p class="ptn"><?=$item['NAME']?>, <?=$item['FUNCTIONS']?> функций, <?=$item['ADDICTIONAL_PROPERTY']?></p>
              <p class="ptn">Артикул: <?=$item['VENDOR_CODE']?></p>
            </div>
            <div class="bc3" id="bc3<?=$item['ID']?>">
            	<?if($item['DISCOUNT']):?>
              <p class="ptb"><?=$item['PRICE']?></p>
              <p class="ptn old-price"><?=$item['BASE_PRICE']?></p>
              	<?else:?>
              <p class="ptb"><?=$item['PRICE']?></p>
              	<?endif;?>
            </div>
            <div class="bc4">
              <div class="amount"><a class="minus">-</a>
                <input class="count" data-product="<?=$item['PRODUCT_ID']?>" type="number" value="<?=$item['QUANTITY']?>"><a class="plus">+</a>
              </div>
            </div>
            <div class="bc5">
              <p class="ptb" id="price<?=$item['ID']?>"><?=$item['FINAL_PRICE']?> ₽</p>
            </div>
            <div class="bc6"><a class="delete" data-product="119" href="#"><img data-product="<?=$item['PRODUCT_ID']?>"  src="<?=SITE_TEMPLATE_PATH?>/images/img/close.png" alt=""></a></div>
          </div>
          <?endforeach;?>
          
        </div>
        <div class="in-bucket--bottom">
          <div class="in-bucket--bottom-discount">
            <div class="discount--left"><span><img src="<?=SITE_TEMPLATE_PATH?>/images/img/percentage.png" alt="">Скидки</span>
              <div>
                <input type="text" id="discount" placeholder="Промокод"><a href="#">Активировать</a>
              </div>
              <p>Вы можете воспользоваться промокодом.<br>Цена товара будет пересчитана с учетом скидки.</p>
            </div>
            <div class="discount--right"><span><img src="<?=SITE_TEMPLATE_PATH?>/images/img/advertising.png" alt="">Бонусная карта<span>500 бонусов будет начислено на карту</span></span>
              <div>
                <input type="text" placeholder="Введите 16 цифр"><a href="#">Добавить</a>
              </div>
              <p>Для оплаты заказа бонусами авторизуйтесь<br>или привяжите карту в Личном кабинете.</p>
            </div>
          </div>
          <div class="in-bucket--bottom-pay"><span><img src="/images/img/checklist.png" alt="">К оплате:</span>
            <p id="totalDiscount">Скидка: <?=$arResult['BASE_PRICE']-$arResult['PRICE']?> ₽</p>
            <p id="totalPrice">Итого: <?=$arResult['PRICE']?> ₽</p>
          </div>
        </div>
      </div>
    </div>
    <div class="hr"></div>
    <div class="container">
      <div class="catalog__bigtitle-text item__head--title">  Оформление заказа</div>
      <div class="order">
        <form>
          <div class="order-present">
            <div class="checkbox-block">
              <input class="checkbox" type="checkbox" id="present" value="present">
              <label class="checkmark" for="present">Доставить в подарок</label><a href="#"><img src="/images/img/question.png" alt=""></a>
            </div>
          </div>
          <div class="order-data" <?if($USER->IsAuthorized()) echo 'style="display: none;"';?>>
            <p class="order-data--title">1. Укажите данные получателя:</p>
            <div class="order-data-form--block">
              <div class="order-data-form">Ваш e-mail
                <input type="text" id="email" <?if($USER->IsAuthorized()):?>value ="<?=$USER->GetEmail()?>"<?endif;?> placeholder="Введите e-mail">
              </div>
              <div class="order-data-form">ФИО
                <input id="user-fio" <?if($USER->IsAuthorized()):?>value ="<?=$USER->GetFullName()?>"<?endif;?> type="text" placeholder="Введите ФИО">
              </div>
              <div class="order-data-form">Телефон
                <input id="user-phone" type="text" <?if($USER->IsAuthorized()):?>value ="<?=$user['PERSONAL_PHONE']?>"<?endif;?> placeholder="Введите телефон">
              </div>
            </div>
            <p class="order-data--desc">Статус заказа можно будет отслеживать в личном кабинете.</p>
            <div class="checkbox-block">
              <input class="checkbox" type="checkbox" id="events" value="present">
              <label class="checkmark" for="events">Я хочу получать информацию о новинках, скидках и акциях</label>
            </div>
          </div>
          <div class="order-delivery">
            <div class="order-delivery--head">
              <p class="order-delivery--title">2. Выберите город и способ доставки:</p>
              <select id="selectCity">
                <option value="1"><?=$_COOKIE['BITRIX_SM_CITY']?></option>
                <option value="1">Москва</option>
                <option value="1">Монако</option>
              </select><a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/img/question.png" alt=""></a>
            </div>
            <div class="order-delivery--controls">
              <?foreach($arResult['DELIVERY'] as $key => $item):?>
              <div class="delivery-type <?if($item['ACTIVE']!='Y')echo "delivery-type--disabled";?>">
                <label class="radio-input">
                  <input class="select-delivery" data-id="<?=$item['ID']?>" type="radio"><span class="checkmarkR"></span><img src="<?=$item['LOGOTIP']?>"><?=$item['NAME']?><br><?=$item['DESCRIPTION']?> - <?=$item['CONFIG']['MAIN']['PRICE']?> ₽
                </label>
              </div>
              <?endforeach;?>
            </div>
            <div class="order-delivery-adress">
              <p class="order-delivery--title">Адрес доставки:</p>
              <div class="adress-inputs">
                <input class="user-fields" id="user-street" <?if($USER->IsAuthorized()):?>value ="<?=$user['UF_STREET']?>"<?endif;?>  type="text"  placeholder="Улица">
                <input class="user-fields" <?if($USER->IsAuthorized()):?>value ="<?=$user['UF_BUILD']?>"<?endif;?> id="user-house" type="text" placeholder="Дом">
                <input class="user-fields" <?if($USER->IsAuthorized()):?>value ="<?=$user['UF_CORP']?>"<?endif;?> id="user-corp" type="text" placeholder="Корпус">
                <input class="user-fields" <?if($USER->IsAuthorized()):?>value ="<?=$user['UF_APPART']?>"<?endif;?> id="user-appart"type="text" placeholder="Квартира/Офис">
              </div>
            </div>
            <div class="order-delivery">
              <div class="order-delivery--head">
                <p class="order-delivery--title">3. Способ оплаты:</p>
              </div>
              <div class="order-delivery--controls">
                <div class="delivery-type ">
                  <label class="radio-input">
                    <input class="payment-target" data-id="1" type="radio"><span class="checkmarkR"></span><img src="<?=SITE_TEMPLATE_PATH?>/images/img/money.png">Наличные
                  </label>
                </div>
                <div class="delivery-type ">
                  <label class="radio-input">
                    <input class="payment-target" data-id="2" type="radio"><span class="checkmarkR"></span><img src="<?=SITE_TEMPLATE_PATH?>/images/img/wallet.png">Картой<br>при получении
                  </label>
                </div>
                <div class="delivery-type">
                  <label class="radio-input">
                    <input class="payment-target" data-id="3" type="radio"><span class="checkmarkR"></span><img src="<?=SITE_TEMPLATE_PATH?>/images/img/online-shop.png">Онлайн оплата<br>на сайте
                  </label>
                </div>
                <div class="delivery-type">
                  <label class="radio-input">
                    <input class="payment-target" data-id="10" type="radio"><span class="checkmarkR"></span><img src="<?=SITE_TEMPLATE_PATH?>/images/img/bank.png">Безналичный расчет<br>(юр. лицо)
                  </label>
                </div>
              </div>
            </div>
            <div class="order-pay-details" style="display: none;">
              <p class="order-delivery--title">Укажите реквизиты компании или прикрепите файл с реквизитами</p>
              <div class="adress-inputs">
                <input type="text" placeholder="Наименование организации">
                <input type="text" placeholder="ОГРН">
                <input type="text" placeholder="ИНН">
                <input type="text" placeholder="КПП">
              </div>
              <button>Прикрепить файл</button>
            </div>
            <div class="order-comments">
              <div class="comment-text">
                <p class="order-delivery--title">Комментарий к заказу:</p>
                <textarea></textarea>
              </div>
              <div class="submit-block">
                <div class="prices"><img src="<?=SITE_TEMPLATE_PATH?>/images/img/checklist-grey.png">
                  <div><span class="price-left">Стоимость товаров:<span class="price-right">6000 ₽</span></span><span class="price-left">Скидка:<span class="price-right">1500 ₽</span></span><span class="price-left">Бонусов за покупку:<span class="price-right">500 ₽</span></span><span class="price-left">Стоимость доставки:<span class="price-right">0 ₽</span></span></div>
                </div>
                <div class="submit-btn">
                  <div>
                    <p>Итого к оплате:</p>
                    <p>4 500 ₽</p>
                  </div>
                  <button id="confirm-order">Подтвердить заказ</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    </div>
  