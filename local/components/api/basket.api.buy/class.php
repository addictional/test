<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;
use Bitrix\Sale;
use Bitrix\Main\Context;
use Bitrix\Sale\Order;
use Bitrix\Sender\Subscription;
use Bitrix\Main\Mail\Event;

Loader::includeModule('sender');
Loader::includeModule('iblock');
Loader::includeModule("sale");


class BasketAndOrder extends CBitrixComponent
{	
	private $content;
	private $registeredUserID;
	private $order;
	private $basket;
	private $selectedDeliveryService ;
	private $deliveryServices =array();
	private $name, $email, $phone, $city, $street, $zip;
	public function __construct($component)
	{
		parent::__construct($component);
		$this->content = Context::getCurrent()->getSite();
		$this->registeredUserID = $this->initUser();
		$this->order=Order::create($this->content, $this->registeredUserID);
		$this->basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), $this->content);
		$this->order->setPersonTypeId(1);
		$this->order->setBasket($this->basket);
		$this->arResult['PRICE'] = $this->basket->getPrice();
		$this->arResult['BASE_PRICE'] = $this->basket->getBasePrice();
	}
	public function executeComponent()
	{	
		global $APPLICATION;
		Sale\DiscountCouponsManager::init();
		$this->getItems();
		$this->deleteItemAction();
		$this->plusQuantityAction();
		$this->minusQuantityAction();
		$this->showDeliveryServices();
		if(isset($_REQUEST['AJAX'])&&$_REQUEST['AJAX']=='Y')
		{
			if(isset($_REQUEST['check']))
			{
				$this->findUser();
			}elseif (isset($_REQUEST['discount'])) {
				$this->setDiscount($_REQUEST['discount']);
			}elseif(isset($_REQUEST['CONFIRM_ORDER']))
			{
				$this->initOrder();
			}

		}
		else
		{
			if($this->arResult['BASE_PRICE']==0)
			$this->IncludeComponentTemplate('empty-basket');
			else
			$this->IncludeComponentTemplate();	
		}
		
	}
	protected function initUser()
	{
        global $USER;
        if ($USER->getId())
            return $USER->getId();
        else
            return \CSaleUser::GetAnonymousUserID();
    }
    protected function getItems()
    {
    	if($basketItems = $this->basket->getOrderableItems())
    	{
	    	$ids = array();
			foreach ($basketItems as $key => $item) 
			{
				$ids[] = $item->getProductId();
				$this->arResult['ITEMS'][$item->getProductId()]['ID']=$item->getProductId();
				$this->arResult['ITEMS'][$item->getProductId()]['BASE_PRICE']=$item->getBasePrice();
				$this->arResult['ITEMS'][$item->getProductId()]['PRICE']=$item->getPrice();
				$this->arResult['ITEMS'][$item->getProductId()]['QUANTITY']=$item->getQuantity();
				$this->arResult['ITEMS'][$item->getProductId()]['FINAL_PRICE']=$item->getFinalPrice();
				$this->arResult['ITEMS'][$item->getProductId()]['DISCOUNT']=$item->getDiscountPrice();
				$this->arResult['ITEMS'][$item->getProductId()]['PRODUCT_ID']=$item->getId();
			}
			$arFilter = array(
					'IBLOCK_ID' => 4,
					'ID' =>  $ids
				);
			$arOrder = array(
					'SORT' => 'ASC'
				);
			$arSelect = array('PREVIEW_PICTURE','NAME','ID');
			$elements = CIBlockElement::GetList($arOrder,$arFilter);
			while($item = $elements->GetNextElement())
			{
					// $arResult['ITEMS'] = $item->GetFields(); 
					// $arResult['PROPERTY'] = $item->GetProperties();
				$this->arResult['ITEMS'][$item->GetFields()['ID']]['IMAGE'] = CFile::GetPath($item->GetFields()['PREVIEW_PICTURE']);
				$this->arResult['ITEMS'][$item->GetFields()['ID']]['NAME'] = $item->GetFields()['NAME'];
				$this->arResult['ITEMS'][$item->GetFields()['ID']]['VENDOR_CODE'] = $item->GetProperties()['VENDOR_CODE']['VALUE'];
				$this->arResult['ITEMS'][$item->GetFields()['ID']]['FUNCTIONS'] = $item->GetProperties()['FUNCTIONS']['VALUE'];
				$this->arResult['ITEMS'][$item->GetFields()['ID']]['ADDICTIONAL_PROPERTY'] =	$item->GetProperties()['ADDICTIONAL_PROPERTY']['VALUE'];
			}
		}else 
		$this->arResult['basket_empty']=true;	
		
    }
    protected function deleteItemAction(){
    	global $APPLICATION;
    	if(isset($_REQUEST['delete']))
		{
 		$APPLICATION->RestartBuffer();
 		
		$item=$this->basket->getItemById($_REQUEST['delete']);
		if($item==NULL)
		{
			echo json_encode(array("STATE" => false));
		}
		else
		{	
			$data= $item->getProductId();
			$item->delete();
			$this->basket->save();
			$totalDiscount=$this->basket->getBasePrice()-$this->basket->getPrice();
 			$totalDiscount="Скидка: ".$totalDiscount;
 			$totalPrice="Итого: ".$this->basket->getPrice(); 
			echo json_encode(array("STATE" => true,'id' => $data,'totalDisc' => $totalDiscount, 'totalPrice' =>$totalPrice));
		}	
 		
 		die();
 		}
    }
    protected function plusQuantityAction()
    {
    	global $APPLICATION;
    	if(isset($_REQUEST['plus']))
 		{
 			$APPLICATION->RestartBuffer();
 			$item=$this->basket->getItemById($_REQUEST['plus']);
 			if($item==NULL)
 			{
 				echo json_encode(array("STATE" => false));
 			}
 			else
 			{
 				$count = $item->getQuantity();
 				(int)$count++;
 				$item->setField('QUANTITY',$count );
 				$price = $item->getFinalPrice();
 				$price = $price." ₽";
 				$id=$item->getProductId();
 				$this->basket->save();
 				$totalDiscount=$this->basket->getBasePrice()-$this->basket->getPrice();
 				$totalDiscount="Скидка: ".$totalDiscount;
 				$totalPrice="Итого: ".$this->basket->getPrice();  				
 				echo json_encode(array("STATE" => true,'price' => $price,'id' => $id,'totalDisc' => $totalDiscount, 'totalPrice' =>$totalPrice, 'order' => $this->order->getPrice()));
 			}	
 			die();	
 		}
    }
    protected function minusQuantityAction()
    {
    	global $APPLICATION;
    	if($_REQUEST['minus'])
 		{
 			$APPLICATION->RestartBuffer();
 			$item=$this->basket->getItemById($_REQUEST['minus']);
 			if($item==NULL)
 			{
 				echo json_encode(array("STATE" => false));
 			}
 			else
 			{
 				$count = $item->getQuantity();
 				(int)$count--;
 				$item->setField('QUANTITY',$count );
 				$price = $item->getFinalPrice();
 				$price = $price." ₽";
 				$id=$item->getProductId();
 				$this->basket->save();
 				$totalDiscount=$this->basket->getBasePrice()-$this->basket->getPrice();
 				$totalDiscount="Скидка: ".$totalDiscount;
 				$totalPrice="Итого: ".$this->basket->getPrice(); 				
 				echo json_encode(array("STATE" => true,'price' => $price,'id' => $id,'totalDisc' => $totalDiscount, 'totalPrice' =>$totalPrice));
 			}	
 			die();
 		}
 		
    }
    protected function checkDiscount($coupon)
    {
    	$check=	Sale\DiscountCouponsManager::isExist($coupon);
    	if($check)
    		{
    			return true;
    		}
    	else
    		{
    			return false;
    		}
    }
   
    protected function setDiscount($coupon)
    {	
    	global $APPLICATION;
    	$APPLICATION->RestartBuffer();
    	if($this->checkDiscount($coupon))
		{
			$APPLICATION->RestartBuffer();	
			Sale\DiscountCouponsManager::add($coupon);
			$discounts = $this->order->getDiscount();
			$discounts->calculate();
			$this->order=Order::create($this->content, $this->registeredUserID);
			$this->basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), $this->content);
			$this->order->setPersonTypeId(1);
			$this->order->setBasket($this->basket);
			$this->getItems();
			echo json_encode(array("STATE" => true,"price" => $this->order->getPrice(),"discount" => $this->order->getDiscountPrice(),"items"=> $this->arResult['ITEMS']));
			die();
		}
		else
			echo json_encode(array("STATE"=> false));	
		die();
    }
    protected function orderDelivery()
    {
    	global $DB;
    		$shipmentCollection = $this->order->getShipmentCollection();
    			if(isset($_REQUEST['delivery']))
    			{
    				$shipment = $shipmentCollection->createItem(
					Bitrix\Sale\Delivery\Services\Manager::getObjectById(
					intval($_REQUEST['delivery'])));
					
    			}
    			

    		$shipmentItemCollection = $shipment->getShipmentItemCollection();
    		// $shipment->setField('delivery_date',date( 'Y-m-d H:i:s' ));
			// 
    		$shipment->setField('CURRENCY', $this->order->getCurrency());
	    	foreach ($this->order->getBasket()->getOrderableItems() as $item) {
				/**
				 * @var $item \Bitrix\Sale\BasketItem
				 * @var $shipmentItem \Bitrix\Sale\ShipmentItem
				 * @var $item \Bitrix\Sale\BasketItem
				 */
				$shipmentItem = $shipmentItemCollection->createItem($item);
				$shipmentItem->setQuantity($item->getQuantity());
			}
	    	
	    	// print_r($shipment->getDelivery()); 
	    	
    }
    protected function showDeliveryServices()
    {
    	$ids = array(2,20,3,17);
	    foreach ($ids as $key => $value)
            {
	    		$this->deliveryServices[$value] = Sale\Delivery\Services\Manager::getById($value);
	    		$this->deliveryServices[$value]['LOGOTIP'] = CFile::GetPath($this->deliveryServices[$value]['LOGOTIP']);
	    	}
	    	$this->arResult['DELIVERY'] = $this->deliveryServices;
    }

  	protected function orderPaymentSystem()
  	{
  		$paymentCollection = $this->order->getPaymentCollection();
  		$payment = $paymentCollection->createItem(
					Bitrix\Sale\PaySystem\Manager::getObjectById(
						intval($_REQUEST['payment'])
					)
				);
		$payment->setField("SUM", $this->order->getPrice());
		$payment->setField("CURRENCY", $this->order->getCurrency());
		// echo "<pre>"; print_r($payment->getPaymentSystemName()); echo "<pre>";
		$this->order->doFinalAction(true);
        $result = $this->order->save();
  		
  	} 

  	protected function findUser()
  	{
  	global $APPLICATION;
  	global $USER;
  	$APPLICATION->RestartBuffer();
  	$user = CUser::GetByLogin($_REQUEST['check']);
 	if($res = $user->Fetch())
 	{
 		echo json_encode(array('user_isset' => true));
 	}
 	else
 	{
 		echo json_encode(array('user_isset' => false));
 	}
 	die();
  	}

  	protected function getUserFields()
  	{
  		global $USER;
  		if($USER->IsAuthorized())
  		{
  			$arUser = $USER->GetByID(intval($USER->GetID()))
			->Fetch();
 
			if (is_array($arUser)) 
			{
				$fio = $arUser['LAST_NAME'] . ' ' . $arUser['NAME'] . ' ' . $arUser['SECOND_NAME'];
				$fio = trim($fio);
				$this->name = $fio;
				$this->email = $arUser['EMAIL'];
				$this->phone = $arUser['PERSONAL_MOBILE'];
				$this->city = $arUser['PERSONAL_CITY'];
				$this->street = $arUser['PERSONAL_STREET'];
				$this->zip = $arUser['PERSONAL_ZIP'];
			}
		// echo "<pre>"; print_r($arUser); echo "<pre>";

  		}
  		else
  		{
  			$this->name = $_REQUEST['user_fio'];
  			$this->email = $_REQUEST['email'];
  			$this->phone = $_REQUEST['phone'];
  			$this->city = $_REQUEST['city'];
  			$this->street = $_REQUEST['address'];
  			$this->zip = '999999';
  			$this->registration();
  		}
  	}
  	protected function setUserFields()
  	{
  		$propertyCollection = $this->order->getPropertyCollection();
  		$propertyCollection->getPayerName()->setValue($this->name);
  		$propertyCollection->getUserEmail()->setValue($this->email);
  		$propertyCollection->getAddress()->setValue($this->city." ".$this->street);
  		$propertyCollection->getPhone()->setValue($this->phone);
  		$propertyCollection->getDeliveryLocationZip()->setValue($this->zip);
  		$this->order->setField('USER_DESCRIPTION',$_REQUEST['description']);
  		$this->order->getPropertyCollection()->getItemByOrderPropertyId(20)->setValue(date( 'Y-m-d H:i:s' ));
  	}
  	protected function initOrder()
  	{
  		global $APPLICATION;
  		$this->setUserFields();
  		$this->orderDelivery();
		$this->getUserFields();
		$this->orderPaymentSystem();
		if(isset($_REQUEST['EMAIL_SUB'])){
			if($this->addSubscriber())
			{
				echo json_encode(array("STATE"=>true,"SUBSCRIBE"=> true));
			}else
			{
				echo json_encode(array("STATE"=>true,"SUBSCRIBE"=> false));
			}
		}elseif($_REQUEST['payment']=='3')
		{$APPLICATION->RestartBuffer();
					echo json_encode(array("STATE"=>true,"SUBSCRIBE"=> false,"redirect"=>true,"order_id"=>$this->order->getId()));
					die();}
		else
			{$APPLICATION->RestartBuffer();
						echo json_encode(array("STATE"=>true,"SUBSCRIBE"=> false,"redirect"=>false,"order_id"=>$this->order->getId()));
						die();}
  	}
  	protected function addSubscriber()
	{
		if(Subscription::add($_REQUEST['EMAIL_SUB'],array($this->user)))
		{
			return true;
		}
		else
			return false;
	}
	protected function randomPassword()
	{
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 6; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass);
	}
	protected function registration()
	{
		global $USER;
	 	$pass = $this->randomPassword();
		$user = new CUser;
		$arFields = Array(
		  "NAME"              => $this->name,
		  "LAST_NAME"         => $this->name,
		  "EMAIL"             => $this->email,
		  "LOGIN"             => $this->email,
		  "LID"               => "s1",
		  "ACTIVE"            => "Y",
		  "GROUP_ID"          => array(10,11),
		  "PASSWORD"          => $pass,
		  "CONFIRM_PASSWORD"  => $pass
		);

		if($ID = $user->Add($arFields))
		$USER->Authorize($ID,true);
		Event::send(array(
			"EVENT_NAME" => "NEW_USER",
			"LID" => "s1",
			"C_FIELDS" => array(
				"EMAIL" => $this->email,
				"PASSWORD" => $pass
			)
		));
	}
	
}