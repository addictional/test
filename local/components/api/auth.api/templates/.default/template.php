<script src="https://vk.com/js/api/openapi.js?159" type="text/javascript"></script>
<div class="signin__reg-wrapper">
            <form class="signin__reg-form" action="">
              <p class="signin__reg-title">Регистрация</p>
              <p class="signin__reg-undersmall">Заполните все обязательные поля для регистрации</p><span class="signin__reg-name">Имя</span>
              <input class="signin__reg-inputname" type="text"><span class="signin__reg-surname">Фамилия</span>
              <input class="signin__reg-inputsurname" type="text"><span class="signin__reg-email">E-mail</span>
              <input class="signin__reg-inputemail" type="text"><span class="signin__reg-errortext">Ошибка! Не корректный e-mail адрес</span>
              <div class="signin__helpful">
                <input class="signin__reg-checkbox-helpful" type="checkbox" id="helpful" value="helpful">
                <label class="signin__reg-labelforcheckbox" for="helpful">Полезная информация, новинки и скидки!</label>
              </div>
              <div class="signin__reg-submitandtext">
                <input class="signin__reg-submitbutton" type="submit" value="Регистрация">
                <p class="signin__reg-submittext">
                  Нажимая на кнопку, Вы даете согласие на обработку
                  персональных данных и принимаете правила продаж
                  товаров через Интернет-магазин.
                </p>
              </div>
            </form>

          </div>
