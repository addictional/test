<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
  die();?>
<footer class="footer">
      <div class="container">
        <div class="footer__wrapper-grid">
          <div class="footer__container-logo"></div>
          <div class="footer__container-phone"><img class="footer__phone-image" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/phone-call.svg" alt="phone"><span class="footer__phone-number">8 800 123-45-67</span>
            <p class="footer__phone-text">Бесплатно по России</p>
          </div>
          <div class="footer__container-socials"><img class="footer__socials-image footer__socials-image-vkontakte" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/vkontakte.png" alt="vkontakte"><img class="footer__socials-image footer__socials-image-facebook" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/facebook.png" alt="facebook"><img class="footer__socials-image footer__socials-image-instagram" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/instagram.png" alt="instagram"><img class="footer__socials-image footer__socials-image-youtubew" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/youtube.png" alt="youtube"></div>
          <div class="footer__container-title-catalog">Каталог товаров</div>
          <div class="footer__container-title-info">Информация</div>
          <div class="footer__container-title-buyer">Покупателю</div>
          <div class="footer__container-title-acceptforpay">Принимаем к оплате</div>
          <div class="footer__container-catalog"><a class="footer__catalog-link" href="javascript: void(0);">Мультититулы</a><a class="footer__catalog-link" href="javascript: void(0);">Браслеты</a><a class="footer__catalog-link" href="javascript: void(0);">Часы</a><a class="footer__catalog-link" href="javascript: void(0);">Ножи</a><a class="footer__catalog-link" href="javascript: void(0);">Аксессуары</a></div>
          <div class="footer__container-info"><a class="footer__catalog-link" href="javascript: void(0);">О компании</a><a class="footer__catalog-link" href="javascript: void(0);">О бренде</a><a class="footer__catalog-link" href="javascript: void(0);">Бонусная программа</a><a class="footer__catalog-link" href="javascript: void(0);">Наш блог</a><a class="footer__catalog-link" href="javascript: void(0);">Пользовательское соглашение</a><a class="footer__catalog-link" href="javascript: void(0);">Политика конфиденциальности</a></div>
          <div class="footer__container-buyer"><a class="footer__catalog-link" href="javascript: void(0);">Условия доставки</a><a class="footer__catalog-link" href="javascript: void(0);">Условия оплаты</a><a class="footer__catalog-link" href="javascript: void(0);">Обмен и возврат</a><a class="footer__catalog-link" href="javascript: void(0);">Гарантия 25 лет</a><a class="footer__catalog-link" href="javascript: void(0);">Как сделать заказ</a><a class="footer__catalog-link" href="javascript: void(0);">Как отследить заказ</a></div>
          <div class="footer__container-acceptforpay"><img class="footer__image-acceptforpay" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/acceptforpay.png" alt="visa mastercard МИР"></div>
        </div>
      </div>
    </footer>
    <div class="copyright-wrapper">
      <div class="container">
        <div class="copyright-container-grid">
          <div class="copyright__text">Copyright 2013-2018 © ileatherman.ru - официальный дилер Leatherman в России</div>
          <div class="copyright__studio"> <a class="copyright__link" href="javascript: void(0);">СОЗДАНИЕ САЙТА ВЕБ СТУДИЯ WAYTOSTART</a></div>
        </div>
      </div>
    </div>
    
   <div class="b-popup-city--choosecitycity" style="display: none;">
    <div class="b-popup-content-city">
      <div class="pop-up--choosecitycity">
        <div class="pop-up--choosecitycity--head">
          <p class="pop-up--choosecitytitle001">Выберите город</p>
          <button class="choosecitycity__cancel001" src="images/icon/svg/cancel.svg"><svg xmlns="http://www.w3.org/2000/svg"
              xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
              <g>
                <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z" />
              </g>
            </svg></button>
        </div>
        <form method="POST">
          <div class="choosecitycity--city--search">
            <input class="choosecitycity--city" type="text" name="city" placeholder="Введите название вашего города">
            <svg class="choosecitycity--citysvg" height="20" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <path class="a" d="M23.674,20.75,19,16.077a1.124,1.124,0,0,0-.8-.328h-.764a9.747,9.747,0,1,0-1.688,1.687V18.2a1.124,1.124,0,0,0,.328.8l4.674,4.673a1.121,1.121,0,0,0,1.589,0l1.327-1.326A1.13,1.13,0,0,0,23.674,20.75Zm-13.923-5a6,6,0,1,1,6-6A6,6,0,0,1,9.751,15.748Z" /></svg>
            <div class="pop-up__choosecitycity--input">
              <ul class="pop-up__choosecitycity--list">
                <li>Москва</li>
                <li>Питер</li>
                <li>Ростов-на-Дону</li>
                <li>Екатеринбург</li>
                <li>Москва</li>
                <li>Питер</li>
                <li>Ростов-на-Дону</li>
                <li>Екатеринбург</li>
              </ul>
            </div>
          </div>
          <p class="choosecity__maybe"><span class="choosecity__maybe--span">Например:</span> Москва, Санкт-Петербург</p>
          <div class="choosecity__all--list">
            <ul class="choosecity--list">
              <li>Москва</li>
              <li>Екатеринбург</li>
              <li>Ростов-на-Дону</li>
            </ul>
            <ul class="choosecity--list">
              <li>Санкт-Петербург</li>
              <li>Челябинск</li>
              <li>Уфа</li>
            </ul>
            <ul class="choosecity--list">
              <li>Новосибирск</li>
              <li>Омск</li>
            </ul>
          </div>
        </form>
        <p class="choosecity--footer">Если вашего города нет в списке, то воспользуйтесь поиском. Мы доставляем
          товары по всей России.</p>
      </div>
    </div>
  </div>
  <div class="b-popup-city" style="display: none;">
    <div class="pop-up--city">
      <div class="pop-up--cityconteinertitle">
        <p class="pop-up--citytitle">Ваш город- <span class="pop-up--youcity"> Москва?</span></p>
        <button class="choosecitycity__cancelsmall" src="images/icon/svg/cancel.svg">
          <svg xmlns="http://www.w3.org/2000/svg" width="8px" height="9px" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
            <g>
              <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
            </g>
          </svg>
        </button>
      </div>
      <button class="pop-up--yes">Да</button>
      <button class="pop-up--no">Нет, выбрать другой</button>
      <p class="pop-up--cityinfo">От выбранного города зависит цена товара и его наличие.</p>
    </div>
  </div>
  <div class="popup__prodyct-cart" style="display: none;">
    <div class="b-popup-content-city-03">
      <div class="content-city03--container">
        <div class="popup03__container--head">
          <p class="popup03__container--title">МУЛЬТИТУЛ LEATHERMAN CHARGE ALX, 18 ФУНКЦИЙ, КОЖАННЫЙ ЧЕХОЛ</p>
          <button class="choosecitycity__cancel" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/cancel.svg">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 64 64"
              enable-background="new 0 0 64 64">
              <g>
                <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z" />
              </g>
            </svg>
          </button>
        </div>
        <div class="prodyct" >
          <div class="gallery__prodyct--img">
            <div class="prodyct__info--01">
              <div class="small-img">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/arrow.png" class="icon-left" alt="" id="prev-img">
                <div class="small-container">
                  <div id="small-img-roll" style="display: flex;">
                    <div class="inner228">
                      <div class="loader-ajax"></div>
                    </div>
                    <div class="inner228">
                      <div class="loader-ajax"></div>
                    </div>
                    <div class="inner228">
                      <div class="loader-ajax"></div>
                    </div>
                  </div>
                </div>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/arrow.png" class="icon-right" alt="" id="next-img">
                <button class="btn__show--video"><svg version="1.1" class="btn__show--video--svg" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 420 420" xml:space="preserve">
                    <path d="M210,21c104.216,0,189,84.784,189,189s-84.784,189-189,189S21,314.216,21,210S105.784,21,210,21 M210,0
                 C94.031,0,0,94.024,0,210s94.031,210,210,210s210-94.024,210-210S325.969,0,210,0L210,0z" />
                    <path d="M293.909,187.215l-111.818-73.591C162.792,100.926,147,109.445,147,132.545V287.42c0,23.1,15.813,31.647,35.147,18.998
                 L293.86,233.31C313.187,220.647,313.208,199.913,293.909,187.215z M279.006,217.868l-99.295,64.981
                 c-6.44,4.221-11.711,1.372-11.711-6.328V143.437c0-7.7,5.264-10.535,11.697-6.3l99.33,65.366
                 C285.46,206.731,285.453,213.647,279.006,217.868z" />
                  </svg></button>
                <button class="btn__show--3d"><svg version="1.1" class="btn__show--3d--svg" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve">
                    <path d="M199.467,312.427c-4.8,4.48-11.52,6.72-20.16,6.72h0c-4.053,0-7.787-0.533-11.2-1.813
                     c-3.307-1.173-6.187-2.88-8.533-5.013c-2.347-2.133-4.16-4.8-5.44-7.787c-1.28-3.093-1.92-6.4-1.92-10.027H124.48
                     c0,7.68,1.493,14.4,4.48,20.267c2.987,5.867,6.933,10.773,11.947,14.613c5.013,3.947,10.773,6.827,17.387,8.853
                     c6.613,2.133,13.44,3.093,20.693,3.093c7.893,0,15.253-1.067,22.08-3.2c6.827-2.133,12.693-5.333,17.707-9.493
                     c5.013-4.16,8.853-9.28,11.733-15.36c2.773-6.08,4.267-13.013,4.267-20.8c0-4.16-0.533-8.107-1.493-11.947
                     c-1.067-3.84-2.667-7.467-4.8-10.88c-2.24-3.413-5.12-6.4-8.64-9.173c-3.52-2.667-7.893-4.907-12.907-6.72
                     c4.267-1.92,8-4.267,11.2-7.04s5.867-5.76,8-8.853c2.133-3.2,3.733-6.4,4.8-9.813c1.067-3.413,1.6-6.827,1.6-10.133
                     c0-7.787-1.28-14.613-3.84-20.48c-2.56-5.867-6.187-10.773-10.88-14.72s-10.24-6.933-16.853-8.96
                     c-6.827-2.133-14.08-3.093-21.973-3.093c-7.68,0-14.827,1.173-21.333,3.413s-12.053,5.44-16.747,9.493
                     c-4.693,4.053-8.32,8.853-10.987,14.4c-2.667,5.547-3.947,11.627-3.947,18.133h27.733c0-3.627,0.64-6.827,1.92-9.6
                     c1.28-2.88,3.093-5.333,5.333-7.253c2.24-2.027,5.013-3.627,8.107-4.693c3.093-1.067,6.507-1.707,10.133-1.707
                     c8.533,0,14.827,2.24,18.987,6.613c4.16,4.373,6.187,10.56,6.187,18.453c0,3.84-0.533,7.253-1.707,10.347
                     c-1.173,3.093-2.88,5.76-5.227,8s-5.333,3.947-8.747,5.227c-3.52,1.28-7.68,1.92-12.373,1.92h-16.427v21.867h16.427
                     c4.693,0,8.96,0.533,12.693,1.6s6.933,2.667,9.6,5.013c2.667,2.24,4.693,5.12,6.187,8.533c1.387,3.413,2.133,7.467,2.133,12.16
                     C206.933,301.227,204.48,307.84,199.467,312.427z" />
                    <path d="M160.32,458.347C90.667,425.28,40.64,357.547,33.067,277.333h-32C11.947,408.747,121.813,512,256,512
                     c4.8,0,9.387-0.427,14.08-0.747l-81.28-81.387L160.32,458.347z" />
                    <path d="M256,0c-4.8,0-9.387,0.427-14.08,0.747l81.28,81.387l28.373-28.373c69.76,32.96,119.787,100.693,127.36,180.907h32
                     C500.053,103.253,390.187,0,256,0z" />
                    <path d="M361.92,192.747L361.92,192.747c-6.72-7.04-14.827-12.48-24.213-16.32c-9.493-3.84-19.84-5.76-31.253-5.76H256v170.667
                     h48.96c11.84,0,22.507-1.92,32.213-5.76c9.707-3.84,17.92-9.28,24.747-16.32c6.827-7.04,12.16-15.573,15.787-25.493
                     c3.733-9.92,5.547-21.12,5.547-33.493v-8.427c0-12.373-1.92-23.467-5.653-33.493C373.867,208.32,368.64,199.787,361.92,192.747z
                      M353.6,260.373h-0.107c0,8.853-0.96,16.96-3.093,24c-2.027,7.147-5.013,13.12-9.067,18.027s-9.067,8.64-15.147,11.307
                     c-6.08,2.56-13.12,3.947-21.227,3.947h-19.307V194.56h20.8c15.36,0,26.987,4.907,35.093,14.613
                     c8,9.813,12.053,23.893,12.053,42.453V260.373z" />
                  </svg>
                </button>
              </div>
              <div class="show" href="<?=SITE_TEMPLATE_PATH?>/images/masha/image2.png">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/masha/image2.png" id="show-img">
              </div>
            </div>
            <div class="prodyct__info--02">
              <div class="prodyct__info">
                <div class="tools__info"><img src="<?=SITE_TEMPLATE_PATH?>/images/img/tools.png" alt=""><span>17<span></div>
                <div class="case__info"><img src="<?=SITE_TEMPLATE_PATH?>/images/img/case.png" alt="">Кожанный чехол</div>
                <div class="sale-percent__info">-20 %</div>
              </div>
              <div class="button__prodyct--like">
                <button class="btn__like"><svg version="1.1" class="btn__like--svg" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 485 485" xml:space="preserve">
                    <path d="M343.611,22.543c-22.613,0-44.227,5.184-64.238,15.409c-13.622,6.959-26.136,16.205-36.873,27.175
                 c-10.738-10.97-23.251-20.216-36.873-27.175c-20.012-10.225-41.625-15.409-64.239-15.409C63.427,22.543,0,85.97,0,163.932
                 c0,55.219,29.163,113.866,86.678,174.314c48.022,50.471,106.816,92.543,147.681,118.95l8.141,5.261l8.141-5.261
                 c40.865-26.406,99.659-68.479,147.682-118.95C455.838,277.798,485,219.151,485,163.932C485,85.97,421.573,22.543,343.611,22.543z
                  M376.589,317.566c-42.918,45.106-95.196,83.452-134.089,109.116c-38.893-25.665-91.171-64.01-134.088-109.116
                 C56.381,262.884,30,211.194,30,163.932c0-61.42,49.969-111.389,111.389-111.389c35.361,0,67.844,16.243,89.118,44.563
                 l11.993,15.965l11.993-15.965c21.274-28.32,53.757-44.563,89.118-44.563c61.42,0,111.389,49.969,111.389,111.389
                 C455,211.194,428.618,262.884,376.589,317.566z" />
                  </svg></button>
                <button class="btn__comparison">
                  <svg version="1.1" class="btn__comparison--svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                    x="0px" y="0px" viewBox="0 0 490 490" style="enable-background:new 0 0 490 490;" xml:space="preserve">
                    <path d="M117,220H9.9c-5.5,0-9.9,4.4-9.9,9.9v248c0,5.5,4.4,9.9,9.9,9.9H117c5.5,0,9.9-4.4,9.9-9.9v-248
                     C126.9,224.4,122.5,220,117,220z M107.1,468H19.8V239.8h87.4V468H107.1z" />
                    <path d="M298.6,2.2H191.4c-5.5,0-9.9,4.4-9.9,9.9v465.7c0,5.5,4.4,9.9,9.9,9.9h107.2c5.5,0,9.9-4.4,9.9-9.9
                     V12.1C308.5,6.7,304.1,2.2,298.6,2.2z M288.7,468h-87.4V22h87.4V468z" />
                    <path d="M480.1,487.8c5.5,0,9.9-4.4,9.9-9.9V162.6c0-5.5-4.4-9.9-9.9-9.9H373c-5.5,0-9.9,4.4-9.9,9.9v315.2
                     c0,5.5,4.4,9.9,9.9,9.9h107.1V487.8z M382.9,172.5h87.4V468h-87.4V172.5z" />
                  </svg>
                </button>
              </div>
              <div class="buy__item--info">
                <div class="buy-card--info">
                  <div class="buy-card--header">
                    <div>
                      <p class="new-price--info">5000 ₽</p>
                      <p class="old-price--info">6000 ₽</p>
                    </div>
                    <div class="stock__info">
                      <p class="in-stock--info"><img class="in-stock--infoimg" src="<?=SITE_TEMPLATE_PATH?>/images/img/done.png" alt="">В
                        наличии</p>
                      <p class="bonuses__info">+500 бонусов</p>
                    </div>
                  </div>
                  <div class="buy-card--buttons--info">
                    <button class="btn-yellow--info">Добавить в корзину</button>
                    <button class="btn-clear--info">Купить в 1 клик</button>
                  </div>
                  <a class="best-price--info" href="#">Нашли дешевле? Снизим цену!</a>
                </div>
                <div class="shipping__info">
                  <p class="shipping__info--title">Доставкав г.Москва</p>
                  <p class="shipping-desc--info">Самовывоз: сегодня, бесплатно </p>
                  <p class="shipping-desc--info">Пункт выдачи: завтра, бесплатно</p>
                  <p class="shipping-desc--info">Курьером: 1-2 дня, 300 ₽</p>
                  <p class="shipping-desc--info">Почтой России: 5-10 дней, 200 ₽</p>
                </div>
                <div class="shipping__advantages--img">
                  <div class="shipping__advantages--desc">
                    <img class="shipping__advantages--desc-img" src="<?=SITE_TEMPLATE_PATH?>/images/img/delivery-man.png" alt="">
                    <p class="shipping__advantages--desc-text">Быстрая Доставка<p>
                  </div>
                  <div class="shipping__advantages--desc">
                    <img class="shipping__advantages--desc-img" src="<?=SITE_TEMPLATE_PATH?>/images/img/guarantee.png" alt="">
                    <p class="shipping__advantages--desc-text">Быстрая Доставка<p>
                  </div>
                  <div class="shipping__advantages--desc">
                    <img class="shipping__advantages--desc-img" src="<?=SITE_TEMPLATE_PATH?>/images/img/exchange.png" alt="">
                    <p class="shipping__advantages--desc-text">Быстрая Доставка<p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="b-popup-city__02" style="display: none;">
    <div class="b-popup-content-city-02" >
      <div class="content-city02--container">
        <p class="content-city02--title">С указанным электронным адресом уже есть зарегистрированный аккаунт.
          Возможно, вы уже регистрировались на нашем сайте, и не помните пароль, попробуйте воспользоваться формой
          <a href="/auth/" class="content-city--pass">восстановления пароля</a>. <span class="content-city02--info">Это
            позволит не создавать дополнительный аккаунт на нашем сайте.</span></p>
        <button class="content-city--closed">Закрыть</button>
      </div>
    </div>
  </div>
  </body>
</html>