var string = "";
document.addEventListener("DOMContentLoaded", function(event) {
  // Get the element with id="defaultOpenTab" and click on it
  document.getElementById("defaultOpenTab").click();
});
var array = {};
// Pop-up on click
function showCatPop() {
  var navList = document.getElementById("catPop");
  if (navList.style.transform === "translateY(0px)") {
    navList.style.transform = "translateY(-2000px)";
  } else {
    navList.style.transform = "translateY(0px)";
  }
}

// Tabs
function openTab(evt, openTab) {
  // Declare all variables
  var i, tabContent, tabLink;
 evt.preventDefault();
  // Get all elements with class="tabcontent" and hide them
  tabContent = document.getElementsByClassName("tabContent");
  for (i = 0; i < tabContent.length; i++) {
    tabContent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tabLink = document.getElementsByClassName("tabLink");
  for (i = 0; i < tabLink.length; i++) {
    tabLink[i].className = tabLink[i].className.replace(" tabActive", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(openTab).style.display = "block";
  evt.target.className += " tabActive";
}
document.addEventListener('DOMContentLoaded', () => {
  var tabsDesc = document.getElementsByClassName("openTabsDesc");

var i;

for (i = 0; i < tabsDesc.length; i++) {
  tabsDesc[i].addEventListener("click", function() {
    let block = this.nextElementSibling;
    if (block.style.display === "block") {
      block.style.display = "none";
    } else {
      block.style.display = "block";
    }
  });
}

// amount
// var plus = document.querySelectorAll('.plus');
// var minus = document.querySelectorAll('.minus');

// plus.forEach(function(el) {
//   el.addEventListener('click',function(e){
//      e.target.parentElement.children[1].value++;
//   });
// });

// minus.forEach(function(el) {
//   el.addEventListener('click',function(e){
//     let qty = e.target.parentElement.children[1];
//      if(parseInt(qty.value) === 1){
//        return;
//      } else {
//         let count = qty.value--;
//      }
//   });
// });


// show/hide block on click

var links = document.getElementsByClassName("sectLink");

var i;

for (i = 0; i < links.length; i++) {
  links[i].addEventListener("click", function() {
    let block = this.nextElementSibling;
    if (block.style.display === "block") {
      block.style.display = "none";
    } else {
      block.style.display = "block";
    }
  });
}
});
// show/hide block on click


// dm
$(document).ready(function() {
  var $butt = $("#hcb"),
    $menu = $("#menu"),
    $ws5 = $(".header__whitespace5"),
    $ws6 = $(".header__whitespace6"),
    $hg = $(".header-catalog"),
    $hs = $(".header-conditions"),
    $hmc = $("#hmc"),
    $cob = $("#ordercall-button-close"),
    $oo = $(".ordercall-overlay"),
    $dcl = $(".header-problem__link-dont-call");

  $butt.on("click", function() {
    $menu.toggleClass("menu-visible");
    $ws5.toggleClass("header-bottomline-clicked");
    $ws6.toggleClass("header-bottomline-clicked");
    $hg.toggleClass("header-bottomline-clicked");
    $hs.toggleClass("header-bottomline-clicked");
    $butt.toggleClass("header-catalog-button-clicked");
  });

  $cob.on("click", function() {
    $oo.hide();
    console.log("clicked");
  });

  $dcl.on("click", function() {
    $oo.show();
    console.log("clicked");
  });

  $hmc.on("click", function() {
    $menu.removeClass("menu-visible");
    $ws5.removeClass("header-bottomline-clicked");
    $ws6.removeClass("header-bottomline-clicked");
    $hg.removeClass("header-bottomline-clicked");
    $hs.removeClass("header-bottomline-clicked");
    $butt.removeClass("header-catalog-button-clicked");
  });

  $(".slick-novelties").slick({
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    prevArrow: $(".novelties__buttons-button-back"),
    nextArrow: $(".novelties__buttons-button-forward"),
    responsive: [
      {
        breakpoint: 1240,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: false
          // centerMode: true,
          // centerPadding: '60px'
        }
      },
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          // centerMode: true,
          // centerPadding: '20px',
          dots: false
        }
      }
    ]
  });

  $(".slick-novelties-second").slick({
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    prevArrow: $(".novelties__buttons-button-back-second"),
    nextArrow: $(".novelties__buttons-button-forward-second"),
    responsive: [
      {
        breakpoint: 1240,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: false
          // centerMode: true,
          // centerPadding: '60px'
        }
      },
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          // centerMode: true,
          // centerPadding: '20px',
          dots: false
        }
      }
    ]
  });

  $(".blog__story-slick").slick({
    dots: false,
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    prevArrow: $(".blog-wrapper__buttons-button-back"),
    nextArrow: $(".blog-wrapper__buttons-button-forward"),
    responsive: [
      {
        breakpoint: 1240,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
          // centerMode: true,
          // centerPadding: '50px'
        }
      },
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: "10px",
          dots: false
        }
      }
    ]
  });

  $(".youwatched__slick").slick({
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 2,
    prevArrow: $(".youwatched-wrapper__buttons-button-back"),
    nextArrow: $(".youwatched-wrapper__buttons-button-forward"),
    responsive: [
      {
        breakpoint: 1240,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false
        }
      }
    ]
  });
});

$( function() {
$( "#sliderRange" ).slider({
  min: 0,
  max: 1000,
  values: [ 0, 1000 ],
  range: true,
  stop: function(event, ui) {
		jQuery("input#minCost").val(jQuery("#sliderRange").slider("values",0));
		jQuery("input#maxCost").val(jQuery("#sliderRange").slider("values",1));
    },
    slide: function(event, ui){
		jQuery("input#minCost").val(jQuery("#sliderRange").slider("values",0));
		jQuery("input#maxCost").val(jQuery("#sliderRange").slider("values",1));
    },
  slide: function( event, ui ) {
    $( "#amountR" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
  }
});
$( "#amountR" ).val( "$" + $( "#sliderRange" ).slider( "values", 0 ) +
  " - $" + $( "#sliderRange" ).slider( "values", 1 ) );
});

jQuery("input#minCost").change(function(){
	var value1=jQuery("input#minCost").val();
	var value2=jQuery("input#maxCost").val();

    if(parseInt(value1) > parseInt(value2)){
		value1 = value2;
		jQuery("input#minCost").val(value1);
	}
	jQuery("#sliderRange").slider("values",0,value1);
});


jQuery("input#maxCost").change(function(){
	var value1=jQuery("input#minCost").val();
	var value2=jQuery("input#maxCost").val();

	if (value2 > 1000) { value2 = 1000; jQuery("input#maxCost").val(1000)}

	if(parseInt(value1) > parseInt(value2)){
		value2 = value1;
		jQuery("input#maxCost").val(value2);
	}
	jQuery("#sliderRange").slider("values",1,value2);
});



$( function() {
$( "#sliderRangeRR" ).slider({
  min: 0,
  max: 1000,
  values: [ 0, 1000 ],
  range: true,
  stop: function(event, ui) {
		jQuery("input#minCostRR").val(jQuery("#sliderRangeRR").slider("values",0));
		jQuery("input#maxCostRR").val(jQuery("#sliderRangeRR").slider("values",1));
    },
    slide: function(event, ui){
		jQuery("input#minCostRR").val(jQuery("#sliderRangeRR").slider("values",0));
		jQuery("input#maxCostRR").val(jQuery("#sliderRangeRR").slider("values",1));
    },
  slide: function( event, ui ) {
    $( "#amountRR" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
  }
});
$( "#amountRR" ).val( "$" + $( "#sliderRangeRR" ).slider( "values", 0 ) +
  " - $" + $( "#sliderRangeRR" ).slider( "values", 1 ) );
});

jQuery("input#minCostRR").change(function(){
	var value1=jQuery("input#minCostRR").val();
	var value2=jQuery("input#maxCostRR").val();

    if(parseInt(value1) > parseInt(value2)){
		value1 = value2;
		jQuery("input#minCostRR").val(value1);
	}
	jQuery("#sliderRangeRR").slider("values",0,value1);
});


jQuery("input#maxCostRR").change(function(){
	var value1=jQuery("input#minCostRR").val();
	var value2=jQuery("input#maxCostRR").val();

	if (value2 > 1000) { value2 = 1000; jQuery("input#maxCostRR").val(1000)}

	if(parseInt(value1) > parseInt(value2)){
		value2 = value1;
		jQuery("input#maxCostRR").val(value2);
	}
	jQuery("#sliderRangeRR").slider("values",1,value2);
});
document.addEventListener('DOMContentLoaded',()=>{
  var buttons=document.querySelectorAll('.item__buttons-inbasket');
  var basket=document.querySelector('a.header-options__link-basket').innerHTML.match(/\d+/)[0];
  buttons.forEach(function(element,index,){
    element.addEventListener('click',function(e){     
        $.ajax({
        url: e.target.getAttribute('data-basket'),
        beforeSend: function(){
          e.target.setAttribute('disabled','');
        },
        success: function(data){
          basket++;
          document.querySelector('a.header-options__link-basket').innerHTML="Корзина ("+basket+")";
          e.target.removeAttribute('disabled');
        }
      });
      
    });
  });
  document.querySelector('.btn-yellow--info').addEventListener('click',(e)=>{
  		$.ajax({url: e.target.getAttribute('data-basket')})
  });
  document.querySelector('.btn-clear--info').addEventListener('click',(e)=>{
  		$.ajax({url: e.target.getAttribute('data-basket')}).then((resp)=>{location = "/personal/cart/"});
  });
  document.querySelector('.choosecitycity__cancel').addEventListener('click',(e)=>{
    document.querySelector('.popup__prodyct-cart').style.display="none";
  });
  document.querySelector('.header-city__link-chosen-city').addEventListener('click',()=>{
    document.querySelector('.b-popup-city--choosecitycity').style = "display: block;";
  });
  document.querySelector('.choosecitycity__cancel001').addEventListener('click',()=>{
    document.querySelector('.b-popup-city--choosecitycity').style = "display: none;";
  });
  document.querySelector('.choosecitycity--city').addEventListener('change',(e)=>{
    Cookies.set('BITRIX_SM_CITY', e.target.value,{domain: 'knifes.infowaf4.beget.tech'});
  });
  document.querySelector('.choosecity__all--list').addEventListener('click',(e)=>{
    if(e.target.localName=="li")
    {
      document.querySelector('.choosecitycity--city').value = e.target.innerText;
      Cookies.set('BITRIX_SM_CITY', e.target.innerText,{domain: 'knifes.infowaf4.beget.tech'});
    }
  });
  document.querySelector('#small-img-roll').addEventListener('click',(e)=>{
  	document.querySelector('#show-img').setAttribute('src',e.target.getAttribute('src'));
  });
  document.querySelector('.pop-up__choosecitycity--list').addEventListener('click',(e)=>{
    if(e.target.localName=="li")
    {
    document.querySelector('.choosecitycity--city').value = e.target.innerText;
    Cookies.set('BITRIX_SM_CITY', e.target.innerText,{domain: 'knifes.infowaf4.beget.tech'});
    console.log(e.target.offsetParent.style.display = "none");
    }
  });
  if(typeof Cookies.get('checked') == 'undefined'){
    setTimeout(()=>{document.querySelector('.b-popup-city').style.display="block";},6000);
    document.querySelector('.pop-up--yes').addEventListener('click',()=>{
      Cookies.set('checked', 'yes',{expires: 30,domain: 'knifes.infowaf4.beget.tech'});
      document.querySelector('.b-popup-city').style.display="none";
    });
    document.querySelector('.pop-up--no').addEventListener('click',()=>{
      Cookies.set('checked', 'yes',{expires: 30,domain: 'knifes.infowaf4.beget.tech'});
      document.querySelector('.b-popup-city').style.display="none";
      document.querySelector('.b-popup-city--choosecitycity').style = "display: block;";
    });
    document.querySelector('.choosecitycity__cancelsmall').addEventListener('click',()=>{
      Cookies.set('checked', 'yes',{expires: 30,domain: 'knifes.infowaf4.beget.tech'});
      document.querySelector('.b-popup-city').style.display="none";
    });
  }
  document.querySelector('.choosecitycity--city').addEventListener('input',(e)=>{
      $.ajax({url: ' http://kladr-api.ru/api.php',
        data: {'query': e.target.value,'contentType': 'city','limit':3},
        crossDomain: true,
        dataType: 'jsonp',
    }).then((response)=>{return response['result'];})
      .then((data)=>{
        var string = "";
        data.forEach((element,index)=>{
       
       console.log(element['name']);
       string +=" <li>"+element['name']+"</li>";
      });
      document.querySelector('.pop-up__choosecitycity--list').innerHTML = string;
       document.querySelector('.pop-up__choosecitycity--input').style.display = "block";
    })
  });
  
});

// document.addEventListener('DOMContentLoaded',()=>{
//   var questioninfo = document.querySelector(".btn-clear--info-01");
// var popupquestion = document.querySelector(".shipping__info-popup");
// var closepopupinfo = document.querySelector(".shipping__info-popup--svg");

// questioninfo.addEventListener("click", function (evt) {
//   evt.preventDefault();
//   popupquestion.classList.add(".shipping__info-popup--show");
// });

// closepopupinfo.addEventListener("click", function (evt) {
//   evt.preventDefault();
//   popupquestion.classList.remove(".shipping__info-popup--show");
// });
// });