<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="blog__bigtitle-wrapper">
      <div class="container">
        <div class="blog__bigtitle-text">	Блог</div>
      </div>
    </div>
    <div class="blog__allpage-wrapper">
      <div class="container">
        <div class="blog__bothsides-wrapper">
          <aside class="blog__menu-wrapper">
            <div class="blog__menu-container-type"><a class="blog__menu-link" href="javascript: void(0);">О компании</a><a class="blog__menu-link" href="javascript: void(0);">О бренде</a><a class="blog__menu-link" href="javascript: void(0);">Бонусная программа</a>
              <p class="blog__menu-link-opened">Наш блог</p>
            </div>
          </aside>
          <main class="blog__main-wrapper">
          	<div class="blog__main-wrapper-grid">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<p class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">	
	
<div class="blog__main-container-post" >
                <div class="blog-post-img" ><img class="blog__main-image" id="<?=$this->GetEditAreaId($arItem['ID']);?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"></div>
                <div class="blog__main-container-text">
                  <h2><a class="blog__main-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a></h2>
                  <p class="blog__main-text">
                    <?echo $arItem["PREVIEW_TEXT"];?>
                  </p>
                </div>
              </div>
		
		
<?endforeach;?>



</div>


           
              
              
              
             
              
            </div>
            <!-- <div class="blog__showbutton-wrapper">
              <button class="blog__showbutton-button">Показать еще</button>
            </div>
            <div class="blog__showoptions-container">
              <div class="blog__showoptions-wrapper">
                <button class="blog__showoptions-button-tostart"></button><a class="blog__showoptions-link-disabled" href="javascript: void(0);">1</a><a class="blog__showoptions-link" href="javascript: void(0);">2</a><a class="blog__showoptions-link" href="javascript: void(0);">3</a><span class="blog__showoptions-text-points">........</span><a class="blog__showoptions-link" href="javascript: void(0);">10</a>
                <button class="blog__showoptions-button-next"></button>
              </div>
              <div class="blog__showoptions-displaymode-wrapper"><span class="blog__showoptions-text-show">Показывать по</span>
                <select class="blog__model-select" name="blog-show">
                  <option value="s1">12</option>
                  <option value="s2">48</option>
                  <option value="s3">все</option>
                </select>
              </div>
            </div> -->
          </main>
        </div>
      </div>
    </div>
    <div class="mt105">&nbsp;</div>