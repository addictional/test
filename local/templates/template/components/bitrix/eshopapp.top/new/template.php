<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<? echo "<pre>"; print_r($arResult); echo "</pre>";?>






 <div class="novelties-wrapper-white">
      <div class="container">
        <div class="novelties__labelbuttons-flex">
          <div class="novelties__label">ВЫ СМОТРЕЛИ</div>
          <div class="novelties__buttons">
            <button class="novelties__buttons-button-back"></button>
            <button class="novelties__buttons-button-forward"></button>
          </div>
        </div>
        <div class="slick-novelties">
        	<?foreach($arResult['ITEMS'] as $key => $arItem):?>
        	<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], "Изменить элемент");
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], "Удалить элемент", "Уверены?");?>
          <div class="novelties__item novelties-item-bg-white" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="item-wrapper">
              <div class="item__quantity">
                <div class="item__quantity-container"><img class="item__quantity-image" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/tools.svg" alt="tools"><span class="item__quantity-span"><?=$arItem['PROPERTIES']['FUNCTIONS']['VALUE']?></span></div>
              </div>
              <div class="item__image"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>"
                    	alt="<?=$arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']?>"
                    	title="<?=$arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']?>"></a></div>
               <?if(DateMatch::DifferenceBetween($arItem['DATE_CREATE'])):?>       
              <div class="item__bonus">

                <div class="item__bonus-container">Новинка</div>

              </div>
              
              <?endif;?>
              <div class="item__about"><span class="item__about-text"><?=$arItem['NAME']?>, <?=$arItem['PROPERTIES']['FUNCTIONS']['VALUE']?> <?=NameCounter::rewriter($arItem['PROPERTIES']['FUNCTIONS']['VALUE'],'функции')?><?if($arItem['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']):?>, <?endif;?><?=$arItem['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']?></span></div>
              <div class="item__stars-and-reviews"><img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><a class="item__stars-and-reviews-link" href="javascript: void(0);"><?=$arResult['comment'][$arItem['ID']]?> <?=NameCounter::rewriter($arResult['comment'][$arItem['ID']],'отзыв','','а','ов',0)?></a></div>
              <div class="item__price"><span class="item__price-span"><?=$arItem['ITEM_PRICES'][0]['PRICE']?> Р</span><?if($arItem['ITEM_PRICES'][0]['DISCOUNT']):?><span class="item__price-span-underlined"><?=$arItem['ITEM_PRICES'][0]['BASE_PRICE']?> Р</span><?endif;?></div>
              <div class="item__buttons">
                <button class="item__buttons-inbasket " data-basket='<?=NameCounter::BasketRewriter($arResult['ADD_URL_TEMPLATE'],$arItem['ID'])?>'>В корзину</button>
                <button class="item__buttons-heart"></button>
                <button class="item__buttons-diagram" data-compare="<?=NameCounter::BasketRewriter($arResult['COMPARE_URL_TEMPLATE'],$arItem['ID'])?>"></button>
              </div>
            </div>
          </div>
          <?endforeach;?>
          
          
         
        
          
        </div>
      </div>
    </div>
