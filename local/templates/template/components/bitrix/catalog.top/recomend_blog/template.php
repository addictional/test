<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);


	$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
	$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
	$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCT_ELEMENT_DELETE_CONFIRM'));

	// echo "<pre>"; print_r($arResult); echo "<pre>";


	
?>
<?foreach($arResult['ITEMS'] as $key => $item):?>
<div class="articles__recommend-wrapper"><a class="articles__recommend-item-image-wrapper" href="<?=$item['DETAIL_PAGE_URL']?>"><img src="<?=$item['PREVIEW_PICTURE']['SRC']?>"></a><a class="articles__recommend-item-name-link" href="javascript: void(0);"><?=$item['NAME']?>, 1<?=NameCounter::rewriter($item['PROPERTIES']['FUNCTIONS']['VALUE'],'функции')?><?if($item['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']):?>, <?endif;?><?=$arItem['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']?></a>
              <div class="articles__recommend-cost-wrapper">
                <div class="articles__recommend-item-cost"><?=$item['PRICES']['BASE']['VALUE_VAT']?> Р</div>
                <?if($item['PRICES']['BASE']['DISCOUNT_DIFF']):?><div class="articles__recommend-item-cost-crossed"><?=$item['PRICES']['BASE']['DISCOUNT_VALUE']?> Р</div><?endif;?>
              </div>
            </div>
<?endforeach;?>