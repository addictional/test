<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);


echo "<pre>";  print_r($arResult); echo "<pre>";
?>

<div class="favorites__bigtitle-wrapper">
    <div class="container">
      <div class="favorites__bigtitle-text"> Избранное</div>
    </div>
  </div>
  <div class="favorites__big-wrapper">
    <div class="container">
      <div class="favorites__wrapper-flex">
        <aside class="aside__menu-wrapper">
          <div class="aside__menu-container-type"><a class="aside__menu-link" href="javascript: void(0);">Главная</a><a
              class="aside__menu-link" href="javascript: void(0);">Моя корзина</a><a class="aside__menu-link aside__menu-link-borderbottom"
              href="javascript: void(0);">Мои доставки</a>
            <p class="aside__menu-link-opened">Избранное</p><a class="aside__menu-link" href="javascript: void(0);">Сравнение</a><a
              class="aside__menu-link" href="javascript: void(0);">Мои данные</a><a class="aside__menu-link" href="javascript: void(0);">История
              заказов</a><a class="aside__menu-link" href="javascript: void(0);">Бонусные баллы</a><a class="aside__menu-link"
              href="javascript: void(0);">Выход</a>
          </div>
        </aside>
        <main class="favorites__main-wrapper">
          <div class="favorites__main-titlebutton-wrapper-flex">
            <div class="favorites__main-title-wrapper">Здесь вы можете посмотреть отложенные товары</div>
            <div class="favorites__main-buttondelete-wrapper">
              <button>Удалить</button>
            </div>
          </div>
          <table class="postponed-table">
            <tr class="postponed-table__header">
              <th>Товар</th>
              <th>Название</th>
              <th>Кол-во</th>
              <th>Общая сумма</th>
              <th>Удалить</th>
            </tr>
            <?foreach($arResult['ITEMS'] as $key => $item):?>
            <tr>
              <td class="postponed-table__images"><img class="postponed-table__images" src="<?=$item['PREVIEW_PICTURE']['SRC']?>"></td>
              <td class="postponed-table__name">
                <p class="postponed-table__description"><?=$item['NAME']?><?=NameCounter::rewriter($item['PROPERTIES']['FUNCTIONS']['VALUE'],'функции')?><?if($item['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']):?>, <?endif;?><?=$item['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']?></p>
                <button class="postponed-table__btn-add-cart" data-basket="<?=$item['ADD_URL']?>">В корзину</button>
              </td>
              <td class="postponed-table__stock">
                <div class="amount-res"><a class="minus">-</a>
                  <input class="count" type="number" value="1"><a class="plus">+</a></div>
              </td>
              <td>
                <?if($item['PRICES']['BASE']['D']):?><p class="postponedtable__old-price"><?=$item['PRICES']['BASE']['VALUE_VAT']?> Р</p><?endif;?>
                <p class="postponed-table__new-price"><?=$item['PRICES']['BASE']['DISCOUNT_VALUE']?> Р</p>
              </td>
              <td><button class="postponed-table__btn-delete"></button></td>
            </tr>
            <?endforeach;?>
          </table>
          <div class="favorites__count-wrapper-flex">
            <div class="favorites__countproduct-wrapper"><b>Всего товаров:&nbsp;&nbsp;</b><?=count($arResult['ELEMENTS'])?></div>
            <div class="favorites__counttotal-wrapper"><b>Итого: 14 800 Р</b></div>
          </div>
        </main>
      </div>
    </div>
  </div>