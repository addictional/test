<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
// echo "<pre>"; print_r($arResult); echo "<pre>";
?>

<body>
    <main>
        <div class="container">
            <aside class="checkbox01">
                <label>
                    <input class="checkbox01-custom" type="checkbox" checked="checked" name="checkbox-test">
                    <span class="checkbox02-custom"></span>
                    <span class="label01">Показывать только различия</span>
                </label>
            </aside>
            
            <article class="favorites-container__wrapper">
            	<?$i=0?>
            	<?foreach($arResult['ITEMS'] as $key => $arItem):?>
		<aside class="favorites-container__product--wrap">
                    <div class="favorites-container__product--df">
                        <div class="item-wrapper__favorites">
                            <div class="item-quantity__favorites">
                                <div class="item-quantity__container-favorites"><img class="item-quantity__image--favorites"
                                        src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/tools.svg" alt="tools"><span class="item-quantity__favorites-span">17</span></div>
                            </div>
                            <div class="item-image__favorites">
                                <div class="item-image__favorites--img">
                                    <img id="img-top__favorites" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="item-image-img__favorites">
                                </div>
                            </div>
                            <div class="item-bonus__favorites">
                                <div class="item-bonus__favorites-container">Новинка</div>
                            </div>
                            <div class="item-about__favorites"><span class="item-about__favorites--text">Мультититул
                                    Leatherman Charge ALX, 18 функций, кожаный чехол</span></div>
                            <div class="item-stars-and-reviews__favorites">
                            		<?for($i=1;$i<=5;$i++):?>
                            		<?if($i<=$arResult['comment'][$arItem['ID']]['rate']):?>
                            		<img class="item__image-star--favorites" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star">
                            		<?else:?>
                            		<img class="item__image-star--favorites" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star_empty.png" alt="star">
                            		<?endif;?>
                            		<?endfor;?>
                                    <a class="item-stars-and-reviews-link__favorites" href="javascript: void(0);"><?=$arResult['comment'][$arItem['ID']]['count']?> <?=NameCounter::rewriter($arResult['comment'][$arItem['ID']]['count'],'отзыв','','а','ов',0)?></a></div>
                            <div class="item-price__favorites"><span class="item-price__favorites--span">3 900 Р</span><span
                                    class="item-price__favorites--span-underlined">5000</span></div>
                        </div>
                        <button class="item-about__favorites--cancel">Удалить</button>
                        <div class="item-about__favorites--black-line"></div>
                    </div class="">
                    <p class="favorites-specifications">Ключевые характеристики</p>
                    <div class="favorites-specifications__skill">
                        <p class="favorites-delivery">Доставка</p>
                        <p class="favorites-delivery__home">Доставка на дом</p>
                        <p class="favorites-delivery__shop">Забрать в магазине</p>
                    </div>
                    <div class="favorites-specifications__skill">
                        <p class="favorites-delivery__home">Доставка на дом</p>
                        <p class="favorites-delivery__shop">Забрать в магазине</p>
                    </div>
                    <div class="favorites-specifications__skill">
                        <p class="favorites-delivery__home">Доставка на дом</p>
                        <p class="favorites-delivery__shop">Забрать в магазине</p>
                    </div>
                </aside>

            	<?endforeach;?>
            </article>
            <!-- <article class="favorites-container__wrapper">
                <aside class="favorites-container__product--wrap">
                    <div class="favorites-container__product--df">
                        <div class="item-wrapper__favorites">
                            <div class="item-quantity__favorites">
                                <div class="item-quantity__container-favorites"><img class="item-quantity__image--favorites"
                                        src="images/icon/svg/tools.svg" alt="tools"><span class="item-quantity__favorites-span">17</span></div>
                            </div>
                            <div class="item-image__favorites">
                                <div class="item-image__favorites--img">
                                    <img id="img-top__favorites" src="images/novelties__item-image.jpg" class="item-image-img__favorites">
                                </div>
                            </div>
                            <div class="item-bonus__favorites">
                                <div class="item-bonus__favorites-container">Новинка</div>
                            </div>
                            <div class="item-about__favorites"><span class="item-about__favorites--text">Мультититул
                                    Leatherman Charge ALX, 18 функций, кожаный чехол</span></div>
                            <div class="item-stars-and-reviews__favorites"><img class="item__image-star--favorites" src="images/icon/svg/star.svg"
                                    alt="star"><img class="item__image-star--favorites" src="images/icon/svg/star.svg"
                                    alt="star"><img class="item__image-star--favorites" src="images/icon/svg/star.svg"
                                    alt="star"><img class="item__image-star--favorites" src="images/icon/svg/star.svg"
                                    alt="star"><img class="item__image-star--favorites" src="images/icon/svg/star.svg"
                                    alt="star"><a class="item-stars-and-reviews-link__favorites" href="javascript: void(0);">6
                                    отзывов</a></div>
                            <div class="item-price__favorites"><span class="item-price__favorites--span">3 900 Р</span><span
                                    class="item-price__favorites--span-underlined">5000</span></div>
                        </div>
                        <button class="item-about__favorites--cancel">Удалить</button>
                        <div class="item-about__favorites--black-line"></div>
                    </div>
                    <div class="favorites-specifications__skill--info">
                        <div class="favorites-specifications__skill">
                            <p class="favorites-delivery__home">Доставка на дом</p>
                            <p class="favorites-delivery__shop">Забрать в магазине</p>
                        </div>
                        <div class="favorites-specifications__skill">
                            <p class="favorites-delivery__home">Доставка на дом</p>
                            <p class="favorites-delivery__shop">Забрать в магазине</p>
                        </div>
                        <div class="favorites-specifications__skill">
                            <p class="favorites-delivery__home">Доставка на дом</p>
                            <p class="favorites-delivery__shop">Забрать в магазине</p>
                        </div>
                    </div>
                </aside>
            </article> -->
        </div>
    </main>
</body>