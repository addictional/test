<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
 // echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if(isset($_REQUEST['nav-comments'])):?>
<?$APPLICATION->RestartBuffer();?>
<div class="tabContent" id="reviews">
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem dolores harum possimus, sequi explicabo omnis delectus ratione iusto laboriosam ad laborum quam accusamus labore culpa ipsum quia itaque odio nemo! <br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. At neque nihil, unde quos sit tempore sunt quod autem aliquid deleniti numquam ratione rem necessitatibus earum itaque animi eligendi quibusdam corporis?<br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum aliquam, mollitia deleniti minus unde eum dolores sequi, laborum nulla molestiae eligendi sit possimus quibusdam expedita voluptate voluptatum. Laudantium, qui rerum. <br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus eum eos culpa, similique fugiat vel nihil aspernatur ducimus, eligendi aut sed dolorum odio architecto sint libero minima in id doloremque.</p><a class="openTabsDesc">Развернуть<img src="/images/img/arrow-down.png" alt=""></a>
            <div class="hiddenTabsDesc" id="comments">
              <p class="htd__title">Отзывы (<?=$arResult['stars']['count']?>)</p>
              <div class="htd__reviews">
                <div class="reviews__head">
                 <div class="reviews__head--left">
                    <div class="rating"><span><img src="/images/img/star-fill.png" alt=""><img src="/images/img/star-fill.png" alt=""><img src="/images/img/star-fill.png" alt=""><img src="/images/img/star-clear.png" alt=""><img src="/images/img/star-clear.png" alt=""></span>
                      <p><?=$arResult['stars']['rate']?> из 5</p>
                    </div>
                    <div class="rating--line"><span>5<img src="/images/img/star-fill.png" alt=""></span>
                      <div class="line">
                        <div style="width:<?=$arResult['stars_percent'][5]?>%;"></div>
                      </div>
                      <p><?=$arResult['stars'][5]?> <?=NameCounter::rewriter($arResult['stars'][5],'отзыв','','а','ов',0)?></p>
                    </div>
                    <div class="rating--line"><span>4<img src="/images/img/star-fill.png" alt=""></span>
                      <div class="line">
                        <div  style="width:<?=$arResult['stars_percent'][4]?>%;"></div>
                      </div>
                      <p><?=$arResult['stars'][4]?> <?=NameCounter::rewriter($arResult['stars'][4],'отзыв','','а','ов',0)?></p>
                    </div>
                    <div class="rating--line"><span>3<img src="/images/img/star-fill.png" alt=""></span>
                      <div class="line">
                        <div style="width:<?=$arResult['stars_percent'][3]?>%;"></div>
                      </div>
                      <p><?=$arResult['stars'][3]?> <?=NameCounter::rewriter($arResult['stars'][3],'отзыв','','а','ов',0)?></p>
                    </div>
                    <div class="rating--line"><span>2<img src="/images/img/star-fill.png" alt=""></span>
                      <div class="line">
                        <div  style="width:<?=$arResult['stars_percent'][2]?>%;"></div>
                      </div>
                      <p><?=$arResult['stars'][2]?> <?=NameCounter::rewriter($arResult['stars'][2],'отзыв','','а','ов',0)?></p>
                    </div>
                    <div class="rating--line"><span>1<img src="/images/img/star-fill.png" alt=""></span>
                      <div class="line">
                        <div  style="width:<?=$arResult['stars_percent'][1]?>%;"></div>
                      </div>
                      <p><?=$arResult['stars'][1]?> <?=NameCounter::rewriter($arResult['stars'][1],'отзыв','','а','ов',0)?></p>
                    </div>
                  </div>
                  <div class="reviews__head--right">
                    <p><strong>Уже купили этот товар? Напишите о нем отзыв!</strong></p>
                    <p>Ваш отзыв будет полезен и может помочь с выбором</p>
                    <button>Написать отзыв</button>
                  </div>
                </div>
                <div class="reviews__control">
                  <p>Сортировка:</p><a class="control-link--active" href="">По дате<img src="/images/img/down-arrow.svg" alt=""></a><a href="">Оценке</a><a href="">Полезности</a>
                </div>
                <?foreach($arResult['comment'] as $key => $item):?>
                <div class="reviews__comment">
                  <div class="comment--avatar"><span><img src="<?=SITE_TEMPLATE_PATH?>/images/img/avatar.png" alt=""></span>
                    <div>
                      <?for($i=1;$i<=5;$i++):?>
                      <?if($i<=$item['UF_STARS']):?>
                      <img src="<?=SITE_TEMPLATE_PATH?>/images/img/star-fill.png" alt="">
                      <?else:?>
                      <img src="<?=SITE_TEMPLATE_PATH?>/images/img/star-clear.png" alt="">
                      <?endif;?>
                      <?endfor;?>
                    </div>

                  </div>
                  <div class="comment--text">
                    <p class="commentator"><?=$item['UF_AUTHOR']?></p>
                    <p><?=$item['UF_DATE']->toString()?></p>
                    <p><?=$item['UF_COMMENT']?></p><a href=""><img src="<?=SITE_TEMPLATE_PATH?>/images/img/like.png" alt=""></a><a href=""><img src="<?=SITE_TEMPLATE_PATH?>/images/img/dislike.png" alt=""></a>
                  </div>
                </div>
                <?endforeach;?>
               <? $APPLICATION->IncludeComponent("bitrix:main.pagenavigation", "comments", Array(
                      "NAV_OBJECT" => $arResult['comment-nav'] ,
                      "SEF_MODE" => "N"
                                    ),
                                $component
                  );?>
                <!-- <div class="reviews__pagination"><a class="pagination-cnt" href="">В начало</a><a class="pagination-page pagination-page--active" href="">1</a><a class="pagination-page" href="">2</a><a class="pagination-page" href="">3</a><a class="pagination-page" href="">.</a><a class="pagination-page" href="">10</a><a class="pagination-cnt" href="">Следующая</a></div> -->
              </div>
            </div>
          </div>
          <?die();?>
 <?else:?>
    <div class="catalog__bigtitle-wrapper item__head--title">
      <div class="container">
        <div class="catalog__bigtitle-text item__head--title">	<?=$arResult['NAME']?>, <?=$arResult['PROPERTIES']['FUNCTIONS']['VALUE']." ".NameCounter::rewriter($arResult['PROPERTIES']['FUNCTIONS']['VALUE'],функция)?><?if($arResult['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']):?>, <?endif;?><?=$arResult['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']?></div>
        <div class="item--head">
          <div>
            <div class="item__stars-and-reviews item__stars-and-reviews--item">
              <?for($i=1;$i<=5;$i++):?>
              <?if($i<=$arResult['stars']['rate']):?><img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><?else:?>
              <img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star_empty.png" alt="star">
              <?endif;?>
              <?endfor;?>
              <a class="item__stars-and-reviews-link" href="javascript: void(0);">Отзывы (<?if(isset($arResult['comment'])):?><?=$arResult['stars']['count']?><?else:?>0<?endif;?>)</a></div>
            <div class="item--present--block"><img src="<?=SITE_TEMPLATE_PATH?>/images/img/gift.png" alt="">Фонарь в подарок</div>
            <div class="item--sales-label">Лидер продаж</div>
          </div>
          <div class="item--article">Артикул: <?=$arResult['PROPERTIES']['VENDOR_CODE']['VALUE']?></div>
        </div>
        <div class="item__content">
          <div class="prodyct">
          <div class="gallery__prodyct--img-01">
            <div class="prodyct__info--01">
              <div class="small-img">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/arrow.png" class="icon-left icon-left-btn" alt="" id="prev-img">
                <div class="small-container">
                  <div id="small-img-roll">
                    <?foreach($arResult['PROPERTIES']['pictures']['VALUE'] as $key => $item):?>
                    <img style="margin: 0;" src="<?=CFile::GetPath($item)?>" class="show-small-img" alt="">
                    <?endforeach?>
                  </div>
                </div>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/arrow.png" class="icon-right icon-right-btn" alt="" id="next-img">
                <button class="btn__show--video"><svg version="1.1" class="btn__show--video--svg" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 420 420" xml:space="preserve">
                    <path d="M210,21c104.216,0,189,84.784,189,189s-84.784,189-189,189S21,314.216,21,210S105.784,21,210,21 M210,0
                     C94.031,0,0,94.024,0,210s94.031,210,210,210s210-94.024,210-210S325.969,0,210,0L210,0z" />
                    <path d="M293.909,187.215l-111.818-73.591C162.792,100.926,147,109.445,147,132.545V287.42c0,23.1,15.813,31.647,35.147,18.998
                     L293.86,233.31C313.187,220.647,313.208,199.913,293.909,187.215z M279.006,217.868l-99.295,64.981
                     c-6.44,4.221-11.711,1.372-11.711-6.328V143.437c0-7.7,5.264-10.535,11.697-6.3l99.33,65.366
                     C285.46,206.731,285.453,213.647,279.006,217.868z" />
                  </svg></button>
                <button class="btn__show--3d"><svg version="1.1" class="btn__show--3d--svg" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve">
                    <path d="M199.467,312.427c-4.8,4.48-11.52,6.72-20.16,6.72h0c-4.053,0-7.787-0.533-11.2-1.813
                         c-3.307-1.173-6.187-2.88-8.533-5.013c-2.347-2.133-4.16-4.8-5.44-7.787c-1.28-3.093-1.92-6.4-1.92-10.027H124.48
                         c0,7.68,1.493,14.4,4.48,20.267c2.987,5.867,6.933,10.773,11.947,14.613c5.013,3.947,10.773,6.827,17.387,8.853
                         c6.613,2.133,13.44,3.093,20.693,3.093c7.893,0,15.253-1.067,22.08-3.2c6.827-2.133,12.693-5.333,17.707-9.493
                         c5.013-4.16,8.853-9.28,11.733-15.36c2.773-6.08,4.267-13.013,4.267-20.8c0-4.16-0.533-8.107-1.493-11.947
                         c-1.067-3.84-2.667-7.467-4.8-10.88c-2.24-3.413-5.12-6.4-8.64-9.173c-3.52-2.667-7.893-4.907-12.907-6.72
                         c4.267-1.92,8-4.267,11.2-7.04s5.867-5.76,8-8.853c2.133-3.2,3.733-6.4,4.8-9.813c1.067-3.413,1.6-6.827,1.6-10.133
                         c0-7.787-1.28-14.613-3.84-20.48c-2.56-5.867-6.187-10.773-10.88-14.72s-10.24-6.933-16.853-8.96
                         c-6.827-2.133-14.08-3.093-21.973-3.093c-7.68,0-14.827,1.173-21.333,3.413s-12.053,5.44-16.747,9.493
                         c-4.693,4.053-8.32,8.853-10.987,14.4c-2.667,5.547-3.947,11.627-3.947,18.133h27.733c0-3.627,0.64-6.827,1.92-9.6
                         c1.28-2.88,3.093-5.333,5.333-7.253c2.24-2.027,5.013-3.627,8.107-4.693c3.093-1.067,6.507-1.707,10.133-1.707
                         c8.533,0,14.827,2.24,18.987,6.613c4.16,4.373,6.187,10.56,6.187,18.453c0,3.84-0.533,7.253-1.707,10.347
                         c-1.173,3.093-2.88,5.76-5.227,8s-5.333,3.947-8.747,5.227c-3.52,1.28-7.68,1.92-12.373,1.92h-16.427v21.867h16.427
                         c4.693,0,8.96,0.533,12.693,1.6s6.933,2.667,9.6,5.013c2.667,2.24,4.693,5.12,6.187,8.533c1.387,3.413,2.133,7.467,2.133,12.16
                         C206.933,301.227,204.48,307.84,199.467,312.427z" />
                    <path d="M160.32,458.347C90.667,425.28,40.64,357.547,33.067,277.333h-32C11.947,408.747,121.813,512,256,512
                         c4.8,0,9.387-0.427,14.08-0.747l-81.28-81.387L160.32,458.347z" />
                    <path d="M256,0c-4.8,0-9.387,0.427-14.08,0.747l81.28,81.387l28.373-28.373c69.76,32.96,119.787,100.693,127.36,180.907h32
                         C500.053,103.253,390.187,0,256,0z" />
                    <path d="M361.92,192.747L361.92,192.747c-6.72-7.04-14.827-12.48-24.213-16.32c-9.493-3.84-19.84-5.76-31.253-5.76H256v170.667
                         h48.96c11.84,0,22.507-1.92,32.213-5.76c9.707-3.84,17.92-9.28,24.747-16.32c6.827-7.04,12.16-15.573,15.787-25.493
                         c3.733-9.92,5.547-21.12,5.547-33.493v-8.427c0-12.373-1.92-23.467-5.653-33.493C373.867,208.32,368.64,199.787,361.92,192.747z
                          M353.6,260.373h-0.107c0,8.853-0.96,16.96-3.093,24c-2.027,7.147-5.013,13.12-9.067,18.027s-9.067,8.64-15.147,11.307
                         c-6.08,2.56-13.12,3.947-21.227,3.947h-19.307V194.56h20.8c15.36,0,26.987,4.907,35.093,14.613
                         c8,9.813,12.053,23.893,12.053,42.453V260.373z" />
                  </svg>
                </button>
              </div>
              <div class="show show-img--01" href="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
                <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" id="show-img">
              </div>
            </div>
            <div class="prodyct__info--02">
              <div class="prodyct__info">
                <div class="tools__info"><img src="<?=SITE_TEMPLATE_PATH?>/images/img/tools.png" alt=""><?=$arResult['PROPERTIES']['FUNCTIONS']['VALUE']?></div>
                <div class="case__info"><img src="<?=SITE_TEMPLATE_PATH?>/images/img/case.png" alt="">Кожанный чехол</div>
                <?if($arResult['PRICES']['BASE']["DISCOUNT_DIFF"]):?><div class="sale-percent__info">-<?=$arResult['PRICES']['BASE']["DISCOUNT_DIFF_PERCENT"]?> %</div><?endif;?>
              </div>
              <div class="button__prodyct--like">
                <button class="btn__like"><svg version="1.1" class="btn__like--svg" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 485 485" xml:space="preserve">
                    <path d="M343.611,22.543c-22.613,0-44.227,5.184-64.238,15.409c-13.622,6.959-26.136,16.205-36.873,27.175
                     c-10.738-10.97-23.251-20.216-36.873-27.175c-20.012-10.225-41.625-15.409-64.239-15.409C63.427,22.543,0,85.97,0,163.932
                     c0,55.219,29.163,113.866,86.678,174.314c48.022,50.471,106.816,92.543,147.681,118.95l8.141,5.261l8.141-5.261
                     c40.865-26.406,99.659-68.479,147.682-118.95C455.838,277.798,485,219.151,485,163.932C485,85.97,421.573,22.543,343.611,22.543z
                      M376.589,317.566c-42.918,45.106-95.196,83.452-134.089,109.116c-38.893-25.665-91.171-64.01-134.088-109.116
                     C56.381,262.884,30,211.194,30,163.932c0-61.42,49.969-111.389,111.389-111.389c35.361,0,67.844,16.243,89.118,44.563
                     l11.993,15.965l11.993-15.965c21.274-28.32,53.757-44.563,89.118-44.563c61.42,0,111.389,49.969,111.389,111.389
                     C455,211.194,428.618,262.884,376.589,317.566z" />
                  </svg></button>
                <button class="btn__comparison">
                  <svg version="1.1" class="btn__comparison--svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                    x="0px" y="0px" viewBox="0 0 490 490" style="enable-background:new 0 0 490 490;" xml:space="preserve">
                    <path d="M117,220H9.9c-5.5,0-9.9,4.4-9.9,9.9v248c0,5.5,4.4,9.9,9.9,9.9H117c5.5,0,9.9-4.4,9.9-9.9v-248
                         C126.9,224.4,122.5,220,117,220z M107.1,468H19.8V239.8h87.4V468H107.1z" />
                    <path d="M298.6,2.2H191.4c-5.5,0-9.9,4.4-9.9,9.9v465.7c0,5.5,4.4,9.9,9.9,9.9h107.2c5.5,0,9.9-4.4,9.9-9.9
                         V12.1C308.5,6.7,304.1,2.2,298.6,2.2z M288.7,468h-87.4V22h87.4V468z" />
                    <path d="M480.1,487.8c5.5,0,9.9-4.4,9.9-9.9V162.6c0-5.5-4.4-9.9-9.9-9.9H373c-5.5,0-9.9,4.4-9.9,9.9v315.2
                         c0,5.5,4.4,9.9,9.9,9.9h107.1V487.8z M382.9,172.5h87.4V468h-87.4V172.5z" />
                  </svg>
                </button>
              </div>
              <div class="buy__item--info-01">
                <div class="buy-card--info-01">
                  <div class="buy-card--header-01">
                    <div class="stock__info-01">
                      <p class="new-price--info-01"><?=$arResult['PRICES']['BASE']['PRINT_DISCOUNT_VALUE_NOVAT']?> ₽</p>
                      <?if($arResult['PRICES']['BASE']["DISCOUNT_DIFF"]):?> <p class="old-price--info-01"><?=$arResult['PRICES']['BASE']['VALUE_VAT']?> ₽</p><?endif;?>
                      
                    </div>
                    <div class="stock__info-02">
                      <div class="in-stock--info-01">
                        <svg xmlns="http://www.w3.org/2000/svg" class="in-stock--infoimg-01" width="15" height="15"
                          viewBox="0 0 48 48">
                          <path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z" /></svg>
                        <p class="in-stock--info-02">В
                          наличии</p>
                      </div>
                      <p class="bonuses__info-01">+500 бонусов <svg class="bonuses__info-01--svg" version="1.1" id="Layer_1"
                          xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                          viewBox="0 0 426.667 426.667" xml:space="preserve">
                          <rect x="192" y="298.667" width="42.667" height="42.667" />
                          <path d="M213.333,0C95.513,0,0,95.513,0,213.333s95.513,213.333,213.333,213.333s213.333-95.513,213.333-213.333
                           S331.154,0,213.333,0z M213.333,388.053c-96.495,0-174.72-78.225-174.72-174.72s78.225-174.72,174.72-174.72
                           c96.446,0.117,174.602,78.273,174.72,174.72C388.053,309.829,309.829,388.053,213.333,388.053z" />
                          <path d="M296.32,150.4c-10.974-45.833-57.025-74.091-102.858-63.117c-38.533,9.226-65.646,43.762-65.462,83.384h42.667
                           c2.003-23.564,22.729-41.043,46.293-39.04s41.043,22.729,39.04,46.293c-4.358,21.204-23.38,36.169-45.013,35.413
                           c-10.486,0-18.987,8.501-18.987,18.987v0v45.013h42.667v-24.32C279.787,241.378,307.232,195.701,296.32,150.4z" />
                        </svg>
                      </p>
                    </div>
                  </div>
                  <div class="buy-card--buttons--info-01">
                    <button class="btn-yellow--info-01">Добавить в корзину</button>
                    <button class="btn-clear--info-01">Купить в 1 клик</button>
                  </div>
                  <a class="best-price--info-01" href="#">Нашли дешевле? Снизим цену!</a>
                </div>
                <div class="shipping__info-01">
                  <div class="shipping__info--title-02">
                    <p class="shipping__info--title-01"> <svg version="1.1" class="shipping__info--title--svg" xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve">
                        <path d="M491.729,112.971L259.261,0.745c-2.061-0.994-4.461-0.994-6.521,0L20.271,112.971c-2.592,1.251-4.239,3.876-4.239,6.754
                     v272.549c0,2.878,1.647,5.503,4.239,6.754l232.468,112.226c1.03,0.497,2.146,0.746,3.261,0.746s2.23-0.249,3.261-0.746
                     l232.468-112.226c2.592-1.251,4.239-3.876,4.239-6.754V119.726C495.968,116.846,494.32,114.223,491.729,112.971z M256,15.828
                     l215.217,103.897l-62.387,30.118c-0.395-0.301-0.812-0.579-1.27-0.8L193.805,45.853L256,15.828z M176.867,54.333l214.904,103.746
                     l-44.015,21.249L132.941,75.624L176.867,54.333z M396.799,172.307v78.546l-41.113,19.848v-78.546L396.799,172.307z
                      M480.968,387.568L263.5,492.55V236.658l51.873-25.042c3.73-1.801,5.294-6.284,3.493-10.015
                     c-1.801-3.729-6.284-5.295-10.015-3.493L256,223.623l-20.796-10.04c-3.731-1.803-8.214-0.237-10.015,3.493
                     c-1.801,3.73-0.237,8.214,3.493,10.015l19.818,9.567V492.55L31.032,387.566V131.674l165.6,79.945
                     c1.051,0.508,2.162,0.748,3.255,0.748c2.788,0,5.466-1.562,6.759-4.241c1.801-3.73,0.237-8.214-3.493-10.015l-162.37-78.386
                     l74.505-35.968L340.582,192.52c0.033,0.046,0.07,0.087,0.104,0.132v89.999c0,2.581,1.327,4.98,3.513,6.353
                     c1.214,0.762,2.599,1.147,3.988,1.147c1.112,0,2.227-0.247,3.26-0.746l56.113-27.089c2.592-1.251,4.239-3.875,4.239-6.754v-90.495
                     l69.169-33.392V387.568z" />
                        <path d="M92.926,358.479L58.811,342.01c-3.732-1.803-8.214-0.237-10.015,3.493c-1.801,3.73-0.237,8.214,3.493,10.015
                     l34.115,16.469c1.051,0.508,2.162,0.748,3.255,0.748c2.788,0,5.466-1.562,6.759-4.241
                     C98.22,364.763,96.656,360.281,92.926,358.479z" />
                        <path d="M124.323,338.042l-65.465-31.604c-3.731-1.801-8.214-0.237-10.015,3.494c-1.8,3.73-0.236,8.214,3.494,10.015
                     l65.465,31.604c1.051,0.507,2.162,0.748,3.255,0.748c2.788,0,5.466-1.562,6.759-4.241
                     C129.617,344.326,128.053,339.842,124.323,338.042z" />
                      </svg>Доставкав г.Москва</p>
                  </div>
                  <p class="shipping-desc--info-01">
                    <svg xmlns="http://www.w3.org/2000/svg" class="shipping-desc--info-svg" width="20" height="15"
                      viewBox="0 0 48 48">
                      <path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z" /></svg>
                    Самовывоз сегодня, <span>бесплатно </span></p>
                  <p class="shipping-desc--info-01">
                    <svg xmlns="http://www.w3.org/2000/svg" class="shipping-desc--info-svg" width="20" height="15"
                      viewBox="0 0 48 48">
                      <path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z" /></svg>
                    Пункт выдачи: <span>завтра, бесплатно</span></p>
                  <p class="shipping-desc--info-01">
                    <svg xmlns="http://www.w3.org/2000/svg" class="shipping-desc--info-svg" width="20" height="15"
                      viewBox="0 0 48 48">
                      <path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z" /></svg>
                    Курьером: <span>1-2 дня, 300 ₽</span></p>
                  <p class="shipping-desc--info-01">
                    <svg xmlns="http://www.w3.org/2000/svg" class="shipping-desc--info-svg" width="20" height="15"
                      viewBox="0 0 48 48">
                      <path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z" /></svg>
                    Почтой России: <span>5-10 дней, 200 ₽</span></p>
                </div>
                <div class="shipping__advantages--img-01">
                  <?foreach($arResult['accomplishments'] as $key => $item):?>
                <div class="shipping-advantages--desc-0<?=$key+1?>">
                <img class="shipping__advantages--desc-img-01" src="<?=$item['SRC']?>" alt="">
                    <p class="shipping__advantages--desc-text-01"><?=$item['UF_NAME']?><p>
              </div>
                <?endforeach;?>
                </div>
                <div class="shipping__info-popup">
                  <svg xmlns="http://www.w3.org/2000/svg" class="shipping__info-popup--svg" width="14" height="14"
                    viewBox="0 0 48 48">
                    <path d="M38 12.83L35.17 10 24 21.17 12.83 10 10 12.83 21.17 24 10 35.17 12.83 38 24 26.83 35.17 38 38 35.17 26.83 24z" />
                  </svg>
                  <p class="shipping__info-popup-text">Начисление Бонусных рублей произойдёт после доставки заказа.</p>
                  <br>
                  <p class="shipping__info-popup-text">Сумма Бонусных
                    рублей определяется в соответствии с нашими <a class="" href="">правилами.</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
          
        </div>
      </div>
    </div>
    <div class="item-tabs">
      <div class="container">
        <div class="tabs">
          <div class="tabNav"><a class="tabLink tabActive" id="defaultOpenTab" href="#" onclick="openTab(event, 'desc')">Описание</a><a class="tabLink" href="#" onclick="openTab(event, 'charact')">Характеристики</a><a class="tabLink" href="#" onclick="openTab(event, 'overview')">Обзор</a><a class="tabLink" href="#" onclick="openTab(event, 'reviews')">Отзывы</a><a class="tabLink" href="#" onclick="openTab(event, 'accessories')">Аксессуары</a></div>
          <div class="tabContent" id="desc">
            <p>Мультитул Leatherman Rebar 831560 – более компактный вариант модели Super Tool 300. Пользователи могут увидеть ряд инструментов, заимствованных также и с Micra.<br><br>Мультитул получил усиленные кусачки благодаря марке стали 154СМ, универсальные пассатижи с тонкими острогубцами, длинную пилу, комплект отверток на все случаи жизни.<br><br>Всего в наборе 17 инструментов, 2 из которых острые ножи. Один обычный, а второй – с серрейторной заточкой кромки клинка. Оба отлиты из нержавеющей стали 420HC. Присутствует напильник по дереву и пила которая легко разделает надвое ветку диаметром 5 см. В наборе предусмотрено крепежное кольцо.<br><br>Помимо всего Лезерман Ребар с кожаным чехлом производители добавили сменные кусачки. Все опции на рукоятках фиксируются предохранителями</p><a class="openTabsDesc">Перейти к подробному обзору<img src="/images/img/arrow-down.png" alt=""></a>
            <div class="hiddenTabsDesc">
              <p class="htd__title">Основные характеристики</p>
              <div class="htd__desc">
                <div class="htd__tables">
                  <table class="charTable">
                    <tr>
                      <td>Материал корпуса:</td>
                      <td><?=$arResult['PROPERTIES']['material_body']['VALUE']?></td>
                    </tr>
                    <tr>
                      <td>Материал клинка:</td>
                      <td><?=$arResult['PROPERTIES']['blade_stuff']['VALUE']?></td>
                    </tr>
                    <tr>
                      <td>Материал рукояти:</td>
                      <td><?=$arResult['PROPERTIES']['blade_hand']['VALUE']?></td>
                    </tr>
                    <tr>
                      <td>Тип ножевого замка:</td>
                      <td><?=$arResult['PROPERTIES']['lock_type']['VALUE']?></td>
                    </tr>
                    <tr>
                      <td>Приспособление для открытия клинка:</td>
                      <td><?=$arResult['PROPERTIES']['blade_unlock']['VALUE']?></td>
                    </tr>
                    <tr>
                      <td>Длина клинка(см.):</td>
                      <td>8.2 см</td>
                    </tr>
                    <tr>
                      <td>Толщина обуха(мм.):</td>
                      <td><?=$arResult['PROPERTIES']['thick']['VALUE']?></td>
                    </tr>
                  </table>
                  <div class="htd__func">
                    <p class="table--title">Функции:</p>
                    <table>
                      <tr>
                        <td>Количество функций:</td>
                        <td><?=$arResult['PROPERTIES']['FUNCTIONS']['VALUE']?></td>
                      </tr>
                      <tr>
                        <td class="htd__func--lt">Функции</td>
                        <td>
                          <ul>
                            <?foreach($arResult['PROPERTIES']['list_of_func']['VALUE'] as $item):?>
                            <li><?=$item?><img src="<?=SITE_TEMPLATE_PATH?>/images/img/question.png" alt=""></li>
                            <?endforeach;?>
                          </ul>
                        </td>
                      </tr>
                    </table>
                  </div>
                  <div class="htd__set">
                    <p class="table--title">Комплектация</p>
                    <table>
                      <?foreach($arResult['PROPERTIES']['complects']['VALUE'] as $item):?>
                            <tr>
                          <td><img src="<?=SITE_TEMPLATE_PATH?>/images/img/done-grey.png" alt=""><?=$item?></td>
                            </tr>
                            <?endforeach;?>
                    </table>
                  </div>
                  <div class="htd__set">
                    <p class="table--title">Инструменты</p>
                    <table>
                      <?foreach($arResult['PROPERTIES']['tools']['VALUE'] as $key => $item):?>
                            <tr>
                          <td><?=$key+1?> <?=$item?></td>
                            </tr>
                            <?endforeach;?>
                      
                      <tr>
                        <td class="table--picture"><img src="<?=CFile::GetPath($arResult['PROPERTIES']['image_tools']['VALUE'])?>" alt=""></td>
                      </tr>
                    </table>
                  </div>
                  <div class="htd__set">
                    <p class="table--title">Инструменты</p>
                    <table>
                      <tr>
                        <td>A. Опции жесткой фиксации</td>
                      </tr>
                      <tr>
                        <td>B. Кольцо для крепления</td>
                      </tr>
                      <tr>
                        <td>C. Съемная клипса для крепления</td>
                      </tr>
                      <tr>
                        <td>D. Доступ к функции без раскрытия</td>
                      </tr>
                      <tr>
                        <td class="table--picture"><img src="<?=CFile::GetPath($arResult['PROPERTIES']['image_instr']['VALUE'])?>" alt=""></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="instructions">
                  <p class="instructions__title">Инструкции и сертификаты</p>
                  <?foreach($arResult['PROPERTIES']['instruction']['VALUE'] as $item):?>
                            <a href="<?=CFile::GetPath($item)?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/img/pdf.png" alt="">
                    <div class="instructions__desc">Инструкция для мультитула<br><?=$arResult['NAME']?>
                      <p>(2.4 mb)</p>
                    </div></a>
                            <?endforeach;?>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="tabContent" id="charact">
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Obcaecati, architecto praesentium. Voluptates totam amet ea, molestias voluptatem et facere quidem eius odit fugiat eveniet sint ipsam. Fuga velit sunt a!<br><br> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quaerat animi officia laboriosam ab, doloremque nostrum et consequuntur quae nam perferendis autem maiores? Aliquam, ab nesciunt atque nam iure voluptate ea.<br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit necessitatibus vero quos laboriosam a harum aliquid excepturi totam! Natus cum sequi esse iste ipsum adipisci minima neque iusto molestias quas. <br><br> Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perspiciatis accusamus iure natus. Sapiente illo tenetur rerum quas culpa ab praesentium modi itaque porro. Obcaecati dolores distinctio tempore aliquam provident voluptatem!</p>
          </div>
          <div class="tabContent" id="overview">
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium inventore reprehenderit voluptates totam unde ab fugit facere magni consectetur hic magnam, suscipit quia? Dignissimos eaque tempora ea mollitia! Accusantium, sit. <br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis cumque nihil quae perspiciatis neque fuga sunt dolorum assumenda suscipit, fugit cum commodi nam atque fugiat impedit delectus doloremque itaque vitae. <br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto omnis, iusto, facere necessitatibus sint maiores harum et nobis, assumenda nulla officia in blanditiis quisquam veritatis a molestias nisi quibusdam debitis?</p><a class="openTabsDesc">Перейти к подробному обзору<img src="/images/img/arrow-down.png" alt=""></a>
            <div class="hiddenTabsDesc">
              <p class="htd__title">Обзор</p>
              <div class="htd__overv"><img src="/images/img/thing-3.png" alt="">
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deleniti, accusantium culpa laboriosam quaerat sapiente dolor nihil numquam cum illo autem velit alias voluptates libero qui nulla aliquam repellendus error omnis.<br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla, omnis! Voluptate dolorum voluptatum vitae iure corrupti quidem perspiciatis a, ullam, dicta reiciendis explicabo non harum aperiam quos veritatis, quibusdam placeat!<br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic sed, quibusdam eveniet laudantium dignissimos dicta praesentium nulla reprehenderit vero expedita doloremque fugiat, aliquam incidunt dolores. Ea sequi explicabo vel molestias.<br><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste nesciunt ipsum mollitia facere illo corporis commodi, aperiam ex beatae expedita cumque, possimus sed tempora rem dolorum quia? Deserunt, quis fugiat?<br><br>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia numquam illum iure fuga nihil? Fuga, maiores vero natus et harum cum quam reiciendis quasi cumque ullam laborum doloribus quod sint?<br><br>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Est nostrum asperiores iusto rem nihil, magni minus fugiat corporis nisi iure impedit? Ut, cum assumenda quisquam aliquid consectetur possimus dicta consequuntur.<br><br>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Non vel itaque aliquam impedit dolorem nesciunt eos autem fugiat tenetur, quae accusantium necessitatibus expedita. Provident rem cum cupiditate dolores ullam minus.<br><br>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis, atque quo at laborum aliquam tenetur soluta sit laboriosam eum doloribus suscipit necessitatibus ea asperiores similique iure. Enim soluta corrupti dicta?</p><iframe width="100%" height="500px" src="https://www.youtube.com/embed/BNzc6hG3yN4?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
            </div>
          </div>
          <div class="tabContent" id="reviews">
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem dolores harum possimus, sequi explicabo omnis delectus ratione iusto laboriosam ad laborum quam accusamus labore culpa ipsum quia itaque odio nemo! <br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. At neque nihil, unde quos sit tempore sunt quod autem aliquid deleniti numquam ratione rem necessitatibus earum itaque animi eligendi quibusdam corporis?<br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum aliquam, mollitia deleniti minus unde eum dolores sequi, laborum nulla molestiae eligendi sit possimus quibusdam expedita voluptate voluptatum. Laudantium, qui rerum. <br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus eum eos culpa, similique fugiat vel nihil aspernatur ducimus, eligendi aut sed dolorum odio architecto sint libero minima in id doloremque.</p><a class="openTabsDesc">Развернуть<img src="/images/img/arrow-down.png" alt=""></a>
            <div class="hiddenTabsDesc" id="comments">
              <p class="htd__title">Отзывы (<?=$arResult['stars']['count']?>)</p>
              <div class="htd__reviews">
                <div class="reviews__head">
                  <div class="reviews__head--left">
                    <div class="rating"><span><img src="/images/img/star-fill.png" alt=""><img src="/images/img/star-fill.png" alt=""><img src="/images/img/star-fill.png" alt=""><img src="/images/img/star-clear.png" alt=""><img src="/images/img/star-clear.png" alt=""></span>
                      <p><?=$arResult['stars']['rate']?> из 5</p>
                    </div>
                    <div class="rating--line"><span>5<img src="/images/img/star-fill.png" alt=""></span>
                      <div class="line">
                        <div style="width:<?=$arResult['stars_percent'][5]?>%;"></div>
                      </div>
                      <p><?=$arResult['stars'][5]?> <?=NameCounter::rewriter($arResult['stars'][5],'отзыв','','а','ов',0)?></p>
                    </div>
                    <div class="rating--line"><span>4<img src="/images/img/star-fill.png" alt=""></span>
                      <div class="line">
                        <div  style="width:<?=$arResult['stars_percent'][4]?>%;"></div>
                      </div>
                      <p><?=$arResult['stars'][4]?> <?=NameCounter::rewriter($arResult['stars'][4],'отзыв','','а','ов',0)?></p>
                    </div>
                    <div class="rating--line"><span>3<img src="/images/img/star-fill.png" alt=""></span>
                      <div class="line">
                        <div style="width:<?=$arResult['stars_percent'][3]?>%;"></div>
                      </div>
                      <p><?=$arResult['stars'][3]?> <?=NameCounter::rewriter($arResult['stars'][3],'отзыв','','а','ов',0)?></p>
                    </div>
                    <div class="rating--line"><span>2<img src="/images/img/star-fill.png" alt=""></span>
                      <div class="line">
                        <div  style="width:<?=$arResult['stars_percent'][2]?>%;"></div>
                      </div>
                      <p><?=$arResult['stars'][2]?> <?=NameCounter::rewriter($arResult['stars'][2],'отзыв','','а','ов',0)?></p>
                    </div>
                    <div class="rating--line"><span>1<img src="/images/img/star-fill.png" alt=""></span>
                      <div class="line">
                        <div  style="width:<?=$arResult['stars_percent'][1]?>%;"></div>
                      </div>
                      <p><?=$arResult['stars'][1]?> <?=NameCounter::rewriter($arResult['stars'][1],'отзыв','','а','ов',0)?></p>
                    </div>
                  </div>
                  <div class="reviews__head--right">
                    <p><strong>Уже купили этот товар? Напишите о нем отзыв!</strong></p>
                    <p>Ваш отзыв будет полезен и может помочь с выбором</p>
                    <div class="">
                      <fieldset class="rating">
                        <input type="radio" id="star5" name="rating" value="5" /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                        <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half"
                          title="Pretty good - 4.5 stars"></label>
                        <input type="radio" id="star4" name="rating" value="4" /><label class="full" for="star4" title="Pretty good - 4 stars"></label>
                        <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half"
                          title="Meh - 3.5 stars"></label>
                        <input type="radio" id="star3" name="rating" value="3" /><label class="full" for="star3" title="Meh - 3 stars"></label>
                        <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half"
                          title="Kinda bad - 2.5 stars"></label>
                        <input type="radio" id="star2" name="rating" value="2" /><label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                        <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half"
                          title="Meh - 1.5 stars"></label>
                        <input type="radio" id="star1" name="rating" value="1" /><label class="full" for="star1" title="Sucks big time - 1 star"></label>
                        <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf"
                          title="Sucks big time - 0.5 stars"></label>
                      </fieldset>
                      <form class="reviews-head--form" method="POST">
                        <input <?if($USER->IsAuthorized()):?> readonly value="<?=$USER->GetFirstName()?>"<?endif;?>name="name" type="name" placeholder="Ваше имя">
                        <input <?if($USER->IsAuthorized()):?> readonly value="<?=$USER->GetEmail()?>"<?endif;?> name="email" type="mail" placeholder="Ваша почта">
                        <p class="reviews-head--title">Оставить отзыв<br>
                          <textarea name="comment" cols="40" rows="3"></textarea></p>
                        <button name="ADD_COMMENT" value="true" type="submit">Написать отзыв</button>
                      </form>
                    </div>
                  </div>
                </div>
                <div class="reviews__control">
                  <p>Сортировка:</p><a class="control-link--active" href="">По дате<img src="/images/img/down-arrow.svg" alt=""></a><a href="">Оценке</a><a href="">Полезности</a>
                </div>
                <?foreach($arResult['comment'] as $key => $item):?>
                <div class="reviews__comment">
                  <div class="comment--avatar"><span><img src="<?=SITE_TEMPLATE_PATH?>/images/img/avatar.png" alt=""></span>
                    <div>
                      <?for($i=1;$i<=5;$i++):?>
                      <?if($i<=$item['UF_STARS']):?>
                      <img src="<?=SITE_TEMPLATE_PATH?>/images/img/star-fill.png" alt="">
                      <?else:?>
                      <img src="<?=SITE_TEMPLATE_PATH?>/images/img/star-clear.png" alt="">
                      <?endif;?>
                      <?endfor;?>
                    </div>

                  </div>
                  <div class="comment--text">
                    <p class="commentator"><?=$item['UF_AUTHOR']?></p>
                    <p><?=$item['UF_DATE']->toString()?></p>
                    <p><?=$item['UF_COMMENT']?></p><a href=""><img src="<?=SITE_TEMPLATE_PATH?>/images/img/like.png" alt=""></a><a href=""><img src="<?=SITE_TEMPLATE_PATH?>/images/img/dislike.png" alt=""></a>
                  </div>
                </div>
                <?endforeach;?>
               <? $APPLICATION->IncludeComponent("bitrix:main.pagenavigation", "comments", Array(
                      "NAV_OBJECT" => $arResult['comment-nav'] ,
                      "SEF_MODE" => "N"
                                    ),
                                $component
                  );?>
              </div>
            </div>
          </div>
          <div class="tabContent" id="accessories">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea, explicabo neque sint, nihil eius iusto mollitia repellendus molestias odit fugiat vitae dolores, consectetur tempore reprehenderit eos cupiditate maxime. Provident, ipsum. <br><br> Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas maxime atque temporibus, magni quae consequatur dicta perferendis. Ullam minus molestias consequuntur praesentium perferendis nemo quod ea, labore eum, pariatur vitae! <br><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui nisi iure alias aspernatur provident velit nemo laudantium sit ea voluptatum magni, commodi, culpa a officia vitae dolorem voluptates doloremque. Ipsa?</p>
          </div>
        </div>
      </div>
    </div>

    
   <?endif;?>