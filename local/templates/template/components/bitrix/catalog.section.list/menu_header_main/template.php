<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

 // echo "<pre>"; print_r($arResult); echo "</pre>";
$prevdepth=1;
?>
<div class="menu" id="menu">
<div class="menu-wrapper-grid">
<div class="menu__empty-grey1"></div>
          <div class="menu__empty-grey2"></div>
          <div class="menu__empty-grey3"></div>
          <div class="menu__empty-grey4"></div>
          <div class="menu__empty-grey5"></div>
          <div class="menu__empty-grey6"></div>
<?foreach($arResult['SECTIONS'] as $key => $arItem):?>
<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strItemEdit);
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strItemDelete, $arItemDeleteParams);?>
<?if($arItem['DEPTH_LEVEL']==1 && $prevdepth==2):?>
</div>
<?$prevdepth=1?>
<?endif;?>
<?if($arItem['DEPTH_LEVEL']==1 && $prevdepth==1):?>
<div class="menu__wrapper-<?=$arItem['UF_CLASS']?>-title menu__wrapper-title" ><?=$arItem['NAME']?></div>
<div class="menu__wrapper-<?=$arItem['UF_CLASS']?>-image" id="<?=$this->GetEditAreaId($arItem['ID'])?>"><img class="menu__image" src="<?=$arItem['PICTURE']['SRC']?>" alt="<?=$arItem['PICTURE']['ALT']?>" title="<?=$arItem['PICTURE']['TITLE']?>"></div>
<div class="menu__wrapper-<?=$arItem['UF_CLASS']?>-links">
<a  class="menu__link" href="<?=$arItem['SECTION_PAGE_URL']?>"><?="Все "?>
<?if(preg_match('/\s/', $arItem['NAME'])){echo explode(" ",strtolower($arItem['NAME']))[1]; }
else{echo strtolower($arItem['NAME']);
}
?></a>
<?$prevdepth=1;?>

<?endif;?>
<!-- <?if($arItem['DEPTH_LEVEL']==2 && $prevdepth==1):?>
<div class="menu__wrapper-<?=$arItem['UF_CLASS']?>-links">
<?endif;?> -->
<?if($arItem['DEPTH_LEVEL']==2):?>
<a id="<?=$this->GetEditAreaId($arItem['ID'])?>" class="menu__link" href="<?=$arItem['SECTION_PAGE_URL']?>"><?=$arItem['NAME']?></a>
<?$prevdepth=2;?>
<?endif;?>
<?endforeach;?>
</div>

  </div>
  
<div class="menu__close">
          <buttom class="menu__close-button" id="hmc">Закрыть меню</buttom>
        </div>
       
      </div> 