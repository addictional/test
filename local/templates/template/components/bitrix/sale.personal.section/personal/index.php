<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;


if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

$theme = Bitrix\Main\Config\Option::get("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);

$availablePages = array();

if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ORDERS'],
		"name" => Loc::getMessage("SPS_ORDER_PAGE_NAME"),
		"icon" => '<i class="fa fa-calculator"></i>'
	);
}

if ($arParams['SHOW_ACCOUNT_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ACCOUNT'],
		"name" => Loc::getMessage("SPS_ACCOUNT_PAGE_NAME"),
		"icon" => '<i class="fa fa-credit-card"></i>'
	);
}

if ($arParams['SHOW_PRIVATE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_PRIVATE'],
		"name" => Loc::getMessage("SPS_PERSONAL_PAGE_NAME"),
		"icon" => '<i class="fa fa-user-secret"></i>'
	);
}

if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
{

	$delimeter = ($arParams['SEF_MODE'] === 'Y') ? "?" : "&";
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ORDERS'].$delimeter."filter_history=Y",
		"name" => Loc::getMessage("SPS_ORDER_PAGE_HISTORY"),
		"icon" => '<i class="fa fa-list-alt"></i>'
	);
}

if ($arParams['SHOW_PROFILE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_PROFILE'],
		"name" => Loc::getMessage("SPS_PROFILE_PAGE_NAME"),
		"icon" => '<i class="fa fa-list-ol"></i>'
	);
}

if ($arParams['SHOW_BASKET_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arParams['PATH_TO_BASKET'],
		"name" => Loc::getMessage("SPS_BASKET_PAGE_NAME"),
		"icon" => '<i class="fa fa-shopping-cart"></i>'
	);
}

if ($arParams['SHOW_SUBSCRIBE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_SUBSCRIBE'],
		"name" => Loc::getMessage("SPS_SUBSCRIBE_PAGE_NAME"),
		"icon" => '<i class="fa fa-envelope"></i>'
	);
}

if ($arParams['SHOW_CONTACT_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arParams['PATH_TO_CONTACT'],
		"name" => Loc::getMessage("SPS_CONTACT_PAGE_NAME"),
		"icon" => '<i class="fa fa-info-circle"></i>'
	);
}

$customPagesList = CUtil::JsObjectToPhp($arParams['~CUSTOM_PAGES']);
if ($customPagesList)
{
	foreach ($customPagesList as $page)
	{
		$availablePages[] = array(
			"path" => $page[0],
			"name" => $page[1],
			"icon" => (strlen($page[2])) ? '<i class="fa '.htmlspecialcharsbx($page[2]).'"></i>' : ""
		);
	}
}

if (empty($availablePages))
{
	ShowError(Loc::getMessage("SPS_ERROR_NOT_CHOSEN_ELEMENT"));
}
else
{
	?>
	<div class="personalarea__bigtitle-wrapper">
      <div class="container">
        <div class="personalarea__bigtitle-text">	Личный кабинет</div>
      </div>
    </div>
    <div class="personalarea__big-wrapper">
      <div class="container">
        <div class="personalarea__wrapper-flex">
          <aside class="aside__menu-wrapper">
            <div class="aside__menu-container-type">
              <p class="aside__menu-link-opened">Главная</p><a class="aside__menu-link" href="javascript: void(0);">Моя корзина</a><a class="aside__menu-link" href="javascript: void(0);">Мои доставки</a><a class="aside__menu-link" href="javascript: void(0);">Избранное</a><a class="aside__menu-link" href="javascript: void(0);">Сравнение</a><a class="aside__menu-link" href="javascript: void(0);">Мои данные</a><a class="aside__menu-link" href="javascript: void(0);">История заказов</a><a class="aside__menu-link" href="javascript: void(0);">Бонусные баллы</a><a class="aside__menu-link" href="javascript: void(0);">Выход</a>
            </div>
          </aside>
          <main class="personalarea__main-wrapper">
            <div class="personalarea__main-wrapper-grid">
              <div class="personalarea__main-wrapper-mybasket">
                <div class="personalarea__card-top personalarea__card-top-mybasket">Моя корзина</div>
                <div class="personalarea__card-center"><span><?=$basket['count']?> товара</span><br>
                  <p><?=$basket['price']?> Р</p>
                </div>
                <div class="personalarea__card-bottom"><a class="personalarea__card-bottom-link-button" href="javascript: void(0);">Подробнее</a></div>
              </div>
              <div class="personalarea__main-wrapper-mydelivery">
                <div class="personalarea__card-top personalarea__card-top-mydelivery">&nbsp;Мои доставки</div>
                <div class="personalarea__card-center"><span>Ближайшая:</span><br>
                  <p>11.06.2018</p>
                </div>
                <div class="personalarea__card-bottom"><a class="personalarea__card-bottom-link-button" href="javascript: void(0);">Подробнее</a></div>
              </div>
              <div class="personalarea__main-wrapper-favorites">
                <div class="personalarea__card-top personalarea__card-top-favorites">Избранное</div>
                <div class="personalarea__card-center"><span>В листе ожидания:</span><br>
                  <p>4 товара</p>
                </div>
                <div class="personalarea__card-bottom"><a class="personalarea__card-bottom-link-button" href="javascript: void(0);">Подробнее</a></div>
              </div>
              <div class="personalarea__main-wrapper-mydetails">
                <div class="personalarea__card-top personalarea__card-top-mydetails">Мои данные</div>
                <div class="personalarea__card-center"><span class="personalarea__card-center-text-block">Управление вашим профилем</span></div>
                <div class="personalarea__card-bottom personalarea__card-bottom-mydetails"><a class="personalarea__card-bottom-link" href="javascript: void(0);">Николай Иванов</a></div>
              </div>
              <div class="personalarea__main-wrapper-historyofororders">
                <div class="personalarea__card-top personalarea__card-top-historyofororders">История заказов</div>
                <div class="personalarea__card-center"><span class="personalarea__card-center-text-historyofororders">История ваших покупок во всех подробностях</span></div>
                <div class="personalarea__card-bottom"><a class="personalarea__card-bottom-link-button" href="javascript: void(0);">Перейти в историю</a></div>
              </div>
              <div class="personalarea__main-wrapper-bonuspoints">
                <div class="personalarea__card-top personalarea__card-top-bonuspoints">&nbsp;Бонусные баллы</div>
                <div class="personalarea__card-center"><span>На вашем бонусном счете:</span><br>
                  <p>550 Р</p>
                </div>
                <div class="personalarea__card-bottom"><a class="personalarea__card-bottom-link-button" href="javascript: void(0);">Подробнее</a></div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
    <div class="helpful">
      <div class="container">
        <div class="helpful__container-flex">
          <div class="helpful__container-imagetext">
            <div class="helpful__container-image"><img class="helpful__image-envelope" src="images/icon/svg/envelope.svg" alt="envelope"></div>
            <div class="helpful__container-text">
              <p class="helpful__text">Только полезная информация, новинки и скидки для своих!</p>
            </div>
          </div>
          <div class="helpful__container-inputbutton">
            <div class="helpful__container-input">
              <input class="helpful__input-email" type="text" placeholder="Введите Ваш E-mail">
            </div>
            <div class="helpful__container-button">
              <button class="helpful__button-join">Вступить в клуб</button>
            </div>
          </div>
        </div>
      </div>
    </div>
	<div class="row">
		<div class="col-md-12 sale-personal-section-index">
			<div class="row sale-personal-section-row-flex">
				<?
				foreach ($availablePages as $blockElement)
				{
					?>
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="sale-personal-section-index-block bx-theme-<?=$theme?>">
							<a class="sale-personal-section-index-block-link" href="<?=htmlspecialcharsbx($blockElement['path'])?>">
								<span class="sale-personal-section-index-block-ico">
									<?=$blockElement['icon']?>
								</span>
								<h2 class="sale-personal-section-index-block-name">
									<?=htmlspecialcharsbx($blockElement['name'])?>
								</h2>
							</a>
						</div>
					</div>
					<?
				}
				?>
			</div>
		</div>
	</div>
	<?
}
?>
