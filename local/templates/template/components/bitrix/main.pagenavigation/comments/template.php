<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

/** @var PageNavigationComponent $component */
$component = $this->getComponent();

$this->setFrameMode(true);

function page($number,$arResult){
	if($number==1){}
	return $arResult['URL']."?".$arResult['ID'].'=page-'.$number;

}

?>
<?if($arResult['PAGE_COUNT']>1):?>
	<div class="reviews__pagination"><a class="pagination-cnt" <?if($arResult['CURRENT_PAGE']!=1):?>  href="<?=page(1,$arResult)?>"<?endif;?>>В начало</a>
<?for($i=1;$i<=$arResult['PAGE_COUNT'];$i++):?>
<?if($arResult['PAGE_COUNT']<4):?>
<a class="pagination-page <?if($i==$arResult['CURRENT_PAGE']):?>pagination-page--active<?endif?>" 
<?if($arResult['CURRENT_PAGE']!=$i):?>  
href="<?=page($i,$arResult)?>"<?endif;?>
			><?=$i?></a>
<?else:?>
<?if($i<4):?>
		<a class="pagination-page <?if($i==$arResult['CURRENT_PAGE']):?>pagination-page--active<?endif?>" 
<?if($arResult['CURRENT_PAGE']!=$i):?>  
href="<?=page($i,$arResult)?>"<?endif;?>
			><?=$i?></a>		
<?endif;?>
<?if($i==$arResult['END_PAGE']):?>
<a class="pagination-page" href="">.</a>
<a class="pagination-page <?if($i==$arResult['CURRENT_PAGE']):?>pagination-page--active<?endif?>" 
<?if($arResult['CURRENT_PAGE']!=$i):?>  href="<?=page($i,$arResult)?>"<?endif;?>
			><?=$i?></a>
<?endif;?>
<?endif;?>			
<?endfor;?>
		<a class="pagination-cnt" <?if($arResult['CURRENT_PAGE']!=$arResult['END_PAGE']):?>  href="<?=page($arResult['CURRENT_PAGE']+1,$arResult)?>"<?endif;?>>Следующая</a></div>
<?endif;?>
