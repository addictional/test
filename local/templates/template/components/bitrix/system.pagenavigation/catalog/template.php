<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

/** @var PageNavigationComponent $component */
$component = $this->getComponent();

$this->setFrameMode(true);

function page($number,$arResult){
	if($number==1){}
	return $arResult['sUrlPathParams'].'PAGEN_1='.$number;

}

?>

<?if($arResult['NavPageCount']>1):?>
<?if($arResult['NavPageNomer']!=$arResult['nEndPage']):?>
<button class="catalog__showbutton-button">Показать еще</button>
<?endif;?>
              </div>
              <div class="catalogBot">
	<div class="catalog__showoptions-wrapper"><a <?if($arResult['NavPageNomer']!=1):?>  href="<?=page(1,$arResult)?>"<?endif;?>><button class="catalog__showoptions-button-tostart"></button></a>
<?for($i=1;$i<=$arResult['NavPageCount'];$i++):?>
<?if($arResult['NavPageCount']<4):?>
<a class="catalog__showoptions-link<?if($i==$arResult['NavPageNomer']):?>-disabled<?endif?>" 
<?if($arResult['NavPageNomer']!=$i):?>  
href="<?=page($i,$arResult)?>"<?endif;?>
			><?=$i?></a>
<?else:?>
<?if(($i==$arResult['NavPageNomer']||$i==$arResult['NavPageNomer']-1||$i==$arResult['NavPageNomer']+1)&&$i!=$arResult['nEndPage']):?>
		<a class="catalog__showoptions-link<?if($i==$arResult['NavPageNomer']):?>-disabled<?endif?>" 
<?if($arResult['NavPageNomer']!=$i):?>  
href="<?=page($i,$arResult)?>"<?endif;?>
			><?=$i?></a>		
<?endif;?>
<?if($i==$arResult['nEndPage']):?>
<?if(($arResult['NavPageNomer']+1!=$arResult['nEndPage'])||($arResult['NavPageNomer']!=$arResult['nEndPage'])):?>
<span class="catalog__showoptions-text-points">........</span>
<?endif;?>
<a class="catalog__showoptions-link<?if($i==$arResult['NavPageNomer']):?>-disabled<?endif?>" 
<?if($arResult['NavPageNomer']!=$i):?>  href="<?=page($i,$arResult)?>"<?endif;?>
			><?=$i?></a>
<?endif;?>
<?endif;?>			
<?endfor;?>
		<a <?if($arResult['NavPageNomer']!=$arResult['nEndPage']):?>  href="<?=page($arResult['NavPageNomer']+1,$arResult)?>"<?endif;?>><button class="catalog__showoptions-button-next"></button></a></div>
<?endif;?> 

