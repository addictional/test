<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

/** @var PageNavigationComponent $component */
$component = $this->getComponent();

$this->setFrameMode(true);

function page($number,$arResult){
	if($number==1){}
	return $arResult['sUrlPathParams'].'PAGEN_1='.$number;

}

?>

<?if($arResult['NavPageCount']>1):?>

      <div class="historyti__links-wrapper">        
              
<?for($i=1;$i<=$arResult['NavPageCount'];$i++):?>
<?if($arResult['NavPageCount']<4):?>
<a class="historyti__pagenumbers-link<?if($i==$arResult['NavPageNomer']):?>-opened<?endif?>" 
<?if($arResult['NavPageNomer']!=$i):?>  
href="<?=page($i,$arResult)?>"<?endif;?>
			><?=$i?></a>
<?else:?>
<?if(($i==$arResult['NavPageNomer']||$i==$arResult['NavPageNomer']-1||$i==$arResult['NavPageNomer']+1)&&$i!=$arResult['nEndPage']):?>
		<a class="historyti__pagenumbers-link<?if($i==$arResult['NavPageNomer']):?>-opened<?endif?>" 
<?if($arResult['NavPageNomer']!=$i):?>  
href="<?=page($i,$arResult)?>"<?endif;?>
			><?=$i?></a>		
<?endif;?>
<?if($i==$arResult['nEndPage']):?>
<?if(($arResult['NavPageNomer']+1!=$arResult['nEndPage'])||($arResult['NavPageNomer']!=$arResult['nEndPage'])):?>
<a class="historyti__pagenumbers-link-points">........</a>
<?endif;?>
<a class="historyti__pagenumbers-link<?if($i==$arResult['NavPageNomer']):?>-opened<?endif?>" 
<?if($arResult['NavPageNomer']!=$i):?>  href="<?=page($i,$arResult)?>"<?endif;?>
			><?=$i?></a>
<?endif;?>
<?endif;?>			
<?endfor;?>
	</div>	
<?endif;?> 

