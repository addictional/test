<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;




Loc::loadMessages(__FILE__);
// echo "<pre>"; print_r($arResult); echo "<pre>";
?>
<div class="historyti__big-wrapper">
      <div class="container">
        <div class="historyti__wrapper-flex">
          <aside class="aside__menu-wrapper">
            <div class="aside__menu-container-type">
              <a class="aside__menu-link" href="/personal/">Главная</a>
              <a class="aside__menu-link" href="/personal/cart/">Моя корзина</a>
              <a class="aside__menu-link" href="/personal/delivery-hystory.php">Мои доставки</a>
              <a class="aside__menu-link" href="javascript: void(0);">Избранное</a>
              <a class="aside__menu-link" href="javascript: void(0);">Сравнение</a>
              <a class="aside__menu-link aside__menu-link-borderbottom" href="/personal/profile.php">Мои данные</a>
              <p class="aside__menu-link-opened">История заказов</p><a class="aside__menu-link" href="javascript: void(0);">Бонусные баллы</a><a class="aside__menu-link" href="javascript: void(0);">Выход</a>
            </div>
          </aside>
          <main class="historyti__main-wrapper">
            <p class="historyti__main-history-text">Здесь отображаются все ваши сделанные заказы на сайте, вы можете посмотреть статус заказа и его состав:</p>
            <table class="historyti__main-history-table" cellspacing="0" cellpadding="0">
              <thead>
                <tr>
                  <td>№ и дата заказа</td>
                  <td>Сумма заказа</td>
                  <td>Доставка</td>
                  <td>Дата доставки</td>
                  <td>Статус</td>
                </tr>
              </thead>
              <tbody>
              	<?foreach($arResult['ORDERS'] as $key => $order):?>
              	<tr>
                  <td>№ <?=$order['ORDER']['ID']?><br><?=$order['ORDER']['DATE_INSERT_FORMATED']?></td>
                  <td><?=$order['ORDER']['PRICE']?> Р</td>
                  <td><?=$order['SHIPMENT'][0]['DELIVERY_NAME']?></td>
                  <td><?=$order['ORDER']['DATE_INSERT_FORMATED']?></td>
                  <td><?=$arResult['INFO']['STATUS'][$order['ORDER']['STATUS_ID']]['NAME']?></td>
                </tr>
              	<?endforeach;?>
              </tbody>
            </table>
            <?=$arResult['NAV_STRING']?>
          </main>
        </div>
      </div>
    </div>
