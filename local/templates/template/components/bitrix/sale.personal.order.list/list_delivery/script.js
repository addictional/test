class Filter {
	constructor(dateFrom,dateTo,quantity)
	{
		this.date_from=document.querySelector('.historyno__searchform-input-datefrom').value.replace(/\-/,".");;
		this.date_to=document.querySelector('.historyno__searchform-input-dateto').value.replace(/\-/,".");;
		this.quan_tity=document.querySelector('.historyno__searchform-quantity-select').options[document.querySelector('.historyno__searchform-quantity-select').selectedIndex].value;
	}
	static init(){
		return new this();
	}
	get dateFrom()
	{
		return this.formatDate(this.date_from);
	}
	get dateTo()
	{
		return this.formatDate(this.date_to);
	}
	get quantity()
	{
		return this.quan_tity;
	}
	formatDate(date){
		var date = new Date(date);
		var month =  date.getMonth()+1;
		if(month<10)
			month ="0"+month;  
		return date.getDate()+"."+month+"."+date.getFullYear();
	}
	static insertCollection(collection)
	 {
	 	let string = "";
	 	console.log(collection);
	 	for(let index in collection){
	 		if(index==0)
	 		{
	 			let topContent = collection[index].querySelector('.list__delivery').innerHTML;
	 			let bottomElements = collection[index].querySelector('.prodyct__table--ilst').innerHTML;
	 			let bottomPrice = collection[index].querySelector('.list__endprice').innerHTML;
	 			document.querySelector('.element-ajax').querySelector('.list__delivery').innerHTML = topContent;
	 			document.querySelector('.element-ajax').querySelector('.prodyct__table--ilst').innerHTML =bottomElements;
	 			document.querySelector('.element-ajax').querySelector('.list__endprice').innerHTML = bottomPrice;
	 		}
	 		else if(index < collection.length) 
	 		{
	 			string += collection[index].innerHTML;
	 		}
	 	}
	 	document.querySelector('#content-delivery').innerHTML =string;
	 }
}
document.addEventListener('DOMContentLoaded',()=>{
	document.querySelector('.historyno__searchform-input-datefrom').addEventListener('input',()=>{ 
		var data = Filter.init();
		console.log(data.dateTo);
		$.ajax({data:{'filter_date_from': data.dateFrom, 'filter_date_to' : data.dateTo , 'quantity': data.quantity }})
		.then((response)=>{Filter.insertCollection($(response).find('.element-ajax'));}).catch((err)=>{console.log(err);})
	});
	document.querySelector('.historyno__searchform-input-dateto').addEventListener('input',()=>{ 
		var data = Filter.init();
		$.ajax({data:{'filter_date_from': data.dateFrom,'filter_date_to' : data.dateTo, 'quantity': data.quantity}})
		.then((response)=>{Filter.insertCollection($(response).find('.element-ajax'));}).catch((err)=>{console.log(err);})
	});
	document.querySelector('.historyno__searchform-quantity-select').onchange = changeHandler;
});
function changeHandler (e)
{
	var data = Filter.init();
		$.ajax({data:{'filter_date_from': data.dateFrom,'filter_date_to' : data.dateTo, 'quantity': data.quantity}})
		.then((response)=>{Filter.insertCollection($(response).find('.element-ajax'));}).catch((err)=>{console.log(err);})
}
