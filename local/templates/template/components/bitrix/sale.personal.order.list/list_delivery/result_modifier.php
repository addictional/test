<?php

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;

if(Loader::includeModule('iblock'))
{
	// echo "<pre>"; print_r($arResult); echo "<pre>";
	foreach ($arResult['ORDERS'] as $key => $order) {
		foreach($order['BASKET_ITEMS'] as $clue =>$item)
		{
			$array[$item['PRODUCT_ID']] = $item['PRODUCT_ID'];
		}
	}
}

$res = CIBlockElement::GetList(array("SORT" => "ASC"),array("IBLOCK_ID" => 4, "ID" =>$array),false,false,array('PREVIEW_PICTURE','ID','IBLOCK_ID'));
while ($ob = $res->GetNextElement()) {
	$fields = $ob->GetFields();
 	$arResult['items_properies'][$fields['ID']]['IMG'] = CFile::GetPath($ob->GetFields()['PREVIEW_PICTURE']);
 	$arResult['items_properies'][$fields['ID']]['PROPRETIES'] = $ob->GetProperties();
 } 
// echo "<pre>"; print_r($arResult['items_properies']); echo "<pre>";
