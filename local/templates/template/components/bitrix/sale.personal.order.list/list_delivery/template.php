<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;



Loc::loadMessages(__FILE__);
// echo "<pre>"; print_r($arResult); echo "<pre>";
?>

<div class="historyti__big-wrapper">
      <div class="container">
        <div class="historyno__wrapper-flex">
          <aside class="aside__menu-wrapper">
            <div class="aside__menu-container-type">
              <a class="aside__menu-link" href="/personal/">Главная</a>
              <a class="aside__menu-link aside__menu-link-borderbottom" href="/personal/cart/">Моя корзина</a>
              <p class="aside__menu-link-opened">Мои доставки</p>
              <a class="aside__menu-link" href="javascript: void(0);">Избранное</a>
              <a class="aside__menu-link" href="/personal/comparison.php">Сравнение</a>
              <a class="aside__menu-link" href="/personal/profile.php">Мои данные</a>
             <a class="aside__menu-link" href="/personal/history-order.php">История заказов</a><a class="aside__menu-link" href="javascript: void(0);">Бонусные баллы</a><a class="aside__menu-link" href="javascript: void(0);">Выход</a>
            </div>
          </aside>
          <div class="delivery__content">
          	<?foreach($arResult['ORDERS'] as $key => $order):?>
            <?if($key == 1):?>
              <div id="content-delivery">
              <?endif;?>
            <div class="element-ajax">
            <ul class="list__delivery">
            <ul class="list__endprice--ul">
              <li>Заказ: <span class=""> № <?=$order['ORDER']['ID']?> </span></li>
              <li>Дата заказа: <span class=""><?=$order['ORDER']['DATE_INSERT_FORMATED']?></span></li>
            </ul>

            <ul class="list__endprice--ul">
              <li>Доставка: <span class=""> <?=$order['SHIPMENT'][0]['DELIVERY_NAME']?></span></li>
              <li>Дата доставки: <span class=""> 25.07.2018</span></li>
            </ul>
            <ul class="list__endprice--ul">
              <li>Статус заказа: <span><?=$arResult['INFO']['STATUS'][$order['ORDER']['STATUS_ID']]['NAME']?></span></li>
            </ul>
          </ul>
          
            <?if($key == 0):?>
            <form class="historyno__searchform" >
              <div class="historyno__searchform-wrapper-flex">
                <div class="historyno__searchform-dates-wrapper"><span class="historyno__searchform-datefrom-text">c</span>
                  <input class="historyno__searchform-input-datefrom" type="date" value="2017-01-01"><span class="historyno__searchform-dateto-text">по</span>
                  <input class="historyno__searchform-input-dateto" type="date" value="<?=date("Y-m-d")?>">
                </div>
                <div class="historyno__searchform-quantity-wrapper">Показывать по:
                  <select class="historyno__searchform-quantity-select">
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                  </select>
                </div>
              </div>
            </form>
            <?endif;?>
          
            <table class="prodyct__table--ilst">
              <tr class="prodyct__table--title">
                <th>Товар</th>
                <th>Название</th>
                <th>Кол-во</th>
                <th>Цена</th>
                <th>Сумма</th>
              </tr>
              <?foreach($order['BASKET_ITEMS'] as $clue => $item):?>
              <tr>
                <td class="prodyct__table--img"><img class="prodyct__table--imges" src="<?=$arResult['items_properies'][$item['PRODUCT_ID']]['IMG']?>"></td>
                <td class="prodyct__table--discr"><?=$item['NAME']?>, 18 функций, кожаный чехол</td>
                <td class="prodyct__table--stock"><?=$item['QUANTITY']?> шт.</td>
                <td class="prodyct__table--price"><?=intval($item['PRICE'])?> Р</td>
                <td class="prodyct__table--allprice"><?=$item['PRICE']*$item['QUANTITY']?> Р</td>
              </tr>
              <?endforeach;?>
            </table>
            <ul class="list__endprice">
              <ul class="list__endprice--ul">
                <li>Сумма предоставленной скидки: <span class=""><?=intval($order['ORDER']['DISCOUNT_VALUE'])?> Р</span></li>
                <li>Бонусов за покупку: <span class="">500 Р</span></li>
              </ul>
              <ul class="list__endprice--ul02">
                <li class="list__endprice--full">Стоимость товаров: <span class=""><?=intval($order['ORDER']['PRICE']-$order['ORDER']['PRICE_DELIVERY'])?> Р</span></li>
                <li class="list__endprice--full ">Стоимость доставки: <span class=""><?=intval($order['ORDER']['PRICE_DELIVERY'])?> Р</span></li>
                <li class="list__endprice--full list__endprice--full--itog endprice__full">Итого: <span class="list__endprice--full--itog"><?=intval($order['ORDER']['PRICE'])?>
                    Р</span></li>
              </ul>
            </ul>
            <?if($key == end($arResult['ORDERS'])):?>
          </div>
            <?endif;?>
          </div>
          <?endforeach;?>
      </div>
        </div>
      </div>
    </div>
     
          