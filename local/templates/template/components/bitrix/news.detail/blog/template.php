<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
// echo "<pre>"; print_r($arResult); echo "<pre>";
?>
  <div class="articles__bigtitle-wrapper">
      <div class="container">
        <div class="articles__bigtitle-text"><?=$arResult["NAME"]?></div>
      </div>
    </div>
    <div class="articles__allpage-wrapper">
      <div class="container">
        <div class="articles__bothsides-wrapper">
          <aside class="articles__menu-wrapper">
            <div class="articles__menu-container-type"><a class="articles__menu-link" href="javascript: void(0);">О компании</a><a class="articles__menu-link" href="javascript: void(0);">О бренде</a><a class="articles__menu-link" href="javascript: void(0);">Бонусная программа</a>
              <p class="articles__menu-link-opened">Наш блог</p>
            </div>
            <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.top",
	"recomend_blog",
	Array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"BASKET_URL" => "/personal/basket.php",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPATIBLE_MODE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "",
		"DETAIL_URL" => "",
		"DISPLAY_COMPARE" => "N",
		"ELEMENT_COUNT" => "4",
		"ELEMENT_SORT_FIELD" => "id",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "RAND",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"LABEL_PROP" => "",
		"LINE_ELEMENT_COUNT" => "3",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"OFFERS_LIMIT" => "3",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(0=>"BASE",),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => "",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => array("={{'VARIANT':'3'}","BIG_DATA':false"),
		"PRODUCT_SUBSCRIPTION" => "Y",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE_MOBILE" => "",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"VIEW_MODE" => "SECTION"
	)
);?>
          </aside>	
<main class="articles__main-wrapper">
            <div class="articles__main-wrapper-grid">
            	<div>
             <?echo $arResult["DETAIL_TEXT"];?>
          </div>
              <div class="articles__socials-wrapper"><script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="//yastatic.net/share2/share.js"></script>
<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter"></div><br><img class="articles__socials-image-greystar" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/grey-star-small.png"><img class="articles__socials-image-greystar" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/grey-star-small.png"><img class="articles__socials-image-greystar" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/grey-star-small.png"><img class="articles__socials-image-greystar" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/grey-star-small.png"><img class="articles__socials-image-greystar" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/grey-star-small.png"><span class="articles__socials-text">&nbsp;(Пока оценок нет)</span><br><span class="articles__socials-text">Просмотров: 16</span></div>
              <div class="articles__form-wrapper">
                <div class="articles__form-title">Оставить комментарий</div>
                <form class="articles__form" id="articles__form" action="">
                  <div class="articles__form-grid">
                    <div class="articles__form-inputname-wrapper">Ваше имя<br>
                      <input class="articles__form-inputname-input" type="text" placeholder="Введите ваше имя">
                    </div>
                    <div class="articles__form-inputmail-wrapper">E-mail<br>
                      <input class="articles__form-inputmail-input" type="text" placeholder="Введите ваш E-mail">
                    </div>
                    <div class="articles__form-inputcomment-wrapper">Комментарий<br>
                      <textarea class="articles__form-inputcomment-textarea" type="text" placeholder="Ваш комментарий"></textarea>
                    </div>
                    <div class="articles__form-checkboxsubmit-wrapper">
                      <div class="checkbox-block checkbox-block-desc">
                        <input class="checkbox" type="checkbox" id="present" value="present">
                        <label class="checkmark" for="present">Согласен(на) на обработкy<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;персональных данных</label>
                      </div>
                      <input class="articles__form-submit" type="submit" value="Отправить">
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </main>
      </div></div></div>