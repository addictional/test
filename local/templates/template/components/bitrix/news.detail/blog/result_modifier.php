<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

use Bitrix\Main\Loader; 

Loader::includeModule("highloadblock");
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;
use \Bitrix\Main\Data\Cache;



$hlbl=3;
$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch(); 

$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
$entity_data_class = $entity->getDataClass();
$rssData = $entity_data_class::getList(array(
   "select" => array("UF_STARS"),
   "order" => array("UF_DATE" => "ASC"),
   "filter" => array("UF_BLOG" => $arResult['ID']),
   'cache' => array(
        'ttl' => 180,
        'cache_joins' => true,
    )  
));
$arResult['stars']['count']=$rssData->getSelectedRowsCount();
for ($i=1; $i <= 5 ; $i++) { 
	$arResult['stars'][$i]=0;
}
while($arData = $rssData->Fetch()){
	$arResult['stars'][$arData['UF_STARS']]++;
   } 
// $arResult['stars']['rate']=(1*$arResult['stars'][1]+2*$arResult['stars'][2]+3*$arResult['stars'][3]+4*$arResult['stars'][4]+5*$arResult['stars'][5])/$arResult['stars']['count']; 
$arResult['stars']['rate']=number_format((float)(1*$arResult['stars'][1]+2*$arResult['stars'][2]+3*$arResult['stars'][3]+4*$arResult['stars'][4]+5*$arResult['stars'][5])/$arResult['stars']['count'], 1, '.', '');
for ($i=1; $i <= 5 ; $i++) { 
	$arResult['stars_percent'][$i]=intval(100/$arResult['stars']['count']*$arResult['stars'][$i]);
} 
$count = array('1');
$nav = new \Bitrix\Main\UI\PageNavigation('nav-comments');
$nav->allowAllRecords(true)
   ->setPageSize(3)
   ->initFromUri();
$rsData = $entity_data_class::getList(array(
   "select" => array("*"),
   "count_total" => true,
   "offset" => $nav->getOffset(), //из объекта пагинации добавляем смещение для HighloadBlock
    "limit" => $nav->getLimit(),
   "order" => array("UF_DATE" => "ASC"),
   "filter" => array("UF_BLOG" => $arResult['ID']),
   'cache' => array(
        'ttl' => 180,
        'cache_joins' => true,
    )   // Задаем параметры фильтра выборки
));
$arResult['comment-nav']=$nav->setRecordCount($rsData->getCount());
while($arData = $rsData->Fetch()){
	$arResult['comment'][]=$arData;
   }
$entity = HL\HighloadBlockTable::compileEntity(5);
$entity_data_class = $entity->getDataClass(); 
$resData = $entity_data_class::getList(array(
      'select' => array('UF_FILE','UF_XML_ID','UF_NAME'),
      'filter' => array('UF_XML_ID' => $arResult['PROPERTIES']['accomplishments']['VALUE']),
      'order' => array('ID' => 'ASC'),
      'limit' => 100000
   ));
$i=0;
   while($result = $resData->Fetch()){
  $arResult['accomplishments'][$i]=$result;
  $arResult['accomplishments'][$i++]['SRC']=CFile::GetPath($result['UF_FILE']);
   }  