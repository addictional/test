var minus,plus,sort;
var images = [];
document.addEventListener('DOMContentLoaded',function(){
	sort = document.querySelector("a.selected-sort").getAttribute('href');
	var defaultMinus = document.querySelector('#minCostRR').value;
	var DefaultPlus=document.querySelector('#maxCostRR').value;
	minus=document.querySelector('#minCostRR').value;
	plus=document.querySelector('#maxCostRR').value;
	var pagination=document.querySelector('select .catalog__model-select'); 
	var buttons=document.querySelectorAll('button.item__buttons-inbasket.item__buttons-inbasket-catalog');
	var basket=document.querySelector('a.header-options__link-basket').innerHTML.match(/\d+/)[0];
	buttons.forEach(function(element,index,){
		element.addEventListener('click',function(e){			
				$.ajax({
				url: e.target.getAttribute('data-basket'),
				beforeSend: function(){
					e.target.setAttribute('disabled','');
				},
				success: function(data){
					basket++;
					document.querySelector('a.header-options__link-basket').innerHTML="Корзина ("+basket+")";
					e.target.removeAttribute('disabled');
				}
			});
			
		});
	});
	// $('select').on('change',(event)=>{
	// 	let headers = new Headers();
	// 	headers.set('Content-Type', 'application/json')
	// 	let value = event.target.getAttribute('value');
	// 	let url = event.target.getAttribute('href')
	// 	fetch(url,{
	// 		method: 'post',
	// 		body: JSON.stringify({
	// 			value: value
	// 		})
	// 	}).then((response)=> {
	// 		console.log(response.json());
	// 		return response.text();}).then((json) => {console.log(json);}).catch((error)=>{console.log("error: "+error);})
	// });
	document.querySelectorAll('a.catalog__menu-link').forEach((element,index)=>{
		element.addEventListener('click',(e)=>{
			e.preventDefault();
			let href = e.target.getAttribute('href');
			console.log(href);
			$.ajax({url :href,
				complete : function(){
    }})
			.then((response)=>{

				Filter.insertCollection($(response).find('.catalog__item'));
				document.querySelector('p.catalog__menu-link-opened').classList.toggle('catalog__menu-link-opened');
				e.target.innerHTML='<p href="'+href+'" class="catalog__menu-link-opened">'+e.target.innerText+'</p>';
				document.querySelector('#minCostRR').value = defaultMinus;
				document.querySelector('#maxCostRR').value = DefaultPlus;
			});
		});
	});
	document.querySelector('#minCostRR').onchange = filterMinus;
	document.querySelector('#maxCostRR').onchange = filterPlus;
	document.querySelectorAll("a.catalog__sorting-link").forEach((element,index)=>{
		element.addEventListener('click',(e)=>{
			e.preventDefault();
			document.querySelectorAll('a.catalog__sorting-link').forEach((element,index)=>{
				element.style = '';
			});
			e.target.style = 'font-weight: 500; color: black;';
			sort = e.target.getAttribute('href');
			Filter.httpRequest(minus,plus,e,sort);
		});
	});
	document.querySelector('.catalogFX').addEventListener('click',(e)=>{
		if(e.target.classList.contains('item__buttons-heart'))
		{
			$.ajax({url: '/local/ajax/comments.php',
					data: {"favorite" :e.target.getAttribute('data-id')},
					complete : function(){
        console.log(this.url);
    }
		}).then((response)=>{console.log(response);});
		}
		if (e.target.classList.contains('item__buttons-fastlook')) 
		{
			document.querySelector('#small-img-roll').innerHTML="";
			document.querySelector('#small-img-roll').style.transform = "translateX(0px)"
			$.ajax({url:"/local/ajax/getimages.php",
				data: {"images": e.target.getAttribute('data-id')}
		}).then((response)=> JSON.parse(response)).then((data)=>{for(let elem in data)
			{
				images[elem] = new Image(70,70);
				images[elem].src = data[elem];
				images[elem].style.cursor = "pointer";
				images[elem].style.transform = "rotate(270deg)";
				document.querySelector('#small-img-roll').innerHTML += '<div id="'+elem+'" class="inner228"><div class="loader-ajax"></div></div>'
				images[elem].addEventListener("load",()=>{
					document.getElementById(elem).innerHTML ="";
					document.getElementById(elem).appendChild(images[elem]);  });
			}
			let count = 3;
			document.querySelector('.icon-right').onclick = function(e){
				e.preventDefault();
				count++
				if(count>images.length)
				{
					count =images.length;
					return;
				}	
				document.querySelector('#small-img-roll').style.transform = "translateX(-"+80*(count%3)+"px)"
				
			}
			document.querySelector('.icon-left').onclick = function(e){
				e.preventDefault();
				count--;
				if(count<3)
				{
					count =3;
					return;
				}			
				document.querySelector('#small-img-roll').style.transform = "translateX(-"+80*(count%3)+"px)"
				
			}
		});
		document.querySelector('p.new-price--info').innerHTML = e.target.getAttribute('data-discount');
		if(e.target.getAttribute('data-discount')==e.target.getAttribute('data-price'))
			document.querySelector('p.old-price--info').innerHTML = '';
		else
			document.querySelector('p.old-price--info').innerHTML = e.target.getAttribute('data-price');
		if(e.target.getAttribute('data-diff')=='0')
		{
			document.querySelector('.sale-percent__info').style.display = "none";
		}
		else
		{
			document.querySelector('.sale-percent__info').style.display = "";
			document.querySelector('.sale-percent__info').innerHTML = "-"+e.target.getAttribute('data-diff')+" %";
		}
		if(e.target.getAttribute('data-add')=="")
		{
			document.querySelector('.case__info').style.display = "none";
		}
		else
		{
			document.querySelector('.case__info').style.display = "";
		}
		document.querySelector('.tools__info span').innerText = " "+e.target.getAttribute('data-count');
		document.querySelector('.popup__prodyct-cart').style.display = "block";
		document.querySelector('.btn-yellow--info').setAttribute('data-basket',e.target.getAttribute('data-basket'));
		document.querySelector('.btn-clear--info').setAttribute('data-basket',e.target.getAttribute('data-basket'));
		}
		if (e.target.classList.contains('item__buttons-diagram'))
		{
			$.ajax({url: '/local/ajax/compare.php',
					data: {"compare" :e.target.getAttribute('data-compare')},
					complete : function(){
        console.log(this.url);
    }
		}).then((response)=>{console.log(response);});
		}
	});

});
class Filter {
	constructor(){}
	static insertCollection(collection)
	 {
	 	let string = "";
	 	for(let index in collection){
	 		
	 		if(index < collection.length) 
	 		{
	 			string += collection[index].innerHTML;
	 		}
	 	}
	 	document.querySelector('.catalogFX').innerHTML =string;
	 }
	 static httpRequest(min,max,e,sort,)
	 {
	 	let this_ = this;
	 	$.ajax(
	 	{
	 		url: document.querySelector('a.catalog__menu-link').getAttribute('href'),
	 		data : {"price_from" : min, "price_to" : max ,'sort' : sort},
	 		complete : function(){
        console.log(this.url);
    }

	 	}
	 		)
	 	.then((response)=>{
				this_.insertCollection($(response).find('.catalog__item'));
				document.querySelector('p.catalog__menu-link-opened').classList.toggle('catalog__menu-link-opened');
				document.querySelector('a.catalog__menu-link').innerHTML = '<p href="'+document.querySelector('a.catalog__menu-link').getAttribute('href')+'" class="catalog__menu-link-opened">'+document.querySelector('a.catalog__menu-link').innerText+'</p>';
			});
	 	}
	 
}

function filterMinus(e)
{
	minus = e.target.value;
	Filter.httpRequest(minus,plus,e,sort);
}
function filterPlus(e)
{
	plus = e.target.value;
	Filter.httpRequest(minus,plus,e,sort);
}