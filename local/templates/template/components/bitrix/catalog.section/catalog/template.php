<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);
// echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<!-- filtr start -------------------------->
<div class="catalog__bigtitle-wrapper">
      <div class="container">
        <div class="catalog__bigtitle-text">	Мультитулы Leatherman</div>
        <div class="catalog__bigtitle-filter-wrapper">
          <select class="catalog__bigtitle-select" name="bigtitle-select">
            <option value="min">По цене min</option>
            <option value="max">По цене max</option>
          </select><a class="catalog__bigtitle-button-filter" onclick="showCatPop()">Фильтр</a>
          <div id="catPop">
            <div class="sect"><a class="sectLink">Тип</a>
              <!-- <div class="sect-cont">
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="allM" value="present">
                  <label class="checkmark" for="allM">Все мультитулы</label>
                </div>
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="bigM" value="present">
                  <label class="checkmark" for="bigM">Полноразмерные мультитулы</label>
                </div>
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="strongM" value="present">
                  <label class="checkmark" for="strongM">Силовые мультитулы</label>
                </div>
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="keyM" value="present">
                  <label class="checkmark" for="keyM">Мультитулы-брелоки</label>
                </div>
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="pocketM" value="present">
                  <label class="checkmark" for="pocketM">Карманные инструменты</label>
                </div>
              </div> -->
              
              <!---menu_mobile---->

            </div>
            <div class="sect"><a class="sectLink">Цена</a>
              <div class="sect-cont">
                <div class="sect-cont--pr"><span class="priceF">от</span>
                  <input id="minCost" type="text" value="0"><span class="priceT">до</span>
                  <input id="maxCost" type="text" value="1000">
                  <div id="sliderRange"></div>
                </div>
              </div>
            </div>
            <div class="sect"><a class="sectLink">Модель</a>
              <div class="sect-cont">
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="testM" value="present">
                  <label class="checkmark" for="testM">Any type content</label>
                </div>
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="testtestM" value="present">
                  <label class="checkmark" for="testtestM">Any type content</label>
                </div>
              </div>
            </div>
            <div class="sect"><a class="sectLink">Инструменты</a>
              <div class="sect-cont">
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="testM" value="present">
                  <label class="checkmark" for="testM">Any type content</label>
                </div>
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="testtestM" value="present">
                  <label class="checkmark" for="testtestM">Any type content</label>
                </div>
              </div>
            </div>
            <div class="sect"><a class="sectLink">Чехол в комплекте</a>
              <div class="sect-cont">
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="testM" value="present">
                  <label class="checkmark" for="testM">Any type content</label>
                </div>
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="testtestM" value="present">
                  <label class="checkmark" for="testtestM">Any type content</label>
                </div>
              </div>
            </div>
            <div class="sect"><a class="sectLink">Упаковка</a>
              <div class="sect-cont">
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="testM" value="present">
                  <label class="checkmark" for="testM">Карманные инструменты</label>
                </div>
                <div class="checkbox-block">
                  <input class="checkbox" type="checkbox" id="testtestM" value="present">
                  <label class="checkmark" for="testtestM">Карманные инструменты</label>
                </div>
              </div>
            </div>
            <div class="sectReset"><a>Сбросить все фильтры</a></div>
            <div class="sectClose"><a>Закрыть фильтры ☓</a></div>
          </div>
        </div>
      </div>
    </div>
    <!-- filtr end -------------------------->
    <div class="catalog__allpage-wrapper">
      <div class="container">
        <div class="catalog__bothsides-wrapper">
          <aside class="catalog__menu-wrapper">
            <div class="catalog__menu-container-type">
        <?
        if(isset($arResult['IBLOCK_SECTION_ID']))
        {
          $sectionId=$arResult['IBLOCK_SECTION_ID'];
        }
        else
        {
          $sectionId=$arResult['ID'];
        }
        ?>     
<?$APPLICATION->IncludeComponent(
  "bitrix:menu",
  "catalog",
  Array(
    "ALLOW_MULTI_SELECT" => "N",
    "CHILD_MENU_TYPE" => "left",
    "COMPONENT_TEMPLATE" => "catalog",
    "DELAY" => "N",
    "MAX_LEVEL" => "2",
    "MENU_CACHE_GET_VARS" => array($sectionId),
    "MENU_CACHE_TIME" => "3600",
    "MENU_CACHE_TYPE" => "N",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "ROOT_MENU_TYPE" => "left",
    "USE_EXT" => "Y"
  ),
  $component
);?>
 
            </div>
            <?$APPLICATION->IncludeComponent(
  "bitrix:menu",
  "catalog",
  Array(
    "ALLOW_MULTI_SELECT" => "N",
    "CHILD_MENU_TYPE" => "left",
    "COMPONENT_TEMPLATE" => "catalog",
    "DELAY" => "N",
    "MAX_LEVEL" => "2",
    "MENU_CACHE_GET_VARS" => array(0=>"#SECTION_ID#",),
    "MENU_CACHE_TIME" => "3600",
    "MENU_CACHE_TYPE" => "N",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "ROOT_MENU_TYPE" => "left",
    "USE_EXT" => "Y"
  )
);?><?$APPLICATION->IncludeComponent(
  "bitrix:catalog.smart.filter",
  "filter",
  Array(
    "CACHE_GROUPS" => "Y",
    "CACHE_TIME" => "36000000",
    "CACHE_TYPE" => "A",
    "COMPONENT_TEMPLATE" => ".default",
    "CONVERT_CURRENCY" => "N",
    "DISPLAY_ELEMENT_COUNT" => "Y",
    "FILTER_NAME" => "arrFilter",
    "FILTER_VIEW_MODE" => "vertical",
    "HIDE_NOT_AVAILABLE" => "N",
    "IBLOCK_ID" => "4",
    "IBLOCK_TYPE" => "catalog",
    "PAGER_PARAMS_NAME" => "arrPager",
    "POPUP_POSITION" => "left",
    "PRICE_CODE" => array(0=>"BASE",),
    "SAVE_IN_SESSION" => "N",
    "SECTION_CODE" => "",
    "SECTION_DESCRIPTION" => "-",
    "SECTION_ID" => $sectionId,
    "SECTION_TITLE" => "-",
    "SEF_MODE" => "N",
    "TEMPLATE_THEME" => "blue",
    "XML_EXPORT" => "N"
  ),
  $component
);?>
            <div class="catalog__menu-banner-small">
              <div class="catalog__menu-banner-warranty"><span class="catalog__menu-banner-warranty-text">- 25 year - warranty</span></div>
              <div class="catalog__menu-banner-title">we build products we love, and also stand by.</div><a class="catalog__menu-banner-link" href="javascript: void(0);">Подробнее</a>
            </div>
          </aside>
          <main class="catalog__main-wrapper">
            <div class="catalog__main-wrapper-grid">
              <div class="catalog__sorting-wrapper">
                <span class="catalog__sorting-title">Сортировка:</span>
                <a class="catalog__sorting-link" href="id" style="    font-weight: 500; color: black;">Сначала популярные</a>
                <a class="catalog__sorting-link selected-sort" href="property_FUNCTIONS">Сначала функциональные</a>
                <a class="catalog__sorting-link" href="catalog__PRICE_1">По цене</a>
                <a class="catalog__sorting-link" href="CATALOG_WEIGHT">По массе</a>
                <a class="catalog__sorting-link" href="property_height">По размеру</a>
              </div>
              <div class="catalogFX">
              	

                
                <?foreach($arResult['ITEMS'] as $key => $arItem):?>
                <?
                $func=$arItem['PROPERTIES']['FUNCTIONS']['VALUE'];
$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], "Изменить элемент");
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], "Удалить элемент", "Уверены?");
?>
                 <div class="catalog__item" >
                	
                  <div class="item-wrapper" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="item__quantity">
                      <div class="item__quantity-container"><img class="item__quantity-image" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/tools.svg" alt="tools"><span class="item__quantity-span"><?=$func?></span></div>
                    </div>
                    <div class="item__image">
                    <div class="b-popup b-popup-show">
                      <div class="b-popup-content">
                        <button data-id="<?=$arItem['ID']?>" data-discount="<?=$arItem['PRICES']['BASE']['PRINT_VALUE_NOVAT']?>" data-price="<?=$arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE_NOVAT']?>"  data-basket="<?=$arItem['ADD_URL']?>" class="item__buttons-fastlook" data-diff="<?=$arItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']?>" data-count="<?=$func?>" <?if($arItem['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']):?>data-add="1"<?else:?>data-add=""<?endif;?>>Быстрый просмотр</button>
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>"><button class="item__buttons-detail">Подробнее</button></a>
                      </div>
                    </div>
                    <div class="item__image--img">
                      <img id="img-top" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" 
                      alt="<?=$arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']?>"
                      title="<?=$arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']?>" class="item__image--img">
                    </div>
                  </div>
                    <!-- <div class="item__image"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>"
                    	alt="<?=$arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']?>"
                    	title="<?=$arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']?>"></a></div> -->
                    <div class="item__bonus">
                      
                      <?if(DateMatch::DifferenceBetween($arItem['DATE_CREATE'],7)||$arItem['PROPERTIES']['NEW']['VALUE_XML_ID']):?>
                      <div class="item__bonus-container">Новинка</div>
                      <?endif;?>
                    </div>
                    <div class="item__about"><span class="item__about-text"><?=$arItem['NAME']?>, <?=$func?> <?=NameCounter::rewriter($func,'функция')?><?if($arItem['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']):?>, <?endif;?><?=$arItem['PROPERTIES']['ADDICTIONAL_PROPERTY']['VALUE']?></span></div>
                    <div class="item__stars-and-reviews">


                      <?for($i=1;$i<=5;$i++):?>
              <?if($i<=$arResult['comment'][$arItem['ID']]['rate']):?><img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><?else:?>
              <img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star_empty.png" alt="star">
              <?endif;?>
              <?endfor;?>

                      <a class="item__stars-and-reviews-link" href="javascript: void(0);"><?=$arResult['comment'][$arItem['ID']]['count']?> <?=NameCounter::rewriter($arResult['comment'][$arItem['ID']]['count'],'отзыв','','а','ов',0)?></a></div>
                    <div class="item__price"><span class="item__price-span"><?=$arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE_NOVAT']?></span><span class="item__price-span-underlined"><?if($arItem['PRICES']['BASE']['DISCOUNT_DIFF']):?><?=$arItem['PRICES']['BASE']['PRINT_VALUE_NOVAT']?><?endif;?></span></div>
                    <div class="item__buttons">
                      <button data-basket="<?=$arItem['ADD_URL']?>" class="item__buttons-inbasket item__buttons-inbasket-catalog">В корзину</button>
                      <button data-id="<?=$arItem['ID']?>" class="item__buttons-heart"></button>
                      <button data-compare="<?=$arItem['ID']?>" class="item__buttons-diagram"></button>
                    </div>
                  </div>
                </div> 
              	   <!-- <div class="novelties__item novelties-item-bg-white">
          <div class="item-wrapper">
            <div class="item__quantity">
              <div class="item__quantity-container"><img class="item__quantity-image" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/tools.svg"
                  alt="tools"><span class="item__quantity-span">17</span></div>
            </div>
            <div class="item__image">
              <div class="b-popup b-popup-show">
                <div class="b-popup-content">
                  <button class="item__buttons-fastlook">Быстрый просмотр</button>
                  <button class="item__buttons-detail">Подробнее</button>
                </div>
              </div>
              <img id="img-top" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="item__image--img">
            </div>
            <div class="item__bonus">
              <div class="item__bonus-container">Хит</div>
            </div>
            <div class="item__about"><span class="item__about-text">Мультититул Leatherman Charge ALX, 18 функций,
                кожаный чехол</span></div>
            <div class="item__stars-and-reviews"><img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><img
                class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg"
                alt="star"><img class="item__image-star" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><img class="item__image-star"
                src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/star.svg" alt="star"><a class="item__stars-and-reviews-link" href="javascript: void(0);">6
                отзывов</a></div>
            <div class="item__price"><span class="item__price-span">6 200 Р</span><span class="item__price-span-underlined"></span></div>
            <div class="item__buttons">
              <button class="item__buttons-inbasket">В корзину</button>
              <button class="item__buttons-heart"></button>
              <button class="item__buttons-diagram"></button>
            </div>
          </div>
        </div> -->
              	<?endforeach;?>

              </div>
              <div class="catalog__banner-big">
                <div class="catalog__banner-big-title">new!<br>black & silver surge</div><a class="catalog__banner-big-link" href="javascript: void(0);">Подробнее</a>
                <div class="catalog__banner-big-logo"></div>
              </div>
              <div class="catalogFX">

              </div>
              <div class="catalog__showbutton-wrapper">
                <?=$arResult['NAV_STRING']?>
                <div class="catalog__showoptions-displaymode-wrapper"><span class="catalog__showoptions-text-show">Показывать по</span>
                  <select href="test.php?&action=submit&bxajaxid=comp_2a5015b14a43574bd61828168ad372de" class="catalog__model-select" name="catalog-show">
                    <option value="s1">12</option>
                    <option value="s2">48</option>
                    <option value="s3">все</option>
                  </select>
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
