<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */
$iD = array();
foreach($arResult['ITEMS'] as $key => $item)
{
	$iD[$key]=$item['ID'];
	$arResult['comment'][$item['ID']]['count']=0;
	for($i=1;$i<=5;$i++)
	{
		$arResult['comment'][$item['ID']]['stars'][$i]=0;
	}
}

use Bitrix\Main\Loader; 

Loader::includeModule("highloadblock");
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

$hlbl=3;
$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch(); 

$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
$entity_data_class = $entity->getDataClass();
$rsData = $entity_data_class::getList(array(
   "select" => array("UF_ID"),
   "order" => array("ID" => "ASC"),
   "filter" => array("UF_ID" => $iD)  // Задаем параметры фильтра выборки
));
$prevId=0;$count=0;$counnt=0;$lenght=count($arResult['ITEMS']);

while($arData = $rsData->Fetch()){
	$arResult['comment'][$arData['UF_ID']]++;
   }

$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
$entity_data_class = $entity->getDataClass();
$rsData = $entity_data_class::getList(array(
   "select" => array("UF_ID","UF_STARS"),
   "order" => array("ID" => "ASC"),
   "filter" => array("UF_ID" => $iD)  // Задаем параметры фильтра выборки
));


while($arData = $rsData->Fetch()){
	$arResult['comment'][$arData['UF_ID']]['count']++;
	$arResult['comment'][$arData['UF_ID']]['stars'][$arData['UF_STARS']]++;
   }
   foreach ($arResult['comment'] as $key => $item) {
   		$arResult['comment'][$key]['rate']=number_format((float)(1*$arResult['comment'][$key]['stars'][1]+2*$arResult['comment'][$key]['stars'][2]+3*$arResult['comment'][$key]['stars'][3]+4*$arResult['comment'][$key]['stars'][4]+5*$arResult['comment'][$key]['stars'][5])/$arResult['comment'][$key]['count'], 1, '.', '');
   }   
// $component = $this->getComponent();
// $arParams = $component->applyTemplateModifications();