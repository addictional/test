<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();
?>



<?if(isset($_REQUEST['LOGOUT'])){
  $USER->Logout();
}?>
<?if($USER->IsAuthorized()):?>

<?
header("HTTP/1.1 303 See Other");
if($_SERVER['HTTP_REFERER']&&$_SERVER['HTTP_REFERER']!="http://".$_SERVER['HTTP_HOST'].'/auth/')
header("Location: ".$_SERVER['HTTP_REFERER']);
else
header("Location: http://".$_SERVER['HTTP_HOST']);?>
<?else:?>
<form class="signin__enter-form" name="system_auth_form6zOYVN" method="post" target="_top" action="/auth/?login=yes">
	<input type="hidden" name="backurl" value="/auth/">
<input type="hidden" name="AUTH_FORM" value="Y">
<input type="hidden" name="TYPE" value="AUTH">
              <p class="signin__enter-title">Вход в личный кабинет</p>
              <p class="signin__enter-undersmall">Для входа укажите адрес электронной почты и пароль</p><span class="signin__enter-email">Ваш E-mail</span>
              <input class="signin__enter-inputemail" type="text" maxlength="50" name="USER_LOGIN"><span class="signin__enter-password">Пароль</span>
              <input class="signin__enter-inputpassword" type="password" name="USER_PASSWORD" maxlength="50"  autocomplete="off">
              <div class="signin__remember">
                <input class="signin__enter-checkbox-remember" type="checkbox" id="remember" value="remember">
                <label class="signin__enter-labelforcheckbox" for="remember">Запомнить меня</label>
              </div>
              <div class="signin__enter-submitforget-wrapper">
                <input class="signin__enter-button-submit" type="submit" value="Войти"><a class="signin__enter-forget-link" href="javascript: void(0);">Забыли пароль</a>
              </div>
              <p class="signin__enter-socials-text">Вы также можете войти, используя свой аккаунт в соцсетях</p>
              <a id=""class="signin__enter-socials-link" onclick="<?=$arResult['AUTH_SERVICES']['VKontakte']['ONCLICK']?>"><img class="signin__enter-socials-image" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/vkontakte-small.png" alt="vkontakte"></a>
              <a class="signin__enter-socials-link" href="javascript: void(0);"><img class="signin__enter-socials-image" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/ok-small.png" alt="odnoklassniki"></a><a class="signin__enter-socials-link" href="javascript: void(0);"><img class="signin__enter-socials-image" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/yandex-small.png" alt="yandex"></a><a class="signin__enter-socials-link" href="javascript: void(0);"><img class="signin__enter-socials-image" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/mailru-small.png" alt="mailru"></a><a class="signin__enter-socials-link" href="javascript: void(0);"><img class="signin__enter-socials-image" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/facebook-small.png" alt="facebook"></a><a class="signin__enter-socials-link" href="javascript: void(0);"><img class="signin__enter-socials-image" src="<?=SITE_TEMPLATE_PATH?>/images/icon/png/googleplus-small.png" alt="google plus"></a>
            </form>
            <?endif;?>