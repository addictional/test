
<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
  die();

  use Bitrix\Main\Page\Asset;

  global $USER;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?$APPLICATION->ShowHead();?>

                <?$APPLICATION->ShowPanel();?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/normalize.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/fonts.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/main.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/slick/slick.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/slick/slick-theme.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/pravki.css");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/jquery331dev.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/cookie.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/slick/slick.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/main.js");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/zoom.css");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/zoom/zoom-image.js");

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/zoom/main.js");
    ?>
    <?CAjax::Init();?>
    <title><?$APPLICATION->ShowTitle();?></title>
  </head>
  <body>
<header class="header">
      <div class="header-wrapper">
        <div class="header__whitespace1"></div>
        <div class="header-city"><img class="header-city__image-arrow" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/you-are-here.svg" alt="target"><span class="header-city__title-city">Ваш город:</span><?$APPLICATION->IncludeComponent(
  "api:geo.city", 
  ".default", 
  array(
    "COMPONENT_TEMPLATE" => ".default",
    "START_PAGE" => "new",
    "SHOW_LIST_PAGE" => "Y",
    "SHOW_EDIT_PAGE" => "Y",
    "SHOW_VIEW_PAGE" => "Y",
    "SUCCESS_URL" => "",
    "WEB_FORM_ID" => $_REQUEST[WEB_FORM_ID],
    "RESULT_ID" => $_REQUEST[RESULT_ID],
    "SHOW_ANSWER_VALUE" => "N",
    "SHOW_ADDITIONAL" => "N",
    "SHOW_STATUS" => "Y",
    "EDIT_ADDITIONAL" => "N",
    "EDIT_STATUS" => "Y",
    "NOT_SHOW_FILTER" => "",
    "NOT_SHOW_TABLE" => "",
    "IGNORE_CUSTOM_TEMPLATE" => "N",
    "USE_EXTENDED_ERRORS" => "N",
    "SEF_MODE" => "N",
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "N",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "CHAIN_ITEM_TEXT" => "",
    "CHAIN_ITEM_LINK" => "",
    "VARIABLE_ALIASES" => array(
      "action" => "action",
    )
  ),
  false
);?></div>
        <div class="header-phone"><img class="header-phone__image-phone" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/phone-call.svg" alt="phone"><span class="header-phone__span-number">8 800 123-45-67</span></div>
        <div class="header-time"><img class="header-time__image-clock" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/clock.svg" alt="clock"><span class="header-time__span-time">Без выходных с 9:00 до 21:00</span></div>
        <div class="header-problem"><a class="header-problem__link-dont-call" href="javascript: void(0);">Не дозвонились?</a></div>
        <div class="header-login"><img class="header-login__image-avatar" src="<?=SITE_TEMPLATE_PATH?>/images/icon/svg/avatar.svg" alt="avatar"><?if($USER->IsAuthorized()):?><a href="/personal/"><?=$USER->GetFirstName()?><a>&nbsp; |  <a class="header-login__link-singin" href="/auth/?LOGOUT=Y">Выход</a><?else:?><a class="header-login__link-singin" href="/auth/">Вход</a>&nbsp; |  <a class="header-login__link-reg" href="/auth/">Регистрация</a><?endif;?></div>
        <div class="header__whitespace2"></div>
        <div class="header__whitespace3"></div>
        <div class="header-logo"><a class="header-logo__link-logo" href="/"><img class="header-logo__image-logo" src="<?=SITE_TEMPLATE_PATH?>/images/logo_leatherman.png" alt="logo"></a></div>
        <div class="header-search">
          <input class="header-search__input-search" type="text" placeholder="Поиск по разделам, названию или артикулу">
        </div>
        <div class="header-options"><?$APPLICATION->IncludeComponent(
	"api:basket.api", 
	".default", 
	array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "Y",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"NOT_SHOW_FILTER" => "",
		"NOT_SHOW_TABLE" => "",
		"RESULT_ID" => $_REQUEST[RESULT_ID],
		"SEF_MODE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_EDIT_PAGE" => "Y",
		"SHOW_LIST_PAGE" => "Y",
		"SHOW_STATUS" => "Y",
		"SHOW_VIEW_PAGE" => "Y",
		"START_PAGE" => "new",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"WEB_FORM_ID" => $_REQUEST[WEB_FORM_ID],
		"COMPONENT_TEMPLATE" => ".default",
		"VARIABLE_ALIASES" => array(
			"action" => "action",
		)
	),
	false
);?></div>
        <div class="header__whitespace4"></div>
        <div class="header__whitespace5"></div>
        <div class="header-catalog">
          <button class="header-catalog__button" id="hcb">Каталог товаров</button>
        </div>
        <div class="header-conditions"><a class="header-conditions__link-delivery" href="javascript: void(0);">Условия доставки</a>|<a class="header-conditions__link-payment" href="javascript: void(0);">Условия оплаты</a>|<a class="header-conditions__link-exchange" href="javascript: void(0);">Обмен и возврат</a></div>
        <div class="header__whitespace6"></div>
        <div class="header__buttons__shadow-wrapper">
          <button class="header__shadow_basket"></button>
          <button class="header__shadow_avatar"></button>
          <button class="header__shadow_question"></button>
        </div>
      </div>
      <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"menu_header_main", 
	array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "menu_header_main",
		"COUNT_ELEMENTS" => "Y",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "PICTURE",
			1 => "",
		),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "/catalog/#SECTION_CODE#/",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_CLASS",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "2",
		"VIEW_MODE" => "LIST"
	),
	false
);?>
    </header>
    <div class="ordercall-overlay">
      <div class="ordercall-form-container">
        <div class="ordercall-container-button-close">
          <button class="ordercall-button-close" id="ordercall-button-close"></button>
        </div>
        <form class="form-overcall" id="form-ordercall" name="form-ordercall">
          <div class="ordercall-wrapper-flex">
            <div class="ordercall-container-title">Заказать звонок</div>
            <div class="ordercall-container-inputname">
              <input class="form-ordercall__inputname" type="text" name="ordercall-name" placeholder="Имя">
            </div>
            <div class="ordercall-container-inputphone">
              <input class="form-ordercall__inputphone" type="text" name="ordercall-phone" placeholder="Телефон">
            </div>
            <div class="ordercall-container-checkboxed">
              <input class="checkbox" id="checkbox" type="checkbox" checked>
              <label class="ordercall-accept-label" for="checkbox">Согласен(на) обработку персональных данных</label>
            </div>
            <div class="ordercall-container-submit">
              <input class="form-odercall__submit" type="submit" value="Заказать звонок">
            </div>
          </div>
        </form>
      </div>
    </div>
     <?if($APPLICATION->GetCurPage()!="/"):?><div class="catalog__header-links-wrapper">
      <div class="container">
        <div class="catalog__header-links-container"><a class="catalog__header-links-link" href="javascript: void(0);">Главная</a><span class="catalog__header-links-unablespan">&nbsp;&nbsp;| &nbsp;Мультитулы</span></div>
      </div>
      <?endif;?>
    </div>