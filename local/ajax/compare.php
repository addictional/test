<?php

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define('PUBLIC_AJAX_MODE', true);
global $APPLICATION;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;
global $USER;
global $APPLICATION;
if(isset($_REQUEST['compare']))
{
	if(!$USER->IsAuthorized()) // Для неавторизованного
      {
        $arElements = unserialize($APPLICATION->get_cookie('compares'));
        if(!in_array($_REQUEST['compare'], $arElements))
        {
               $arElements[] = $_REQUEST['compare'];
               $result = 1; // Датчик. Добавляем
        }
        else {
            $key = array_search($_REQUEST['compare'], $arElements); // Находим элемент, который нужно удалить из избранного
            unset($arElements[$key]);
            $result = 2; // Датчик. Удаляем
        }
        $APPLICATION->set_cookie("compares", serialize($arElements));
      }
      else { // Для авторизованного
         $idUser = $USER->GetID();
         $rsUser = CUser::GetByID($idUser);
         $arUser = $rsUser->Fetch();
         $arElements = $arUser['UF_COMPARES'];  // Достаём избранное пользователя
         if(!in_array($_REQUEST['compare'], $arElements)) // Если еще нету этой позиции в избранном
         {
            $arElements[] = $_REQUEST['compare'];
            $result = 1;
         }
         else {
            $key = array_search($_REQUEST['compare'], $arElements); // Находим элемент, который нужно удалить из избранного
            unset($arElements[$key]);
            $result = 2;
         }
         $USER->Update($idUser, Array("UF_COMPARES"=>$arElements )); // Добавляем элемент в избранное
      }

}