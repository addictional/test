<?php

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define('PUBLIC_AJAX_MODE', true);
use Bitrix\Main\Loader;
$array = array();
if(isset($_REQUEST['images']))
{
	if(Loader::includeModule('iblock'))
	{
		$arFilter = array("ID" => (int)$_REQUEST['images']);
		$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()){ 
 		$arProps = $ob->GetProperties();
		foreach ($arProps['pictures']['VALUE'] as $key => $image) {
			$array[] = CFile::GetPath($image); 
		}
		echo json_encode($array);
}

	}
}