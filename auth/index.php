<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("auth");
?>
<?if(isset($_REQUEST['SUCCESS'])):?>
<div class="successreg__bigtitle-wrapper">
      <div class="container">
        <div class="successreg__bigtitle-text">Вы зарегистрированы</div>
      </div>
    </div>
    <div class="successreg__wrapper">
      <div class="container">
        <div class="successreg__wrapper-flex">
          <div class="successreg__image"></div>
          <div class="successreg__message-wrapper">
            <div class="successreg__message-title">Спасибо, <?=$USER->GetFirstName()?>!</div>
            <div class="successreg__message-text">
              На указанный Вами e-mail было отправлено письмо с регистрационными данными.
              Если вы не получили письмо, проверьте, пожалуйста, папку Спам в Вашем почтовом ящике
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="helpful">
      <div class="container">
        <div class="helpful__container-flex">
          <div class="helpful__container-imagetext">
            <div class="helpful__container-image"><img class="helpful__image-envelope" src="images/icon/svg/envelope.svg" alt="envelope"></div>
            <div class="helpful__container-text">
              <p class="helpful__text">Только полезная информация, новинки и скидки для своих!</p>
            </div>
          </div>
          <div class="helpful__container-inputbutton">
            <div class="helpful__container-input">
              <input class="helpful__input-email" type="text" placeholder="Введите Ваш E-mail">
            </div>
            <div class="helpful__container-button">
              <button class="helpful__button-join">Вступить в клуб</button>
            </div>
          </div>
        </div>
      </div>
    </div>
<?else:?>
<div class="signin__bigtitle-wrapper">
	<div class="container">
		<div class="signin__bigtitle-text">
			 Вход
		</div>
	</div>
</div>
<div class="signin__enterreg-wrapper">
	<div class="container">
		<div class="signin__enterreg-wrapper-flex">
			<div class="signin__enter-wrapper">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:system.auth.form",
	"login",
	Array(
		"FORGOT_PASSWORD_URL" => "",
		"PROFILE_URL" => "",
		"REGISTER_URL" => "",
		"SHOW_ERRORS" => "N"
	)
);?>
			</div>
			 <?$APPLICATION->IncludeComponent(
	"api:auth.api",
	"",
	Array(
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "Y",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"NOT_SHOW_FILTER" => "",
		"NOT_SHOW_TABLE" => "",
		"RESULT_ID" => $_REQUEST[RESULT_ID],
		"SEF_MODE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_EDIT_PAGE" => "Y",
		"SHOW_LIST_PAGE" => "Y",
		"SHOW_STATUS" => "Y",
		"SHOW_VIEW_PAGE" => "Y",
		"START_PAGE" => "new",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("action"=>"action"),
		"WEB_FORM_ID" => $_REQUEST[WEB_FORM_ID]
	)
);?>
		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
    "api:subscribe.api",
    "",
    Array(
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_ADDITIONAL" => "N",
        "EDIT_STATUS" => "Y",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "NOT_SHOW_FILTER" => "",
        "NOT_SHOW_TABLE" => "",
        "RESULT_ID" => $_REQUEST[RESULT_ID],
        "SEF_MODE" => "N",
        "SHOW_ADDITIONAL" => "N",
        "SHOW_ANSWER_VALUE" => "N",
        "SHOW_EDIT_PAGE" => "Y",
        "SHOW_LIST_PAGE" => "Y",
        "SHOW_STATUS" => "Y",
        "SHOW_VIEW_PAGE" => "Y",
        "START_PAGE" => "new",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("action"=>"action"),
        "WEB_FORM_ID" => $_REQUEST[WEB_FORM_ID]
    ),
   FALSE
);?>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>