<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>

	<div class="error-wrapper">
      <div class="container">
        <div class="error-container-flex">
          <div class="error__image-container"></div>
          <div class="error__notfound">
            <p class="error__title">Страница не найдена</p>
            <p class="error__text">Страница, на которую вы пытаетесь попасть, не существует</p><a class="error__link" href="/">
              <button class="error__link-button">На главную</button></a>
          </div>
        </div>
      </div>
    </div>
    <?$APPLICATION->IncludeComponent(
    "api:subscribe.api",
    "",
    Array(
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_ADDITIONAL" => "N",
        "EDIT_STATUS" => "Y",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "NOT_SHOW_FILTER" => "",
        "NOT_SHOW_TABLE" => "",
        "RESULT_ID" => $_REQUEST[RESULT_ID],
        "SEF_MODE" => "N",
        "SHOW_ADDITIONAL" => "N",
        "SHOW_ANSWER_VALUE" => "N",
        "SHOW_EDIT_PAGE" => "Y",
        "SHOW_LIST_PAGE" => "Y",
        "SHOW_STATUS" => "Y",
        "SHOW_VIEW_PAGE" => "Y",
        "START_PAGE" => "new",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("action"=>"action"),
        "WEB_FORM_ID" => $_REQUEST[WEB_FORM_ID]
    ),
   FALSE
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>